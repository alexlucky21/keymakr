CREATE OR REPLACE VIEW V_APPLICATION_CAMERA_VIEWING_HOURS AS
  SELECT
    row_number() OVER ( ORDER BY ac.application_camera_id, dow.day_of_week) AS v_application_camera_viewing_hours_id,
    ac.application_camera_id,
    dow.day_of_week,
    acdvh.application_camera_viewing_hours_id,
    acdvh.from_time,
    acdvh.to_time,
    CASE WHEN acdvh.application_camera_viewing_hours_id ISNULL
      THEN FALSE
    ELSE TRUE END AS active
  FROM application_camera ac
    CROSS JOIN
    (SELECT 0 AS day_of_week
     UNION SELECT 1
     UNION SELECT 2
     UNION SELECT 3
     UNION SELECT 4
     UNION SELECT 5
     UNION SELECT 6) AS dow
    LEFT JOIN application_camera_viewing_hours acdvh ON ac.application_camera_id = acdvh.application_camera_id AND dow.day_of_week = acdvh.day_of_week AND acdvh.deleted_at ISNULL;