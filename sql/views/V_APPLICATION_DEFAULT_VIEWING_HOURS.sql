CREATE OR REPLACE VIEW v_application_default_viewing_hours AS
  SELECT
    row_number() OVER (ORDER BY a.application_id, dow.day_of_week) AS v_application_default_viewing_hours_id,
    a.application_id,
    dow.day_of_week,
    advh.application_default_viewing_hours_id,
    advh.from_time,
    advh.to_time,
    CASE WHEN advh.application_default_viewing_hours_id ISNULL
      THEN FALSE
    ELSE TRUE END AS active
  FROM application a
    CROSS JOIN
    (SELECT 0 AS day_of_week
     UNION SELECT 1
     UNION SELECT 2
     UNION SELECT 3
     UNION SELECT 4
     UNION SELECT 5
     UNION SELECT 6) AS dow
    LEFT JOIN application_default_viewing_hours advh ON a.application_id = advh.application_id AND dow.day_of_week = advh.day_of_week AND advh.deleted_at ISNULL;
