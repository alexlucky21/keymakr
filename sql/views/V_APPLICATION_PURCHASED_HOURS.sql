CREATE OR REPLACE VIEW v_application_purchased_hours AS
SELECT
  aph.application_id,
  sum(aph.amount_purchased_hours) AS purchased_hours
FROM application_purchased_hours aph
GROUP BY (aph.application_id);