CREATE OR REPLACE VIEW v_camera_assignment AS
  SELECT
    c.camera_id,
    COUNT(c2o.camera2operator_id) AS total
  FROM
    camera c
    LEFT OUTER JOIN mtm_camera2operator c2o ON c.camera_id = c2o.camera_id AND c2o.unassigned_at IS NULL
  GROUP BY
    c.camera_id