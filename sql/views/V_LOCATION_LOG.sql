CREATE OR REPLACE VIEW V_LOCATION_LOG AS
  SELECT
    l.location_id,
    MAX(al.date_time_start) AS last_log_at
  FROM
    location l
    LEFT OUTER JOIN camera c ON l.location_id = c.location_id
    LEFT OUTER JOIN mtm_camera2action c2a ON c.camera_id = c2a.camera_id
    LEFT OUTER JOIN action_log al ON c2a.camera2action_id = al.camera_action_id
  GROUP BY
    l.location_id