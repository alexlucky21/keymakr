CREATE OR REPLACE VIEW v_operator_assignment AS
  SELECT
    o.operator_id,
    COUNT(c2o.camera2operator_id) AS total,
    SUM(CASE
          WHEN c2o.is_main THEN 1
          ELSE 0 END) AS main_total
  FROM
    operator o
    LEFT OUTER JOIN mtm_camera2operator c2o ON o.operator_id = c2o.operator_id AND c2o.unassigned_at IS NULL
  GROUP BY
    o.operator_id