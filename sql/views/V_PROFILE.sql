CREATE OR REPLACE VIEW v_profile AS
    SELECT i.identity_id, p.first_name, p.last_name, concat_ws(' ', p.first_name, p.last_name) AS name,
            p.email_address, p.phone, r.role_id, r.description AS role_name FROM profile p
        JOIN identity i on p.identity_id = i.identity_id
        JOIN role r on i.role_id = r.role_id;