package com.keymakr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KeymakrApplication {

	public static void main(String[] args) {
		SpringApplication.run(KeymakrApplication.class, args);
	}
}
