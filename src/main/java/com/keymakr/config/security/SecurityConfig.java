package com.keymakr.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import javax.sql.DataSource;

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter{

    private static final String LOGIN_PATH = "/login";
    private static final String LOGOUT_PATH = "/logout";

    @Autowired
    DataSource dataSource;

    @Autowired
    private HttpAuthenticationEntryPoint authenticationEntryPoint;

    @Autowired
    private AuthSuccessHandler authSuccessHandler;

    @Autowired
    private AuthFailureHandler authFailureHandler;

    @Autowired
    private HttpLogoutSuccessHandler logoutSuccessHandler;


    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.csrf().disable()
                .exceptionHandling()
                .authenticationEntryPoint(authenticationEntryPoint)
                .and()
                .authorizeRequests()
                .antMatchers("/app/customer/**").hasRole("CUSTOMER")
                .antMatchers("/app/operator/**").hasAnyRole("OPERATOR")
                .antMatchers("/app/customer-admin/**").hasAnyRole("CUSTOMER_ADMIN")
                .antMatchers("/app/operator-admin/**").hasAnyRole("OPERATOR_ADMIN")
                .antMatchers("/app/main-admin/**").hasAnyRole("MAIN_ADMIN")
                .anyRequest().permitAll()
                .and()
                .formLogin()
                .loginPage(LOGIN_PATH)
                .successHandler(authSuccessHandler)
                .failureHandler(authFailureHandler)
                .permitAll()
                .and()
                .logout()
                .logoutSuccessHandler(logoutSuccessHandler)
                .logoutUrl(LOGOUT_PATH)
                .deleteCookies("JSESSIONID")
                .permitAll();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        //language=SQL
        final String USERS_BY_USERNAME_SQL =
                "SELECT " +
                "       username, password, 1 AS enabled " +
                "   FROM " +
                "       identity " +
                "   WHERE " +
                "       username = ? ";
        final String AUTHORITIES_BY_USERNAME_SQL =
                "SELECT " +
                "       u.username, r.name AS authority " +
                "   FROM " +
                "       identity AS u " +
                "       INNER JOIN role AS r ON u.role_id = r.role_id " +
                "   WHERE " +
                "       username = ? ";

        auth.jdbcAuthentication().dataSource(dataSource)
                .rolePrefix("ROLE_")
                .usersByUsernameQuery(USERS_BY_USERNAME_SQL)
                .authoritiesByUsernameQuery(AUTHORITIES_BY_USERNAME_SQL);
    }
}
