package com.keymakr.controller;

import com.keymakr.model.action.Action;
import com.keymakr.model.action.ActionParameter;
import com.keymakr.service.ActionParameterService;
import com.keymakr.service.ActionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
public class ActionController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private ActionService actionService;
    private ActionParameterService actionParameterService;

    @Autowired
    public ActionController(ActionService actionService, ActionParameterService actionParameterService) {
        this.actionService = actionService;
        this.actionParameterService = actionParameterService;
    }

    @RequestMapping("/actions/default")
    @ResponseBody
    public List<Action> getDefaultActions() {
        return actionService.getDefaultActions();
    }

    @RequestMapping(value = "/actions", method = POST)
    @ResponseBody
    public Action saveAction(@RequestBody Action action) {
        return actionService.saveAction(action);
    }

    @RequestMapping(value = "/actions/{actionId}", method = DELETE)
    public Long deleteAction(@PathVariable("actionId") Long actionId) {
        return actionService.deleteAction(actionId);
    }

    @RequestMapping(value = "/actions/{actionId}/parameters", method = GET)
    public List<ActionParameter> getActionParameterByActionId(@PathVariable("actionId") Long actionId){
        return actionParameterService.getActionParameterByActionId(actionId);
    }

    @RequestMapping(value = "/actions/parameters", method = POST)
    @ResponseBody
    public ActionParameter saveActionParameter(@RequestBody ActionParameter actionParameter) {
        return actionParameterService.saveActionParameter(actionParameter);
    }

    @RequestMapping(value = "/actions/parameters/{actionParameterId}", method = DELETE)
    @ResponseBody
    public Long deleteActionParameter(@PathVariable("actionParameterId") Long actionParameterId) {
        return actionParameterService.deleteActionParameter(actionParameterId);
    }

}