package com.keymakr.controller;

import com.keymakr.model.application.*;
import com.keymakr.model.customer.Customer;
import com.keymakr.service.application.*;
import com.keymakr.service.email.EmailNotificationService;
import freemarker.template.TemplateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
public class ApplicationController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private ApplicationService applicationService;
    private EmailNotificationService emailNotificationService;
    private ApplicationLocationService applicationLocationService;
    private ApplicationCameraService applicationCameraService;
    private ApplicationDefaultViewingHoursService applicationDefaultViewingHoursService;
    private ApplicationCameraViewingHoursService applicationCameraViewingHoursService;
    private ApplicationPurchasedHoursService applicationPurchasedHoursService;

    @Autowired
    public ApplicationController(ApplicationCameraViewingHoursService applicationCameraViewingHoursService, ApplicationDefaultViewingHoursService applicationDefaultViewingHoursService, ApplicationCameraService applicationCameraService, ApplicationLocationService applicationLocationService, ApplicationService applicationService, EmailNotificationService emailNotificationService, ApplicationPurchasedHoursService applicationPurchasedHoursService) {
        this.applicationService = applicationService;
        this.emailNotificationService = emailNotificationService;
        this.applicationLocationService = applicationLocationService;
        this.applicationCameraService = applicationCameraService;
        this.applicationDefaultViewingHoursService = applicationDefaultViewingHoursService;
        this.applicationCameraViewingHoursService = applicationCameraViewingHoursService;
        this.applicationPurchasedHoursService = applicationPurchasedHoursService;
    }

    @RequestMapping("/applications")
    @ResponseBody
    public Page<Application> getApplications(@RequestParam("pageNumber") Integer pageNumber, @RequestParam("pageSize") Integer pageSize) {
        return applicationService.getApplications(pageNumber, pageSize);
    }

    @RequestMapping(value = "/applications", method = POST)
    @ResponseBody
    public Application saveApplication(@RequestBody Application application) {
        return applicationService.saveApplication(application);
    }

    @RequestMapping(value = "/applications/{applicationId}", method = GET)
    public Application getOperatorById(@PathVariable("applicationId") Long applicationId) {
        return applicationService.getApplicationById(applicationId);
    }

    @RequestMapping(value = "/applications/activateAccount", method = POST)
    @ResponseBody
    @Secured({"ROLE_CUSTOMER_ADMIN"})
    public Customer activateAccount(@RequestBody Application application) throws IOException, TemplateException {
        Customer customer = applicationService.activateAccount(application.getId());
        emailNotificationService.sendAccountActivationNotification(customer);
        return customer;
    }

    @RequestMapping(value = "/applications/decline", method = POST)
    @ResponseBody
    public Application declineApplication(@RequestBody Application application) throws IOException, TemplateException {
        Application declinedApplication = applicationService.declineApplication(application);
        emailNotificationService.sendDeclineNotification(declinedApplication);
        return declinedApplication;
    }

    @RequestMapping(value = "/applications/{applicationId}/applicationLocations", method = GET)
    public List<ApplicationLocation> getApplicationLocationsByApplicationId(@PathVariable("applicationId") Long applicationId){
        return applicationLocationService.getApplicationLocationsByApplicationId(applicationId);
    }

    @RequestMapping(value = "/applications/applicationLocations", method = POST)
    public ApplicationLocation saveApplicationLocation(@RequestBody ApplicationLocation applicationLocation){
        return applicationLocationService.saveApplicationLocation(applicationLocation);
    }

    @RequestMapping(value = "/applications/applicationLocations/{applicationLocationId}", method = DELETE)
    public Long deleteApplicationLocation(@PathVariable("applicationLocationId") Long applicationLocationId) {
        return applicationLocationService.deleteApplicationLocation(applicationLocationId);
    }

    @RequestMapping(value = "/applications/applicationLocations/{applicationLocationId}/applicationCameras", method = GET)
    public List<ApplicationCamera> getApplicationCamerasByApplicationLocationId(@PathVariable("applicationLocationId") Long applicationLocationId){
        return applicationCameraService.getApplicationCamerasByApplicationLocationId(applicationLocationId);
    }

    @RequestMapping(value = "/applications/applicationLocations/applicationCameras/{applicationCameraId}", method = GET)
    public ApplicationCamera getApplicationCameraById(@PathVariable("applicationCameraId") Long applicationCameraId) {
        return applicationCameraService.getApplicationCameraById(applicationCameraId);
    }

    @RequestMapping(value = "/applications/applicationLocations/applicationCameras/{applicationCameraId}", method = DELETE)
    public Long deleteApplicationCamera(@PathVariable("applicationCameraId") Long applicationCameraId) {
        return applicationCameraService.deleteApplicationCamera(applicationCameraId);
    }

    @RequestMapping(value = "/applications/applicationLocations/applicationCameras", method = POST)
    public ApplicationCamera saveApplicationCamera(@RequestBody ApplicationCamera applicationCamera) {
        return applicationCameraService.saveApplicationCamera(applicationCamera);
    }

    @RequestMapping(value = "/applications/applicationLocations/applicationCameras/activate", method = POST)
    public ApplicationCamera activateApplicationCamera(@RequestBody ApplicationCamera applicationCamera) {
        return applicationCameraService.activateApplicationCamera(applicationCamera);
    }

    @RequestMapping(value = "/applications/applicationLocations/applicationCameras/viewingHours", method = POST)
    public Iterable<ApplicationCameraViewingHours> saveViewingHours(@RequestBody List<ApplicationCameraViewingHours> applicationCameraViewingHours) {
        return applicationCameraViewingHoursService.saveViewingHours(applicationCameraViewingHours);
    }

    @RequestMapping(value = "/applications/applicationLocations/applicationCameras/viewingHours", method = PUT)
    public List<Long> deleteApplicationCameraViewingHours(@RequestBody List<Long> applicationApplicationCameraViewingHoursIds) {
        return applicationCameraViewingHoursService.deleteViewingHours(applicationApplicationCameraViewingHoursIds);
    }

    @RequestMapping(value = "/applications/applicationLocations/applicationCameras/{applicationCameraId}/viewingHours", method = GET)
    public List<ApplicationCameraViewingHoursView> getViewingHoursByApplicationCameraId(@PathVariable("applicationCameraId") Long applicationCameraId) {
        return applicationCameraViewingHoursService.getViewingHoursListByApplicationCameraId(applicationCameraId);
    }

    @RequestMapping(value = "/applications/{applicationId}/viewingHours", method = GET)
    @ResponseBody
    public List<ApplicationDefaultViewingHoursView> getViewingHoursByApplicationId(@PathVariable("applicationId") Long applicationId){
        return applicationDefaultViewingHoursService.getViewingHoursListByApplicationId(applicationId);
    }

    @RequestMapping(value = "/applications/viewingHours", method = POST)
    @ResponseBody
    public Iterable<ApplicationDefaultViewingHours> saveDefaultViewingHours(@RequestBody List<ApplicationDefaultViewingHours> applicationDefaultViewingHoursList) {
        return applicationDefaultViewingHoursService.saveViewingHours(applicationDefaultViewingHoursList);
    }

    @RequestMapping(value = "/applications/viewingHours", method = PUT)
    public List<Long> deleteDefaultViewingHours(@RequestBody List<Long> applicationDefaultViewingHoursIds) {
        return applicationDefaultViewingHoursService.deleteViewingHours(applicationDefaultViewingHoursIds);
    }

    @RequestMapping(value = "/applications/purchasedHours", method = POST)
    public ApplicationPurchasedHours savePurchasedHours(@RequestBody ApplicationPurchasedHours applicationPurchasedHours) {
        return applicationPurchasedHoursService.saveApplicationPurchasedHours(applicationPurchasedHours);
    }

    @RequestMapping(value = "/applications/{applicationId}/purchasedHours", method = GET)
    public ApplicationPurchasedHoursView gePurchasedHoursByApplicationId(@PathVariable("applicationId") Long applicationId){
        return applicationPurchasedHoursService.getApplicationPurchasedHoursViewByApplicationId(applicationId);
    }
}