package com.keymakr.controller;

import com.keymakr.model.camera.Camera;
import com.keymakr.model.camera.CameraAssignment;
import com.keymakr.model.camera.CameraFilter;
import com.keymakr.service.CameraAssignmentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CameraAssignmentController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private CameraAssignmentService cameraAssignmentService;

    @Autowired
    public CameraAssignmentController(CameraAssignmentService cameraAssignmentService) {
        this.cameraAssignmentService = cameraAssignmentService;
    }

    @RequestMapping("/cameraAssignments/all")
    public List<CameraAssignment> getAllAssignment(@RequestParam(required = false) Long operatorId,
                                                   @RequestParam(required = false) Long locationId,
                                                   @RequestParam(required = false) Long cameraId,
                                                   @RequestParam(required = false) Long customerId,
                                                   @RequestParam(required = false) Camera.State state,
                                                   @RequestParam(required = false) String searchQuery) {
        CameraFilter filter = CameraFilter.builder()
                .operatorId(operatorId)
                .locationId(locationId)
                .cameraId(cameraId)
                .customerId(customerId)
                .state(state)
                .searchString(searchQuery)
                .build();

        return cameraAssignmentService.getCameraAssignments(filter);
    }

    @RequestMapping("/cameraAssignments")
    public Page<CameraAssignment> getAllAssignment(@RequestParam(required = false) Integer pageNumber,
                                                   @RequestParam(required = false) Integer pageSize,
                                                   @RequestParam(required = false) Long operatorId,
                                                   @RequestParam(required = false) Long locationId,
                                                   @RequestParam(required = false) Long cameraId,
                                                   @RequestParam(required = false) Long customerId,
                                                   @RequestParam(required = false) Camera.State state,
                                                   @RequestParam(required = false) String searchQuery) {
        CameraFilter filter = CameraFilter.builder()
                .operatorId(operatorId)
                .locationId(locationId)
                .cameraId(cameraId)
                .customerId(customerId)
                .state(state)
                .searchString(searchQuery)
                .build();

        if (pageNumber == null || pageSize == null) {
            pageNumber = 1;
            pageSize = Integer.MAX_VALUE;
        }

        return cameraAssignmentService.getCameraAssignments(pageNumber, pageSize, filter);
    }
}
