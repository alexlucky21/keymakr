package com.keymakr.controller;

import com.keymakr.model.camera.Camera;
import com.keymakr.model.camera.CameraFilter;
import com.keymakr.model.camera.Location;
import com.keymakr.model.customer.Customer;
import com.keymakr.service.CameraService;
import com.keymakr.service.CustomerService;
import com.keymakr.service.LocationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
public class CustomerController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private CustomerService customerService;

    private LocationService locationService;

    private CameraService cameraService;

    @Autowired
    public CustomerController(CustomerService customerService, LocationService locationService,
                              CameraService cameraService) {
        this.customerService = customerService;
        this.locationService = locationService;
        this.cameraService = cameraService;
    }

    @RequestMapping("/customers/all")
    public List<Customer> getCustomers() {
        return customerService.getCustomers();
    }

    @RequestMapping("/customers")
    public Page<Customer> getCustomers(@RequestParam("pageNumber") Integer pageNumber,
                                       @RequestParam("pageSize") Integer pageSize,
                                       @RequestParam("searchQuery") String searchQuery) {
        return customerService.getCustomerList(pageNumber, pageSize, searchQuery);
    }


    @RequestMapping(value = "/customers", method = POST)
    public Customer saveCustomer(Customer customer) {
        return customerService.saveCustomer(customer);
    }

    @RequestMapping(value = "/customers/{customerId}")
    public Customer getCustomerById(@PathVariable("customerId") Long customerId) {
        return customerService.getCustomerById(customerId);
    }

    @RequestMapping(value = "/customers/{customerId}", method = DELETE)
    public Long deleteCustomer(@PathVariable("customerId") Long customerId) {
        return customerService.deleteCustomer(customerId);
    }

    @RequestMapping("/customers/{customerId}/locations")
    public List<Location> getLocationsByCustomerId(@PathVariable("customerId") Long customerId) {
        return locationService.getLocationsByCustomerId(customerId);
    }

    @RequestMapping("/customers/{customerId}/cameras")
    public List<Camera> getCamerasByCustomerId(@PathVariable("customerId") Long customerId) {
        CameraFilter filter = CameraFilter.builder().customerId(customerId).build();

        return cameraService.getCamerasByCustomerId(filter);
    }
}
