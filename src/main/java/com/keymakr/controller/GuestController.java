package com.keymakr.controller;

import com.keymakr.service.GuestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GuestController {

    private GuestService guestService;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    public GuestController(GuestService guestService) {
        this.guestService = guestService;
    }
}
