package com.keymakr.controller;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

@Controller
public class IndexController {

    @RequestMapping({"customer/**", "operator/**", "customer-admin/**", "operator-admin/**", "main-admin/**", "registration/**"})
    public String redirectToIndex() {
        return "/";
    }

    @RequestMapping("/login")
    @ResponseBody
    public User getUser(HttpServletResponse response, Authentication authentication){
        response.setStatus(HttpServletResponse.SC_OK);

        return authentication == null ? null : (User) authentication.getPrincipal();
    }

}
