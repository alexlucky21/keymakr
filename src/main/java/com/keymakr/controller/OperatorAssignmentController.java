package com.keymakr.controller;

import com.keymakr.model.operator.Operator;
import com.keymakr.model.operator.OperatorAssignment;
import com.keymakr.service.OperatorAssignmentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
public class OperatorAssignmentController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private OperatorAssignmentService operatorAssignmentService;

    @Autowired
    public OperatorAssignmentController(OperatorAssignmentService operatorAssignmentService) {
        this.operatorAssignmentService = operatorAssignmentService;
    }

    @RequestMapping("/operatorAssignments/all")
    public List<OperatorAssignment> getAllAssignment(@RequestParam(required = false) List<Operator.State> states,
                                                     @RequestParam(required = false) String searchQuery) {
        return operatorAssignmentService.getAssignmentsByNameOrStateIn(states, searchQuery);
    }

    @RequestMapping(value = "/operatorAssignments", method = GET)
    public Page<OperatorAssignment> getOperators(@RequestParam(required = false) Integer pageNumber,
                                                 @RequestParam(required = false) Integer pageSize,
                                                 @RequestParam(required = false) List<Operator.State> states,
                                                 @RequestParam(required = false) String searchQuery) {
        if (pageNumber == null || pageSize == null) {
            pageNumber = 1;
            pageSize = Integer.MAX_VALUE;
        }

        return operatorAssignmentService.getAssignmentsByNameAndStateIn(pageNumber, pageSize, states, searchQuery);
    }

    @RequestMapping("/operatorAssignments/suggested")
    public List<OperatorAssignment> getOtherAssignment(@RequestParam(required = false) Long locationId,
                                                       @RequestParam(required = false) Long cameraId,
                                                       @RequestParam(required = false) String searchQuery) {
        return operatorAssignmentService.getFilteredAssignments(locationId, cameraId, searchQuery, true);
    }

    @RequestMapping("/operatorAssignments/other")
    public List<OperatorAssignment> getSeggestedAssignment(@RequestParam(required = false) Long locationId,
                                                           @RequestParam(required = false) Long cameraId,
                                                           @RequestParam(required = false) String searchQuery) {
        return operatorAssignmentService.getFilteredAssignments(locationId, cameraId, searchQuery, false);
    }


}
