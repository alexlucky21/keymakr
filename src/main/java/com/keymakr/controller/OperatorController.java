package com.keymakr.controller;

import com.keymakr.model.camera.CameraOperator;
import com.keymakr.model.operator.Operator;
import com.keymakr.model.operator.OperatorRequest;
import com.keymakr.service.CameraService;
import com.keymakr.service.OperatorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
public class OperatorController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private OperatorService operatorService;

    private CameraService cameraService;

    @Autowired
    public OperatorController(OperatorService operatorService, CameraService cameraService) {
        this.operatorService = operatorService;
        this.cameraService = cameraService;
    }

    @RequestMapping(value = "/operators", method = GET)
    public Page<Operator> getOperators(@RequestParam(required = false) Integer pageNumber,
                                       @RequestParam(required = false) Integer pageSize,
                                       @RequestParam(required = false) List<Operator.State> states,
                                       @RequestParam(required = false) String searchQuery) {
        if (pageNumber == null || pageSize == null) {
            pageNumber = 1;
            pageSize = Integer.MAX_VALUE;
        }

        return operatorService.getOperatorsByNameAndStateIn(pageNumber, pageSize, states, searchQuery);
    }

    @RequestMapping(value = "/operators/all", method = GET)
    public List<Operator> getOperators() {
        return operatorService.getOperators();
    }

    @Transactional
    @RequestMapping(value = "/operators", method = POST)
    public Operator saveOperator(@RequestBody Operator operator) {
        return operatorService.saveOperator(operator);
    }

    @RequestMapping(value = "/operators/{operatorId}")
    public Operator getOperatorById(@PathVariable("operatorId") Long operatorId) {
        return operatorService.getOperatorById(operatorId);
    }

    @Transactional
    @RequestMapping(value = "/operators/{operatorId}", method = DELETE)
    public Long deleteOperator(@PathVariable("operatorId") long operatorId) {
        return operatorService.deleteOperator(operatorId);
    }

    @RequestMapping(value = "/operators/{operatorId}/cameras", method = GET)
    public List<CameraOperator> getCameraOperatorsByOperatorId(@PathVariable("operatorId") Long operatorId) {
        return cameraService.getCameraOperatorsByOperatorId(operatorId);
    }

    @Transactional
    @RequestMapping(value = "/operators/{operatorId}/assign")
    public CameraOperator assignCameraToOperator(@PathVariable("operatorId") Long operatorId,
                                                 @RequestParam Long cameraId,
                                                 @RequestParam Boolean isMain) {
        return cameraService.assignCameraToOperator(cameraId, operatorId, null, isMain);
    }

    @Transactional
    @RequestMapping(value = "/operators/{operatorId}/unassign")
    public Long unassignCameraFromOperator(@PathVariable("operatorId") Long operatorId,
                                           @RequestParam Long cameraId) {
        return cameraService.unassignCameraFromOperator(cameraId, operatorId);
    }

    @Transactional
    @RequestMapping(value = "/operators/requests/viewed", method = POST)
    public Boolean markRequestsAsViewed(@RequestBody List<Long> viewedRequests)  {
        return operatorService.markRequestsAsViewed(viewedRequests);
    }

    @RequestMapping(value = "/operators/requests", method = GET)
    public Page<OperatorRequest> getOperators(@RequestParam(required = false) Integer pageNumber,
                                              @RequestParam(required = false) Integer pageSize) {
        if (pageNumber == null || pageSize == null) {
            pageNumber = 1;
            pageSize = Integer.MAX_VALUE;
        }

        return operatorService.getOperatorRequests(pageNumber, pageSize);
    }
}
