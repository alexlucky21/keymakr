package com.keymakr.controller;

import com.google.gson.Gson;
import com.keymakr.controller.exeption.CaptchaException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import static org.springframework.web.bind.annotation.RequestMethod.POST;


@RestController
public class RecaptchaController {
    private final String SECRET_PARAMETER = "6LcTUAsUAAAAADpanbkHgV__NWC4Vc6SyHxJdfQs";

    @RequestMapping(value = "/reCaptcha", method = POST)
    public String checkCaptcha(@RequestBody String recap) throws Exception {
        URL url = new URL("https://www.google.com/recaptcha/api/siteverify");

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        String postParams = "secret=" + SECRET_PARAMETER + "&response=" + recap;
        conn.setDoOutput(true);
        try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
            wr.writeBytes(postParams);
            wr.flush();
        }

        String line, outputString = "";

        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(conn.getInputStream()))) {
            while ((line = reader.readLine()) != null) {
                outputString += line;
            }
        }
        // Convert response into Object
        CaptchaResponse capRes = new Gson().fromJson(outputString, CaptchaResponse.class);

        // Verify whether the input from Human or Robot
        if (capRes.isSuccess()) {
            return outputString;
        } else {
            throw new CaptchaException("Client used fake reCaptcha");
        }
    }

    private class CaptchaResponse {
        private boolean success;

        private boolean isSuccess() {
            return success;
        }

        public void setSuccess(boolean success) {
            this.success = success;
        }
    }
}
