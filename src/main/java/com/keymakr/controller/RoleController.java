package com.keymakr.controller;

import com.keymakr.model.Role;
import com.keymakr.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class RoleController {

    private RoleService roleService;

    @Autowired
    public RoleController(RoleService roleService) {
        this.roleService = roleService;
    }

    @RequestMapping("/roles/admin")
    @ResponseBody
    public List<Role> getAdminRoles() {
        return roleService.getAdminRoles();
    }

    @RequestMapping("/roles/all")
    @ResponseBody
    public List<Role> getAllRoles() {
        return roleService.getAllRoles();
    }

}
