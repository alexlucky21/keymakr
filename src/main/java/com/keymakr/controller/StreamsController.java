package com.keymakr.controller;

import com.keymakr.model.camera.CameraFilter;
import com.keymakr.model.log.ActionLog;
import com.keymakr.model.camera.Camera;
import com.keymakr.model.camera.CameraOperator;
import com.keymakr.model.camera.Location;
import com.keymakr.model.customer.Customer;
import com.keymakr.model.log.ActionLogFilter;
import com.keymakr.model.log.LocationLog;
import com.keymakr.model.log.LocationLogFilter;
import com.keymakr.model.operator.Operator;
import com.keymakr.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
public class StreamsController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private LocationService locationService;

    private CameraService cameraService;

    private CustomerService customerService;

    private OperatorService operatorService;

    private ActionLogService actionLogService;

    private LocationLogService locationLogService;

    @Autowired
    public StreamsController(LocationService locationService, CameraService cameraService,
                             CustomerService customerService, OperatorService operatorService,
                             ActionLogService actionLogService,
                             LocationLogService locationLogService) {
        this.locationService = locationService;
        this.cameraService = cameraService;
        this.customerService = customerService;
        this.operatorService = operatorService;
        this.actionLogService = actionLogService;
        this.locationLogService = locationLogService;
    }

    @RequestMapping(value = "/streams/locations/all", method = GET)
    public List<Location> getLocations() {
        return locationService.getAllLocations();
    }

    @RequestMapping(value = "/streams/locations", method = POST)
    public Location saveLocation(Location location) {
        return locationService.saveLocation(location);
    }

    @RequestMapping(value = "/streams/locations/unassigned")
    public List<Location> getLocationsWithUnassignedCameras() {
        return locationService.getLocationsWithUnassignedCameras();
    }

    @RequestMapping(value = "/streams/locations/{locationId}", method = DELETE)
    public void deleteLocation(@PathVariable("locationId") Long locationId) {
        locationService.deleteLocation(locationId);
    }

    @RequestMapping(value = "/streams/locations/{locationId}")
    public Location getLocationById(@PathVariable("locationId") Long locationId) {
        return locationService.getLocationById(locationId);
    }

    @RequestMapping("/streams/locations/{locationId}/cameras")
    public List<Camera> getCamerasByLocationId(@PathVariable("locationId") Long locationId) {
        CameraFilter filter = CameraFilter.builder().locationId(locationId).build();

        return cameraService.getCamerasByLocationId(filter);
    }

    @RequestMapping("/streams/locations/{locationId}/customer")
    public Customer getCustomerByLocationId(@PathVariable("locationId") Long locationId) {
        return customerService.getCustomerByLocationId(locationId);
    }

    @RequestMapping(value = "/streams/locations/{locationId}/log", method = GET)
    public Page<ActionLog> getLogDetails(@PathVariable("locationId") Long locationId,
                                             @RequestParam(value = "pageNumber", required = false) Integer pageNumber,
                                             @RequestParam(value = "pageSize", required = false) Integer pageSize,
                                             @RequestParam(value = "from", required = false) Long from,
                                             @RequestParam(value = "to", required = false) Long to,
                                             @RequestParam(value = "streamName", required = false) String streamName,
                                             @RequestParam(value = "actionName", required = false) String actionName,
                                             @RequestParam(value = "operatorName", required = false) String operatorName,
                                             @RequestParam(value = "isMain", required = false) Boolean isMain) {
        ActionLogFilter filter = ActionLogFilter.builder()
                                    .from(from == null ? null : new Date(from))
                                    .to(to == null ? null : new Date(to))
                                    .locationId(locationId)
                                    .streamName(streamName)
                                    .actionName(actionName)
                                    .operatorName(operatorName)
                                    .isMain(isMain)
                                    .build();

        if (pageNumber == null || pageSize == null) {
            pageNumber = 1;
            pageSize = Integer.MAX_VALUE;
        }

        return actionLogService.getActionLogs(pageNumber, pageSize, filter);
    }

    @RequestMapping(value = "/streams/locations/logs", method = GET)
    public Page<LocationLog> getLocationLog(@RequestParam(value = "pageNumber", required = false) Integer pageNumber,
                                            @RequestParam(value = "pageSize", required = false) Integer pageSize,
                                            @RequestParam(value = "from", required = false) Long from,
                                            @RequestParam(value = "to", required = false) Long to,
                                            @RequestParam(value = "locationName", required = false) String locationName,
                                            @RequestParam(value = "customerId", required = false) Long customerId,
                                            @RequestParam(value = "cameraId", required = false) Long cameraId,
                                            @RequestParam(value = "operatorId", required = false) Long operatorId) {
        LocationLogFilter filter = LocationLogFilter.builder()
                .from(from == null ? null : new Date(from))
                .to(to == null ? null : new Date(to))
                .locationName(locationName)
                .customerId(customerId)
                .operatorId(operatorId)
                .build();


        if (pageNumber == null || pageSize == null) {
            pageNumber = 1;
            pageSize = Integer.MAX_VALUE;
        }

        return locationLogService.getLocationLogs(pageNumber, pageSize, filter);
    }

    @RequestMapping(value = "/streams/cameras/all", method = GET)
    public List<Camera> getCameras() {
        return cameraService.getCameras();
    }

    @RequestMapping(value = "/streams/cameras", method = GET)
    public Page<Camera> getCameras(@RequestParam(value = "pageNumber", required = false) Integer pageNumber,
                                   @RequestParam(value = "pageSize", required = false) Integer pageSize,
                                   @RequestParam(value = "operatorId", required = false) Long operatorId,
                                   @RequestParam(value = "locationId", required = false) Long locationId,
                                   @RequestParam(value = "cameraId", required = false) Long cameraId,
                                   @RequestParam(value = "customerId", required = false) Long customerId,
                                   @RequestParam(value = "state", required = false) Camera.State state,
                                   @RequestParam(value = "searchQuery", required = false) String searchQuery) {
        CameraFilter filter = CameraFilter.builder()
                                          .operatorId(operatorId)
                                          .locationId(locationId)
                                          .cameraId(cameraId)
                                          .customerId(customerId)
                                          .state(state)
                                          .searchString(searchQuery)
                                          .build();

        if (pageNumber == null || pageSize == null) {
            pageNumber = 1;
            pageSize = Integer.MAX_VALUE;
        }

        return cameraService.getCameras(pageNumber, pageSize, filter);
    }

    @RequestMapping(value = "/streams/cameras", method = POST)
    public Camera saveCamera(Camera camera) {
        return cameraService.saveCamera(camera);
    }

    @RequestMapping("/streams/cameras/{cameraId}")
    public Camera getCameraById(@PathVariable("cameraId") Long cameraId) {
        return cameraService.getCameraById(cameraId);
    }

    @RequestMapping(value = "/streams/cameras/{cameraId}", method = DELETE)
    public void deleteCamera(@PathVariable("cameraId") Long cameraId) {
        cameraService.deleteCamera(cameraId);
    }

    @RequestMapping("/streams/cameras/{cameraId}/location")
    public Location getLocationByCameraId(@PathVariable("cameraId") Long cameraId) {
        return locationService.getLocationByCameraId(cameraId);
    }

    @RequestMapping("/streams/cameras/{cameraId}/main_operator")
    public Operator getMainOperatorByCameraId(@PathVariable("cameraId") Long cameraId) {
        return operatorService.getMainOperatorByCameraId(cameraId);
    }

    @RequestMapping(value = "/streams/cameras/{cameraId}/operators", method = GET)
    public List<CameraOperator> getOperatorsByCameraId(@PathVariable("cameraId") Long cameraId) {
        return operatorService.getCameraOperatorsByCameraId(cameraId);
    }

    @RequestMapping(value = "/streams/cameras/{cameraId}/operators", method = POST)
    public List<CameraOperator> updateCamerasOfOperator(@PathVariable("cameraId") Long cameraId,
                                                        @RequestBody List<CameraOperator> cameraOperators)  {
        return cameraService.updateOperatorsOfCamera(cameraId, cameraOperators);
    }

    @RequestMapping("/streams/cameras/{cameraId}/customer")
    public Customer getCustomerByCameraId(@PathVariable("cameraId") Long cameraId) {
        return customerService.getCustomerByCameraId(cameraId);
    }

    @RequestMapping(value = "/streams/cameras/{cameraId}/reassign")
    public CameraOperator reassignCamera(@PathVariable("cameraId") Long cameraId,
                                                 @RequestParam Long operatorRequestId,
                                                 @RequestParam Long toOperatorId,
                                                 @RequestParam Boolean isMain) {
        return cameraService.reassignCamera(operatorRequestId, toOperatorId, isMain);
    }

    @RequestMapping(value = "/streams/cameras/{cameraId}/assign")
    public CameraOperator assignOperatorToCamera(@PathVariable("cameraId") Long cameraId,
                                                 @RequestParam Long operatorId,
                                                 @RequestParam Boolean isMain) {
        return cameraService.assignCameraToOperator(cameraId, operatorId, null, isMain);
    }

    @RequestMapping(value = "/streams/cameras/{cameraId}/unassign")
    public Long unassignOperatorFromCamera(@PathVariable("cameraId") Long cameraId,
                                           @RequestParam Long operatorId) {
        return cameraService.unassignCameraFromOperator(cameraId, operatorId);
    }
}
