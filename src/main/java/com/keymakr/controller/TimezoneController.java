package com.keymakr.controller;

import com.keymakr.service.TimezoneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TimezoneController {

    private TimezoneService timezoneService;

    @Autowired
    public TimezoneController(TimezoneService timezoneService) {
        this.timezoneService = timezoneService;
    }

    @RequestMapping("/timezone/all")
    @ResponseBody
    public List<String> getTimezones() {
        return timezoneService.getTimezones();
    }
}
