package com.keymakr.controller.exeption;

public class CaptchaException extends Exception {

    public CaptchaException(String message) {
        super(message);
    }
}
