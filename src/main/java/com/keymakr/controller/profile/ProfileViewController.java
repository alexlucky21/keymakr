package com.keymakr.controller.profile;

import com.keymakr.model.Profile;
import com.keymakr.model.ProfileView;
import com.keymakr.service.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


@Controller
public class ProfileViewController {

    private ProfileService profileService;

    @Autowired
    public ProfileViewController(ProfileService profileService) {
        this.profileService = profileService;
    }

    @RequestMapping("/profileView/admins")
    @ResponseBody
    public Page<ProfileView> getAdminList(@RequestParam("pageNumber") Integer pageNumber,
                                          @RequestParam("pageSize") Integer pageSize,
                                          @RequestParam("custAdminFilterFl") Boolean custAdminFilterFl,
                                          @RequestParam("operAdminFilterFl") Boolean operAdminFilterFl,
                                          @RequestParam("searchQuery") String searchQuery) {
        return profileService.getAdminList(pageNumber, pageSize, custAdminFilterFl, operAdminFilterFl, searchQuery);
    }

    @RequestMapping("/profileView/operators")
    @ResponseBody
    public Page<ProfileView> getOperatorList(@RequestParam("pageNumber") Integer pageNumber,
                                             @RequestParam("pageSize") Integer pageSize,
                                             @RequestParam("searchQuery") String searchQuery) {
        return profileService.getOperatorList(pageNumber, pageSize, searchQuery);
    }

    @RequestMapping("/profileView/customers")
    @ResponseBody
    public Page<ProfileView> getCustomerList(@RequestParam("pageNumber") Integer pageNumber,
                                             @RequestParam("pageSize") Integer pageSize,
                                             @RequestParam("searchQuery") String searchQuery) {
        return profileService.getCustomerList(pageNumber, pageSize, searchQuery);
    }

    @RequestMapping("/profileView/profile")
    @ResponseBody
    public Profile getProfile(@RequestParam("identityId") Long identityId) {
        return profileService.getProfile(identityId);
    }

    @RequestMapping(value = "/profile/save", method = RequestMethod.POST)
    @ResponseBody
    public Profile saveProfile(@RequestBody Profile profile) {
        return profileService.saveProfile(profile);
    }

}
