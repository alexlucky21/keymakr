package com.keymakr.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "IDENTITY")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(doNotUseGetters = true)
@EqualsAndHashCode(of = {"id"})
@Builder
@JsonIgnoreProperties({"profile", "password"})
public class Identity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDENTITY_ID")
    private Long id;

    @Column(name = "USERNAME", nullable = false, length = 128)
    private String username;

    @Column(name = "PASSWORD", nullable = false, length = 128)
    private String password;

    @ManyToOne
    @JoinColumn(name = "ROLE_ID", nullable = false)
    private Role role;

    @OneToOne(mappedBy="identity")
    public Profile profile;

}
