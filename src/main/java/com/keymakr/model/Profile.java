package com.keymakr.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "PROFILE")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(doNotUseGetters = true)
@Builder
public class Profile implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PROFILE_ID")
    private Long id;

    @OneToOne
    @JoinColumn(name="IDENTITY_ID", nullable = false)
    Identity identity;

    @Column(name = "FIRST_NAME", nullable = false, length = 256)
    private String firstName;

    @Column(name = "LAST_NAME", nullable = false, length = 256)
    private String lastName;

    @Column(name = "EMAIL_ADDRESS", nullable = false, length = 256)
    private String emailAddress;

    @Column(name = "PHONE", length = 128)
    private String phone;

}
