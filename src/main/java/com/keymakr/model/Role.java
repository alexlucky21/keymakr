package com.keymakr.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "ROLE")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(doNotUseGetters = true)
@EqualsAndHashCode(of = {"id"})
@Builder
public class Role implements Serializable {

    public static final Long CUSTOMER = 1L;
    public static final Long OPERATOR = 2L;
    public static final Long CUSTOMER_ADMIN = 3L;
    public static final Long OPERATOR_ADMIN = 4L;
    public static final Long MAIN_ADMIN = 5L;

    @Id
    @Column(name = "ROLE_ID")
    private Long id;

    @Column(name = "NAME", nullable = false, length = 128)
    private String name;

    @Column(name = "DESCRIPTION", length = 1024)
    private String description;

}
