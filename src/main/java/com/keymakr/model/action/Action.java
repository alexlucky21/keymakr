package com.keymakr.model.action;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "ACTION")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(doNotUseGetters = true)
@EqualsAndHashCode(of = {"id"})
@Builder
@JsonIgnoreProperties({"parameters", "deletedAt"})
@Where(clause="deleted_at > CURRENT_TIMESTAMP")
@SQLDelete(sql = "UPDATE action SET deleted_at = CURRENT_TIMESTAMP WHERE action_id = ?")
public class Action {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ACTION_ID")
    private Long id;

    @Column(name = "NAME", nullable = false, length = 128)
    private String name;

    @Column(name = "METADATA")
    private String metadata;

    @Column(name = "PREDEFINED")
    private Boolean predefined;

    @OneToMany(mappedBy="action", fetch = FetchType.LAZY)
    private Set<ActionParameter> parameters;

    @Column(name = "DELETED_AT", nullable = false, columnDefinition = "TIMESTAMP DEFAULT '9999-12-31 00:00:00'", insertable = false, updatable = false)
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date deletedAt;
}