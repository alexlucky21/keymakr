package com.keymakr.model.action;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "ACTION_PARAMETER")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(doNotUseGetters = true)
@EqualsAndHashCode(of = {"id"})
@Builder
@JsonIgnoreProperties({"deletedAt"})
@Where(clause="deleted_at > CURRENT_TIMESTAMP")
@SQLDelete(sql = "UPDATE action_parameter SET deleted_at = CURRENT_TIMESTAMP WHERE action_parameter_id = ?")
public class ActionParameter {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ACTION_PARAMETER_ID")
    private Long id;

    @Column(name = "NAME", nullable = false, length = 128)
    private String name;

    @ManyToOne
    @JoinColumn(name="ACTION_ID", nullable = false)
    private Action action;

    @Column(name = "DELETED_AT", nullable = false, columnDefinition = "TIMESTAMP DEFAULT '9999-12-31 00:00:00'", insertable = false, updatable = false)
    private Date deletedAt;
}
