package com.keymakr.model.action;

import com.keymakr.model.camera.Camera;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(
        name = "MTM_CAMERA2ACTION",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"CAMERA_ID", "ACTION_ID"})})
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(doNotUseGetters = true)
@EqualsAndHashCode(of = {"id"})
@Builder
public class CameraAction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CAMERA2ACTION_ID")
    private Long id;

    @ManyToOne
    @JoinColumn(name="CAMERA_ID", nullable = false)
    private Camera camera;

    @ManyToOne
    @JoinColumn(name="ACTION_ID", nullable = false)
    private Action action;
}
