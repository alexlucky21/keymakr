package com.keymakr.model.application;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "APPLICATION")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(doNotUseGetters = true)
@EqualsAndHashCode(of = {"id"})
@Builder
@JsonIgnoreProperties({"applicationLocations", "applicationDefaultViewingHours"})
public class Application implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "APPLICATION_ID")
    private Long id;

    @Column(name = "APPLICATION_DATE", nullable = false)
    private Date applicationDate;

    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
    @JoinColumn(name = "APPLICATION_STATUS_ID", nullable = false)
    private ApplicationStatus applicationStatus;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="APPLICATION_PROFILE_ID", nullable = false)
    private ApplicationProfile applicationProfile;

    @Column(name = "DECLINE_REASON")
    private String declineReason;

    @OneToMany(mappedBy="ownerApplication", fetch = FetchType.LAZY)
    private Set<ApplicationLocation> applicationLocations;

    @OneToMany(mappedBy="application", fetch = FetchType.LAZY, orphanRemoval = true)
    private Set<ApplicationDefaultViewingHours> applicationDefaultViewingHours;
}
