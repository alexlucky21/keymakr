package com.keymakr.model.application;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "APPLICATION_CAMERA")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(doNotUseGetters = true)
@EqualsAndHashCode(of = {"id", "name"})
@Builder
@JsonIgnoreProperties({"applicationCameraViewingHours", "deletedAt"})
public class ApplicationCamera implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "APPLICATION_CAMERA_ID")
    private Long id;

    @Column(name = "NAME", nullable = false, length = 128)
    private String name;

    @Column(name = "COMMENT", length = 256)
    private String comment;

    @ManyToOne
    @JoinColumn(name="APPLICATION_LOCATION_ID", nullable = false)
    private ApplicationLocation applicationLocation;

    @Column(name = "URL")
    private String url;

    @Column(name = "LOGIN", length = 128)
    private String login;

    @Column(name = "PASSWORD", length = 128)
    private String password;

    @Column(name = "USE_DEFAULT_VIEWING_HOURS")
    private Boolean useDefaultViewingHours;

    @OneToMany(mappedBy="applicationCamera", fetch = FetchType.LAZY)
    private Set<ApplicationCameraViewingHours> applicationCameraViewingHours;

    @Column(name = "ACTIVATION_DATE")
    private Date activationDate;

    @Column(name = "DELETED_AT")
    private Date deletedAt;
}
