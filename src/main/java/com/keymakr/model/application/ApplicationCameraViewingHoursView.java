package com.keymakr.model.application;

import lombok.*;
import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.Subselect;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Time;
import java.time.DayOfWeek;

@Entity
@Immutable
@Subselect("select * from V_APPLICATION_CAMERA_VIEWING_HOURS")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(doNotUseGetters = true)
@Builder
public class ApplicationCameraViewingHoursView implements Serializable {

    @Id
    @Column(name = "V_APPLICATION_CAMERA_VIEWING_HOURS_ID")
    private Long id;

    @Column(name="APPLICATION_CAMERA_ID")
    private Long applicationCameraId;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "DAY_OF_WEEK")
    private DayOfWeek dayOfWeek;

    @Column(name="APPLICATION_CAMERA_VIEWING_HOURS_ID")
    private Long applicationCameraViewingHoursId;

    @Column(name = "FROM_TIME")
    private Time fromTime;

    @Column(name = "TO_TIME")
    private Time toTime;

    @Column(name = "ACTIVE")
    private boolean active;
}
