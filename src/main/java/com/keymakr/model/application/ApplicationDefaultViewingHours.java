package com.keymakr.model.application;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.io.Serializable;
import java.sql.Time;
import java.time.DayOfWeek;
import java.util.Date;

@Entity
@Table(name = "APPLICATION_DEFAULT_VIEWING_HOURS")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(doNotUseGetters = true)
@Builder
@JsonIgnoreProperties({"deletedAt"})
public class ApplicationDefaultViewingHours implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "APPLICATION_DEFAULT_VIEWING_HOURS_ID")
    private Long id;

    @ManyToOne
    @JoinColumn(name="APPLICATION_ID", nullable = false)
    private Application application;

    @Column(name = "DAY_OF_WEEK", nullable = false)
    private DayOfWeek dayOfWeek;

    @Column(name = "FROM_TIME", nullable = false)
    private Time fromTime;

    @Column(name = "TO_TIME", nullable = false)
    private Time toTime;

    @Column(name = "DELETED_AT")
    private Date deletedAt;
}
