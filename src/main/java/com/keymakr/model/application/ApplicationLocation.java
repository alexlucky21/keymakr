package com.keymakr.model.application;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "APPLICATION_LOCATION")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(doNotUseGetters = true)
@Builder
@JsonIgnoreProperties({"applicationCameras"})
public class ApplicationLocation implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "APPLICATION_LOCATION_ID")
    private Long id;

    @Column(name = "NAME", nullable = false, length = 128)
    private String name;

    @Column(name = "ADDRESS", nullable = false, length = 256)
    private String address;

    @Column(name = "TIME_ZONE", nullable = false, length = 64)
    private String timezone;

    @Column(name = "COMMENT", length = 256)
    private String comment;

    @ManyToOne
    @JoinColumn(name="APPLICATION_ID", nullable = false)
    private Application ownerApplication;

    @OneToMany(mappedBy="applicationLocation", fetch = FetchType.LAZY, orphanRemoval = true)
    private Set<ApplicationCamera> applicationCameras;
}
