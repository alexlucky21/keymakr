package com.keymakr.model.application;

import com.keymakr.model.Identity;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "APPLICATION_PROFILE")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(doNotUseGetters = true)
@Builder
public class ApplicationProfile  implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "APPLICATION_PROFILE_ID")
    private Long id;

    @Column(name = "FIRST_NAME", nullable = false, length = 256)
    private String firstName;

    @Column(name = "LAST_NAME", nullable = false, length = 256)
    private String lastName;

    @Column(name = "EMAIL_ADDRESS", nullable = false, length = 256)
    private String emailAddress;

    @Column(name = "PHONE", length = 128)
    private String phone;
}
