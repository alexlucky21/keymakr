package com.keymakr.model.application;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "APPLICATION_PURCHASED_HOURS")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(doNotUseGetters = true)
@Builder
public class ApplicationPurchasedHours implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "APPLICATION_PURCHASED_HOURS_ID")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "APPLICATION_ID", nullable = false)
    private Application application;

    @Column(name = "PURCHASE_DATE", nullable = false)
    private Date purchaseDate;

    @Column(name = "AMOUNT_PURCHASED_HOURS", nullable = false)
    private Integer amountPurchasedHours;
}