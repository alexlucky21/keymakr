package com.keymakr.model.application;

import lombok.*;
import org.hibernate.annotations.Subselect;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Subselect("select * from V_APPLICATION_PURCHASED_HOURS")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(doNotUseGetters = true)
@Builder
public class ApplicationPurchasedHoursView implements Serializable {

    @Id
    @Column(name = "APPLICATION_ID")
    private Long applicationId;

    @Column(name = "PURCHASED_HOURS")
    private Integer purchasedHours;
}
