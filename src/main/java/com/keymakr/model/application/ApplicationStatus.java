package com.keymakr.model.application;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "APPLICATION_STATUS")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(doNotUseGetters = true)
@EqualsAndHashCode(of = {"id"})
@Builder
public class ApplicationStatus implements Serializable {

    public static final Long NEW = 1L;
    public static final Long READ = 2L;
    public static final Long DELETED = 3L;
    public static final Long APPLIED = 4L;

    @Id
    @Column(name = "APPLICATION_STATUS_ID")
    private Long id;

    @Column(name = "NAME", nullable = false, length = 128)
    private String name;

    @Column(name = "DESCRIPTION", length = 1024)
    private String description;
}
