package com.keymakr.model.camera;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "CAMERA")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(doNotUseGetters = true)
@EqualsAndHashCode(of = {"id"})
@Builder
@JsonIgnoreProperties({"cameraOperators"})
public class Camera implements Serializable {

    public enum State { STREAMING, PENDING, DISCONNECTED }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CAMERA_ID")
    private Long id;

    @Column(name = "NAME", nullable = false, length = 128)
    private String name;

    @Column(name = "COMMENT", nullable = false, length = 256)
    private String comment;

    @Column(name = "STATE", nullable = false)
    private State state = State.DISCONNECTED;

    @ManyToOne
    @JoinColumn(name="LOCATION_ID", nullable = false)
    private Location location;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "camera", cascade = CascadeType.REMOVE)
    private List<CameraOperator> cameraOperators;

    @Column(name = "URL")
    private String url;

    @Column(name = "LOGIN", length = 128)
    private String login;

    @Column(name = "PASSWORD", length = 128)
    private String password;
}
