package com.keymakr.model.camera;

import lombok.*;
import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.Subselect;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Immutable
@Subselect("SELECT * FROM v_camera_assignment")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(doNotUseGetters = true)
@Builder
public class CameraAssignment implements Serializable {
    @Id
    @Column(name = "CAMERA_ID")
    private Long id;

    @OneToOne
    @JoinColumn(name="CAMERA_ID", nullable = false)
    Camera camera;

    @Column(name = "TOTAL")
    private Integer total;
}