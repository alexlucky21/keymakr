package com.keymakr.model.camera;

import lombok.*;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(doNotUseGetters = true)
@Builder
public class CameraFilter implements Serializable {

    private Long operatorId;

    private Long locationId;

    private Long cameraId;

    private Long customerId;

    private Camera.State state;

    private Boolean hasAssignments;

    private String searchString;
}
