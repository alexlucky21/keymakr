package com.keymakr.model.camera;

import com.keymakr.model.operator.Operator;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Entity keeps association between cameras and operators.
 * If property {@code isMain} is true then operator is assigned to main time line.
 * {@code assignAt} property store date of assigning operator to camera.
 * {@code unassignAt} - date of un-assigning/re-assigning operator from camera.
 * {@code permanentOperator} - in case temporary re-assignment stores link to operator who
 * is permanently assigned to the camera. For tracking history of changes we do not update
 * records but we unassigne in old record and assign in new record
 */

@Entity
@Table(
        name = "MTM_CAMERA2OPERATOR",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"CAMERA_ID", "OPERATOR_ID", "UNASSIGNED_AT"})}
)
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(doNotUseGetters = true)
@EqualsAndHashCode(of = {"id"})
@Builder
public class CameraOperator implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CAMERA2OPERATOR_ID")
    private Long id;

    @Column(name = "IS_MAIN", nullable = false)
    private Boolean isMain = false;

    @ManyToOne
    @JoinColumn(name = "OPERATOR_ID", nullable = false)
    private Operator operator;

    @ManyToOne
    @JoinColumn(name = "CAMERA_ID", nullable = false)
    private Camera camera;

    @ManyToOne
    @JoinColumn(name = "PERMANENT_OPERATOR_ID", referencedColumnName = "OPERATOR_ID")
    private Operator permanentOperator;

    @Column(name = "ASSIGNED_AT", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date assignedAt = new Date();

    @Column(name = "UNASSIGNED_AT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date unassignedAt;

}
