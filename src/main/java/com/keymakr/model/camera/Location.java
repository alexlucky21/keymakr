package com.keymakr.model.camera;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.keymakr.model.customer.Customer;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "LOCATION")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(doNotUseGetters = true)
@Builder
@JsonIgnoreProperties({"cameras"})
public class Location implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "LOCATION_ID")
    private Long id;

    @Column(name = "NAME", nullable = false, length = 128)
    private String name;

    @Column(name = "ADDRESS", nullable = false, length = 256)
    private String address;

    @Column(name = "TIME_ZONE", nullable = false, length = 64)
    private String timezone;

    @Column(name = "COMMENT", length = 256)
    private String comment;

    @ManyToOne
    @JoinColumn(name="CUSTOMER_ID", nullable = false)
    private Customer owner;

    @OneToMany(mappedBy="location", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Camera> cameras;
}
