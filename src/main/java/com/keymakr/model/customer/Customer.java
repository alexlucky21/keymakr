package com.keymakr.model.customer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.keymakr.model.Profile;
import com.keymakr.model.camera.Location;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "CUSTOMER")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(doNotUseGetters = true)
@EqualsAndHashCode(of = {"id"})
@Builder
@JsonIgnoreProperties({"locations"})
public class Customer implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CUSTOMER_ID")
    private Long id;

    @Column(name = "REGISTRATION_DATE")
    private Date registrationDate;

    @OneToOne
    @JoinColumn(name="PROFILE_ID", nullable = false)
    private Profile profile;

    @Column(name = "CUSTOMER_STATUS")
    private CustomerStatus customerStatus;

    @OneToMany(mappedBy="owner", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Location> locations;
}