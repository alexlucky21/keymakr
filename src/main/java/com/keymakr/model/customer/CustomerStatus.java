package com.keymakr.model.customer;

public enum CustomerStatus {
    ACTIVE, DELETED
}
