package com.keymakr.model.log;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.keymakr.model.action.ActionParameter;
import com.keymakr.model.action.CameraAction;
import com.keymakr.model.operator.Operator;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "ACTION_LOG")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(doNotUseGetters = true)
@EqualsAndHashCode(of = {"id"})
@Builder
public class ActionLog {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ACTION_LOG_ID")
    private Long id;

    @Column(name = "DATE_TIME_START", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateTimeStart;

    @ManyToOne
    @JoinColumn(name="OPERATOR_ID", nullable = false)
    private Operator operator;

    @Column(name = "IS_MAIN", nullable = false)
    private Boolean isMain;

    @ManyToOne
    @JoinColumn(name="CAMERA_ACTION_ID", nullable = false)
    private CameraAction cameraAction;

    @ManyToOne
    @JoinColumn(name="ACTION_PARAMETER_ID")
    private ActionParameter actionParameter;
}
