package com.keymakr.model.log;

import lombok.*;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(doNotUseGetters = true)
@Builder
public class ActionLogFilter {
    private Long locationId;

    private Date from;

    private Date to;

    private String streamName;

    private String actionName;

    private String operatorName;

    private Boolean isMain;
}
