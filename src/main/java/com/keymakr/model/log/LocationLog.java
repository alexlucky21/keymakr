package com.keymakr.model.log;

import com.keymakr.model.camera.Location;
import lombok.*;
import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.Subselect;

import javax.persistence.*;
import java.util.Date;

@Entity
@Immutable
@Subselect("SELECT * FROM v_location_log")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(doNotUseGetters = true)
@EqualsAndHashCode(of = {"id"})
@Builder
public class LocationLog {
    @Id
    @Column(name = "LOCATION_ID")
    private Long id;

    @OneToOne
    @JoinColumn(name="LOCATION_ID", nullable = false)
    private Location location;

    @Column(name = "LAST_LOG_AT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateTimeLastLog;
}
