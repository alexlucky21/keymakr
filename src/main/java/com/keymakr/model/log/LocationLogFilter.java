package com.keymakr.model.log;

import lombok.*;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(doNotUseGetters = true)
@Builder
public class LocationLogFilter {
    private Date from;

    private Date to;

    private String locationName;

    private Long customerId;

    private Long cameraId;

    private Long operatorId;
}
