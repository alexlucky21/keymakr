package com.keymakr.model.operator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.keymakr.model.Profile;
import com.keymakr.model.camera.CameraOperator;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "OPERATOR")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(doNotUseGetters = true)
@EqualsAndHashCode(of = {"id"})
@Builder
@JsonIgnoreProperties({"cameraOperators"})
public class Operator implements Serializable {

    public enum State { WORKING, PAUSED, NOT_WORKING }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "OPERATOR_ID")
    private Long id;

    @Column(name = "STATE", nullable = false, length = 16)
    private State state = State.NOT_WORKING;

    @OneToOne
    @JoinColumn(name="PROFILE_ID", nullable = false)
    private Profile profile;

    @OneToMany(mappedBy="operator", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private List<CameraOperator> cameraOperators;
}
