package com.keymakr.model.operator;

import lombok.*;
import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.Subselect;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Immutable
@Subselect("SELECT * FROM v_operator_assignment")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(doNotUseGetters = true)
@Builder
public class OperatorAssignment implements Serializable {
    @Id
    @Column(name = "OPERATOR_ID")
    private Long id;

    @OneToOne
    @JoinColumn(name="OPERATOR_ID", nullable = false)
    Operator operator;

    @Column(name = "TOTAL")
    private Integer total;

    @Column(name = "MAIN_TOTAL")
    private Integer mainTotal;

}