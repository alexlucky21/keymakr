package com.keymakr.model.operator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.keymakr.model.camera.CameraOperator;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "OPERATOR_REQUEST")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(doNotUseGetters = true)
@EqualsAndHashCode(of = {"id"})
@Builder
@JsonIgnoreProperties({"cameraOperators"})
public class OperatorRequest {

    public enum RequestType {TEMPORARY, PERMANENT}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "OPERATOR_REQUEST_ID")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "CAMERA2OPERATOR_ID", nullable = false)
    private CameraOperator fromCamera2Operator;

    @Column(name = "REQUEST_TYPE", nullable = false)
    private RequestType requestType = RequestType.PERMANENT;

    @Column(name = "REQUESTED_AT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date requestedAt = new Date();

    @ManyToOne
    @JoinColumn(name = "NEW_OPERATOR2CAMERA_ID", referencedColumnName = "CAMERA2OPERATOR_ID")
    private CameraOperator toCamera2Operator;

    @Column(name = "CHANGED_AT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date changedAt;

    @Column(name = "IS_VIEWED", nullable = false)
    private Boolean isRequestViewed = false;
}
