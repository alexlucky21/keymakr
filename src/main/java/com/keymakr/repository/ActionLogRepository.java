package com.keymakr.repository;

import com.keymakr.model.log.ActionLog;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ActionLogRepository extends CrudRepository<ActionLog, Long>, JpaSpecificationExecutor<ActionLog> {

}
