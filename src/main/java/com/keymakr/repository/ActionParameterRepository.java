package com.keymakr.repository;

import com.keymakr.model.action.ActionParameter;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ActionParameterRepository extends CrudRepository<ActionParameter, Long> {

    List<ActionParameter> findByActionId(Long actionId);
}
