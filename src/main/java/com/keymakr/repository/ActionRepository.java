package com.keymakr.repository;

import com.keymakr.model.action.Action;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ActionRepository extends CrudRepository<Action, Long> {

    List<Action> findByPredefinedTrueOrderById();
}
