package com.keymakr.repository;

import com.keymakr.model.camera.CameraAssignment;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

public interface CameraAssignmentRepository extends CrudRepository<CameraAssignment, Long>, JpaSpecificationExecutor<CameraAssignment> {

}
