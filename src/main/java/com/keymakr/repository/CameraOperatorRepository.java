package com.keymakr.repository;

import com.keymakr.model.camera.CameraOperator;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CameraOperatorRepository extends CrudRepository<CameraOperator, Long>, JpaSpecificationExecutor<CameraOperator> {
    List<CameraOperator> findByOperatorIdAndUnassignedAtIsNull(Long operatorId);

    List<CameraOperator> findByCameraIdAndUnassignedAtIsNull(Long cameraId);

    List<CameraOperator> findByCameraIdAndIsMainAndUnassignedAtIsNull(Long cameraId, Boolean isMain);

    CameraOperator findOneByOperatorIdAndCameraIdAndUnassignedAtIsNull(Long operatorId,Long cameraId);
}
