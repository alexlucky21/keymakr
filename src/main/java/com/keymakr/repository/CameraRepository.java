package com.keymakr.repository;

import com.keymakr.model.camera.Camera;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CameraRepository extends CrudRepository<Camera, Long>, JpaSpecificationExecutor<Camera> {
    List<Camera> findAll();
}
