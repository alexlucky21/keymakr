package com.keymakr.repository;

import com.keymakr.model.customer.Customer;
import com.keymakr.model.customer.CustomerStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CustomerRepository extends PagingAndSortingRepository<Customer, Long> {

    List<Customer> findAll();

    @Query("SELECT " +
            "       cstm " +
            "   FROM " +
            "       Location l, Customer cstm " +
            "   WHERE " +
            "       l.id = :locationId AND l.owner.id = cstm.id")
    Customer findByLocationId(@Param("locationId") Long locationId);

    @Query("SELECT " +
            "       cstm " +
            "   FROM " +
            "       Camera c, Location l, Customer cstm " +
            "   WHERE " +
            "       c.id = :cameraId AND c.location.id = l.id AND cstm.id = l.owner.id")
    Customer findByCameraId(@Param("cameraId") Long cameraId);

    @Query("SELECT cstm FROM Customer cstm WHERE ((cstm.customerStatus <> :customerStatus) AND (UPPER(CONCAT(cstm.profile.firstName, cstm.profile.lastName, cstm.profile.emailAddress)) like :searchString))")
    Page<Customer> findByNameOrEmailAddressAndCustomerStatusNot(Pageable pageRequest,  @Param("searchString") String searchString, @Param("customerStatus") CustomerStatus customerStatus);

    Page<Customer> findByCustomerStatusNot(Pageable pageRequest, CustomerStatus customerStatus);
}
