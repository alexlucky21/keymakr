package com.keymakr.repository;

import com.keymakr.model.Identity;
import org.springframework.data.repository.CrudRepository;

public interface IdentityRepository extends CrudRepository<Identity, Long> {

    Identity findByUsername(String username);

}
