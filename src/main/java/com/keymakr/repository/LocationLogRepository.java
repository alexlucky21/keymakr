package com.keymakr.repository;

import com.keymakr.model.log.LocationLog;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

public interface LocationLogRepository extends CrudRepository<LocationLog, Long>, JpaSpecificationExecutor<LocationLog> {

}
