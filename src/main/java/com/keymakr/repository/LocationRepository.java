package com.keymakr.repository;

import com.keymakr.model.camera.Location;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface LocationRepository extends CrudRepository<Location, Long> {

    List<Location> findAll();

    List<Location> findByOwnerId(Long customerId);

    @Query("SELECT " +
            "       l " +
            "   FROM " +
            "       Camera c, Location l " +
            "   WHERE " +
            "       c.id = :cameraId AND c.location.id = l.id")
    Location findLocationByCameraId(@Param("cameraId") Long cameraId);



}
