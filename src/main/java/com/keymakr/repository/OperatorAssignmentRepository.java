package com.keymakr.repository;

import com.keymakr.model.operator.OperatorAssignment;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

public interface OperatorAssignmentRepository extends CrudRepository<OperatorAssignment, Long>, JpaSpecificationExecutor<OperatorAssignment> {

}
