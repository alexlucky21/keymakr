package com.keymakr.repository;

import com.keymakr.model.operator.Operator;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface OperatorRepository extends PagingAndSortingRepository<Operator, Long>, JpaSpecificationExecutor<Operator> {
    List<Operator> findAll();
}
