package com.keymakr.repository;

import com.keymakr.model.operator.OperatorRequest;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface OperatorRequestRepository extends PagingAndSortingRepository<OperatorRequest, Long>, JpaSpecificationExecutor<OperatorRequest> {
    List<OperatorRequest> findAll();
}
