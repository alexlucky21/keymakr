package com.keymakr.repository;

import com.keymakr.model.Profile;
import org.springframework.data.repository.CrudRepository;

public interface ProfileRepository extends CrudRepository<Profile, Long> {

    Profile findByIdentityId(Long identityId);

}
