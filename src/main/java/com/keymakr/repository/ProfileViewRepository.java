package com.keymakr.repository;

import com.keymakr.model.ProfileView;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProfileViewRepository extends PagingAndSortingRepository<ProfileView, Long> {

    Page<ProfileView> findByRoleId(Pageable pageable, Long roleId);

    Page<ProfileView> findByRoleIdIn(Pageable pageable, List<Long> roleId);

    @Query("SELECT pv FROM ProfileView pv WHERE (UPPER(pv.name) like :name or UPPER(pv.emailAddress) like :email) AND pv.roleId in :roleIds")
    Page<ProfileView> findByNameOrEmailAddressAndRoleIdIn(Pageable pageable, @Param("name") String name, @Param("email") String email, @Param("roleIds") List<Long> roleIds);

    @Query("SELECT pv FROM ProfileView pv WHERE (UPPER(CONCAT(pv.name, pv.emailAddress)) like :searchString) AND pv.roleId = :roleId")
    Page<ProfileView> findByNameOrEmailAddressAndRoleId(Pageable pageable, @Param("searchString") String searchString, @Param("roleId") Long roleId);

}
