package com.keymakr.repository;

import com.keymakr.model.Role;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RoleRepository extends CrudRepository<Role, Long> {

    List<Role> findByIdIn(List<Long> roleIds);
    List<Role> findAll();

}
