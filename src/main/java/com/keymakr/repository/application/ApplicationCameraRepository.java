package com.keymakr.repository.application;

import com.keymakr.model.application.ApplicationCamera;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ApplicationCameraRepository extends CrudRepository<ApplicationCamera, Long> {

    List<ApplicationCamera> findByApplicationLocationIdAndDeletedAtIsNullOrderById(Long applicationLocationId);
}
