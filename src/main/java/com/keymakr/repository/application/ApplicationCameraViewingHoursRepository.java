package com.keymakr.repository.application;

import com.keymakr.model.application.ApplicationCameraViewingHours;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ApplicationCameraViewingHoursRepository extends CrudRepository<ApplicationCameraViewingHours, Long> {

    List<ApplicationCameraViewingHours> findByApplicationCameraIdAndDeletedAtIsNull(Long applicationCameraId);
}
