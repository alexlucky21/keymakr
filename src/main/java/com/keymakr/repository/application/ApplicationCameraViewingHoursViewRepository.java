package com.keymakr.repository.application;

import com.keymakr.model.application.ApplicationCameraViewingHoursView;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ApplicationCameraViewingHoursViewRepository extends CrudRepository<ApplicationCameraViewingHoursView, Long> {

    List<ApplicationCameraViewingHoursView> findByApplicationCameraId(Long applicationCameraId);
}
