package com.keymakr.repository.application;

import com.keymakr.model.application.ApplicationDefaultViewingHours;
import org.springframework.data.repository.CrudRepository;

public interface ApplicationDefaultViewingHoursRepository extends CrudRepository<ApplicationDefaultViewingHours, Long> {
}
