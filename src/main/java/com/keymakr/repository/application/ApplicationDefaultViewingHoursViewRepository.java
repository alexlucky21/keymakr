package com.keymakr.repository.application;

import com.keymakr.model.application.ApplicationDefaultViewingHoursView;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ApplicationDefaultViewingHoursViewRepository extends CrudRepository<ApplicationDefaultViewingHoursView, Long> {

    List<ApplicationDefaultViewingHoursView> findByApplicationId(Long applicationId);
}
