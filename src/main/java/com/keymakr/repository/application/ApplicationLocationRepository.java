package com.keymakr.repository.application;

import com.keymakr.model.application.ApplicationLocation;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ApplicationLocationRepository  extends CrudRepository<ApplicationLocation, Long> {

    List<ApplicationLocation> findByOwnerApplicationId(Long ownerApplicationId);
}
