package com.keymakr.repository.application;

import com.keymakr.model.application.ApplicationPurchasedHours;
import org.springframework.data.repository.CrudRepository;

public interface ApplicationPurchasedHoursRepository extends CrudRepository<ApplicationPurchasedHours, Long> {
}
