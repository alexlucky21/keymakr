package com.keymakr.repository.application;

import com.keymakr.model.application.ApplicationPurchasedHoursView;
import org.springframework.data.repository.CrudRepository;

public interface ApplicationPurchasedHoursViewRepository extends CrudRepository<ApplicationPurchasedHoursView, Long> {
}
