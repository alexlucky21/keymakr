package com.keymakr.repository.application;

import com.keymakr.model.application.Application;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ApplicationRepository extends PagingAndSortingRepository<Application, Long> {

    Page<Application> findByApplicationStatusIdNotIn(Pageable pageable, List<Long> applicationStatusId);
}
