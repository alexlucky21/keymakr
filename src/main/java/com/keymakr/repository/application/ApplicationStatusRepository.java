package com.keymakr.repository.application;

import com.keymakr.model.application.ApplicationStatus;
import org.springframework.data.repository.CrudRepository;

public interface ApplicationStatusRepository extends CrudRepository<ApplicationStatus, Long> {
}
