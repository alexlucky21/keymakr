package com.keymakr.repository.specification;


import com.keymakr.model.log.ActionLog;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Date;

public class ActionLogSpecs {

    public static Specification<ActionLog> alwaysTrue() {
        return new Specification<ActionLog>() {
            public Predicate toPredicate(Root<ActionLog> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                return builder.and();
            }
        };
    }

    public static Specification<ActionLog> isLogRecordAfter(Date from) {
        return new Specification<ActionLog>() {
            public Predicate toPredicate(Root<ActionLog> root, CriteriaQuery<?> query, CriteriaBuilder builder) {

                return builder.greaterThan(root.get("dateTimeStart"), from);
            }
        };
    }

    public static Specification<ActionLog> isLogRecordBefore(Date to) {
        return new Specification<ActionLog>() {
            public Predicate toPredicate(Root<ActionLog> root, CriteriaQuery<?> query, CriteriaBuilder builder) {

                return builder.lessThan(root.get("dateTimeStart"), to);
            }
        };
    }

    public static Specification<ActionLog> isLogRecordTimeline(Boolean isMain) {
        return new Specification<ActionLog>() {
            public Predicate toPredicate(Root<ActionLog> root, CriteriaQuery<?> query, CriteriaBuilder builder) {

                return builder.equal(root.get("isMain"), isMain);
            }
        };
    }

    public static Specification<ActionLog> doesStreamNameContain(String searchString) {
        return new Specification<ActionLog>() {
            public Predicate toPredicate(Root<ActionLog> root, CriteriaQuery<?> query, CriteriaBuilder builder) {

                return builder.like(builder.lower(root.get("cameraAction").get("camera").get("name")), searchString);
            }
        };
    }

    public static Specification<ActionLog> doesActionNameContain(String searchString) {
        return new Specification<ActionLog>() {
            public Predicate toPredicate(Root<ActionLog> root, CriteriaQuery<?> query, CriteriaBuilder builder) {

                return builder.like(builder.lower(root.get("cameraAction").get("action").get("name")), searchString);
            }
        };
    }


    public static Specification<ActionLog> doesOperatorNameContain(String searchString) {
        return new Specification<ActionLog>() {
            public Predicate toPredicate(Root<ActionLog> root, CriteriaQuery<?> query, CriteriaBuilder builder) {

                return builder.or(
                        builder.like(builder.lower(root.get("operator").get("profile").get("firstName")), searchString),
                        builder.like(builder.lower(root.get("operator").get("profile").get("lastName")), searchString));
            }
        };
    }

    public static Specification<ActionLog> doesStreamBelongToLocation(Long locationId) {
        return new Specification<ActionLog>() {
            public Predicate toPredicate(Root<ActionLog> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                query.distinct(true);

                return builder.equal(root.get("cameraAction").get("camera").get("location").get("id"), locationId);
            }
        };
    }
}
