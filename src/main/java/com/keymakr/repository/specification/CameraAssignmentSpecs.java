package com.keymakr.repository.specification;


import com.keymakr.model.camera.*;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;

public class CameraAssignmentSpecs {

    public static Specification<CameraAssignment> isFiltered(CameraFilter filter) {
        return new Specification<CameraAssignment>() {
            public Predicate toPredicate(Root<CameraAssignment> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                Join<CameraAssignment, Camera> cameras = root.join("camera");
                Join<CameraAssignment, Location> locations = cameras.join("location");
                Join<CameraAssignment, CameraOperator> cameraOperators = cameras.join("cameraOperators", JoinType.LEFT);
                Predicate predicate = builder.and();

                query.distinct(true);

                if (filter.getOperatorId() != null) {
                    predicate = builder.and(predicate, cameraOperators.get("unassigned_at").isNull(),
                                            builder.equal(cameraOperators.get("operator").get("id"), filter.getOperatorId() )
                    );
                }

                if (filter.getLocationId()  != null) {
                    predicate = builder.and(predicate, builder.equal(locations.get("id"), filter.getLocationId()));
                }

                if (filter.getCameraId() != null) {
                    predicate = builder.and(predicate, builder.equal(cameras.get("id"), filter.getCameraId() ));
                }

                if (filter.getCustomerId()  != null) {
                    predicate = builder.and(predicate, builder.equal(locations.get("owner").get("id"), filter.getCustomerId()));
                }

                if (filter.getState() != null) {
                    predicate = builder.and(predicate, builder.equal(cameras.get("state"), filter.getState()));
                }

                if (filter.getHasAssignments() != null) {
                    if (filter.getHasAssignments()) {
                        predicate = builder.and(predicate, builder.greaterThan(root.get("total"), 0));
                    }
                    else {
                        predicate = builder.and(predicate, builder.equal(root.get("total"), 0));
                    }

                }

                if (filter.getSearchString() != null && !filter.getSearchString().trim().isEmpty()) {
                    Predicate doesContainString;
                    String sqlSearchQuery = "%" + filter.getSearchString().trim().toLowerCase() + "%";

                    doesContainString = builder.or(
                            builder.like(builder.lower(cameras.get("name")), sqlSearchQuery),
                            builder.like(builder.lower(locations.get("name")), sqlSearchQuery),
                            builder.like(builder.lower(locations.get("address")), sqlSearchQuery),
                            builder.like(builder.lower(locations.get("owner").get("profile").get("firstName")), sqlSearchQuery),
                            builder.like(builder.lower(locations.get("owner").get("profile").get("lastName")), sqlSearchQuery),
                            builder.like(builder.lower(cameraOperators.get("operator").get("profile").get("firstName")), sqlSearchQuery),
                            builder.like(builder.lower(cameraOperators.get("operator").get("profile").get("lastName")), sqlSearchQuery));

                    predicate = builder.and(predicate, doesContainString);
                }

                return predicate;
            }
        };
    }
}
