package com.keymakr.repository.specification;


import com.keymakr.model.camera.Camera;
import com.keymakr.model.camera.CameraOperator;
import com.keymakr.model.camera.Location;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;

public class CameraOperatorSpecs {

    public static Specification<CameraOperator> alwaysTrue() {
        return new Specification<CameraOperator>() {
            public Predicate toPredicate(Root<CameraOperator> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                return builder.and();
            }
        };
    }

    public static Specification<CameraOperator> isRecordActive() {
        return new Specification<CameraOperator>() {
            public Predicate toPredicate(Root<CameraOperator> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                return root.get("unassigned_at").isNull();
            }
        };
    }

    public static Specification<CameraOperator> isCameraAssignedToOperator(Long operatorId) {
        return new Specification<CameraOperator>() {
            public Predicate toPredicate(Root<CameraOperator> root, CriteriaQuery<?> query, CriteriaBuilder builder) {

                return builder.equal(root.get("operator").get("id"), operatorId);
            }
        };
    }

    public static Specification<CameraOperator> doesCameraBelongToLocation(Long locationId) {
        return new Specification<CameraOperator>() {
            public Predicate toPredicate(Root<CameraOperator> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                return builder.equal(root.get("camera").get("location").get("id"), locationId);
            }
        };
    }

    public static Specification<CameraOperator> isCameraIdEqualsTo(Long cameraId) {
        return new Specification<CameraOperator>() {
            public Predicate toPredicate(Root<CameraOperator> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                return builder.equal(root.get("id"), cameraId);
            }
        };
    }

    public static Specification<CameraOperator> doesCameraBelongToCustomer(Long customerId) {
        return new Specification<CameraOperator>() {
            public Predicate toPredicate(Root<CameraOperator> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                return builder.equal(root.get("camera").get("location").get("owner").get("id"), customerId);
            }
        };
    }

    public static Specification<CameraOperator> isCameraState(Camera.State state) {
        return new Specification<CameraOperator>() {
            public Predicate toPredicate(Root<CameraOperator> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                return builder.equal(root.get("camera").get("state"), state);
            }
        };
    }

    public static Specification<CameraOperator> doesCameraHaveAnyAssignee() {
        return new Specification<CameraOperator>() {
            public Predicate toPredicate(Root<CameraOperator> root, CriteriaQuery<?> query, CriteriaBuilder builder) {

                return builder.greaterThan(root.get("total"), 0);

            }
        };
    }

    public static Specification<CameraOperator>  doesCustomerOrOperatorOrLocationOrCameraContain(String searchQuery) {
        return new Specification<CameraOperator>() {
            public Predicate toPredicate(Root<CameraOperator> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                Join<CameraOperator, Camera> cameras = root.join("camera");
                Join<CameraOperator, Location> locations = cameras.join("location");
                Join<CameraOperator, CameraOperator> cameraOperators = cameras.join("cameraOperators", JoinType.LEFT);

                Predicate doesContainString = builder.or(
                        builder.like(builder.lower(cameras.get("name")), searchQuery),
                        builder.like(builder.lower(locations.get("name")), searchQuery),
                        builder.like(builder.lower(locations.get("address")), searchQuery),
                        builder.like(builder.lower(locations.get("owner").get("profile").get("firstName")), searchQuery),
                        builder.like(builder.lower(locations.get("owner").get("profile").get("lastName")), searchQuery),
                        builder.like(builder.lower(cameraOperators.get("operator").get("profile").get("firstName")), searchQuery),
                        builder.like(builder.lower(cameraOperators.get("operator").get("profile").get("lastName")), searchQuery));

                query.distinct(true);

                return builder.and(doesContainString, cameraOperators.get("unassigned_at").isNull());
            }
        };
    }
}
