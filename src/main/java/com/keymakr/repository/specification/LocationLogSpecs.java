package com.keymakr.repository.specification;


import com.keymakr.model.camera.Camera;
import com.keymakr.model.camera.CameraOperator;
import com.keymakr.model.log.LocationLog;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.Date;

public class LocationLogSpecs {

    public static Specification<LocationLog> alwaysTrue() {
        return new Specification<LocationLog>() {
            public Predicate toPredicate(Root<LocationLog> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                query.distinct(true);

                return builder.and();
            }
        };
    }

    public static Specification<LocationLog> isLogRecordAfter(Date from) {
        return new Specification<LocationLog>() {
            public Predicate toPredicate(Root<LocationLog> root, CriteriaQuery<?> query, CriteriaBuilder builder) {

                return builder.greaterThan(root.get("dateTimeLastLog"), from);
            }
        };
    }

    public static Specification<LocationLog> isLogRecordBefore(Date to) {
        return new Specification<LocationLog>() {
            public Predicate toPredicate(Root<LocationLog> root, CriteriaQuery<?> query, CriteriaBuilder builder) {

                return builder.lessThan(root.get("dateTimeLastLog"), to);
            }
        };
    }

    public static Specification<LocationLog> doesLocationNameContain(String searchString) {
        return new Specification<LocationLog>() {
            public Predicate toPredicate(Root<LocationLog> root, CriteriaQuery<?> query, CriteriaBuilder builder) {

                return builder.like(builder.lower(root.get("location").get("name")), searchString);
            }
        };
    }

    public static Specification<LocationLog> doesLocationBelongCustomer(Long customerId) {
        return new Specification<LocationLog>() {
            public Predicate toPredicate(Root<LocationLog> root, CriteriaQuery<?> query, CriteriaBuilder builder) {

                return builder.equal(root.get("location").get("owner").get("id"), customerId);
            }
        };
    }

    public static Specification<LocationLog> doesLocationIncludeStream(Long cameraId) {
        return new Specification<LocationLog>() {
            public Predicate toPredicate(Root<LocationLog> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                Join<LocationLog, Camera> cameras = root.join("location").join("cameras");

                return builder.equal(cameras.get("id"), cameraId);
            }
        };
    }

    //TODO: fix this predicate because it works incorrectly
    public static Specification<LocationLog> doesLocationIncludeStreamAssignedToOperator(Long operatorId) {
        return new Specification<LocationLog>() {
            public Predicate toPredicate(Root<LocationLog> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                Join<LocationLog, CameraOperator> cameraOperators = root.join("location")
                                                                .join("cameras")
                                                                .join("cameraOperators");

                return builder.equal(cameraOperators.get("operator").get("id"), operatorId);
            }
        };
    }
}
