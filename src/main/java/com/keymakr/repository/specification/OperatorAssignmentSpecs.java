package com.keymakr.repository.specification;


import com.keymakr.model.Profile;
import com.keymakr.model.camera.Camera;
import com.keymakr.model.camera.CameraOperator;
import com.keymakr.model.camera.Location;
import com.keymakr.model.operator.Operator;
import com.keymakr.model.operator.OperatorAssignment;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.List;

public class OperatorAssignmentSpecs {

    public static Specification<OperatorAssignment> alwaysTrue() {
        return new Specification<OperatorAssignment>() {
            public Predicate toPredicate(Root<OperatorAssignment> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                return builder.and();
            }
        };
    }

    public static Specification<OperatorAssignment> isSuggested(Long locationId) {
        return new Specification<OperatorAssignment>() {
            public Predicate toPredicate(Root<OperatorAssignment> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                Join<OperatorAssignment, Location> locationJoin = root
                                                                    .join("operator")
                                                                    .join("cameraOperators", JoinType.LEFT)
                                                                    .join("camera", JoinType.LEFT)
                                                                    .join("location", JoinType.LEFT);
                return builder.or(
                        builder.equal(locationJoin.get("id"), locationId),
                        builder.equal(root.get("mainTotal"), 0)
                );
            }
        };
    }

    public static Specification<OperatorAssignment> isOther(Long locationId) {
        return new Specification<OperatorAssignment>() {
            public Predicate toPredicate(Root<OperatorAssignment> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                Subquery<Long> cameraOfOperatorFromLocationSQ = query.subquery(Long.class);
                Root<CameraOperator> sqRoot = cameraOfOperatorFromLocationSQ.from(CameraOperator.class);

                cameraOfOperatorFromLocationSQ
                        .select(sqRoot.get("camera").get("id"))
                        .where(
                                builder.and(
                                        builder.equal(sqRoot.get("camera").get("location").get("id"), locationId)),
                                        builder.equal(sqRoot.get("operator").get("id"), root.get("operator").get("id")),
                                        sqRoot.get("unassignedAt").isNull()
                                );
                return builder.and(
                        builder.not(builder.exists(cameraOfOperatorFromLocationSQ)),
                        builder.greaterThan(root.get("mainTotal"), 0)
                );
            }
        };
    }

    public static Specification<OperatorAssignment> isOperatorNotAssignedToCamera(Long cameraId) {
        return new Specification<OperatorAssignment>() {
            public Predicate toPredicate(Root<OperatorAssignment> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                Subquery<Long> assignedOperatorsSQ = query.subquery(Long.class);
                Root<CameraOperator> sqRoot = assignedOperatorsSQ.from(CameraOperator.class);

                assignedOperatorsSQ
                        .select(sqRoot.get("operator").get("id"))
                        .where(builder.and(
                                builder.equal(sqRoot.get("camera").get("id"), cameraId)),
                                sqRoot.get("unassignedAt").isNull()
                        );
                CriteriaBuilder.In in = builder.in(root.get("operator").<Long>get("id"));
                in.value(assignedOperatorsSQ);

                return builder.not(in);
            }
        };
    }

    public static Specification<OperatorAssignment> hasMainCameras() {
        return new Specification<OperatorAssignment>() {
            public Predicate toPredicate(Root<OperatorAssignment> root, CriteriaQuery<?> query, CriteriaBuilder builder) {

                return builder.notEqual(root.get("mainTotal"), 0);
            }
        };
    }

    public static Specification<OperatorAssignment> doesNameContain(String searchQuery) {
        return new Specification<OperatorAssignment>() {
            public Predicate toPredicate(Root<OperatorAssignment> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                String sqlSearchQuery = '%' + searchQuery + '%';
                Join<OperatorAssignment, Profile> profiles = root
                                                                .join("operator")
                                                                .join("profile");

                return builder.or(
                        builder.like(builder.lower(profiles.get("firstName")), sqlSearchQuery),
                        builder.like(builder.lower(profiles.get("lastName")), sqlSearchQuery),
                        builder.like(builder.lower(profiles.get("emailAddress")), sqlSearchQuery));
            }
        };
    }

    public static Specification<OperatorAssignment> hasAssignmentsNotGreaterThan(Integer amount) {
        return new Specification<OperatorAssignment>() {
            public Predicate toPredicate(Root<OperatorAssignment> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                query.distinct(true);
                query.orderBy(builder.asc(root.get("total")));

                return builder.lessThanOrEqualTo(root.get("total"), amount);
            }
        };
    }


    public static Specification<OperatorAssignment> isStateIn(List<Operator.State> states) {
        return new Specification<OperatorAssignment>() {
            public Predicate toPredicate(Root<OperatorAssignment> root, CriteriaQuery<?> query, CriteriaBuilder builder) {

                return root.get("operator").get("state").in(states);
            }
        };
    }

    public static Specification<OperatorAssignment> doesEmailOrNameContain(String searchQuery) {
        return new Specification<OperatorAssignment>() {
            public Predicate toPredicate(Root<OperatorAssignment> root, CriteriaQuery<?> query, CriteriaBuilder builder) {

                return builder.or(
                        builder.like(builder.lower(root.get("operator").get("profile").get("firstName")), searchQuery),
                        builder.like(builder.lower(root.get("operator").get("profile").get("lastName")), searchQuery),
                        builder.like(builder.lower(root.get("operator").get("profile").get("emailAddress")), searchQuery));
            }
        };
    }

    public static Specification<OperatorAssignment> isAssignedToCamera(Long cameraId) {
        return new Specification<OperatorAssignment>() {
            public Predicate toPredicate(Root<OperatorAssignment> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                Join<OperatorAssignment, Camera> cameras = root.join("operator").join("cameras");

                return builder.equal(cameras.get("cameraId"), cameraId);
            }
        };
    }
}
