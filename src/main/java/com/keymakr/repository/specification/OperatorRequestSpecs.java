package com.keymakr.repository.specification;


import com.keymakr.model.operator.OperatorRequest;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

public class OperatorRequestSpecs {

    public static Specification<OperatorRequest> alwaysTrue() {
        return new Specification<OperatorRequest>() {
            public Predicate toPredicate(Root<OperatorRequest> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                query.orderBy(builder.asc(root.get("isRequestViewed")));

                return builder.and();
            }
        };
    }

    public static Specification<OperatorRequest> isNotProcessed() {
        return new Specification<OperatorRequest>() {
            public Predicate toPredicate(Root<OperatorRequest> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                return root.get("changedAt").isNull();
            }
        };
    }

    public static Specification<OperatorRequest> isInList(List<Long> ids) {
        return new Specification<OperatorRequest>() {
            public Predicate toPredicate(Root<OperatorRequest> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                return root.get("id").in(ids);
            }
        };
    }

}
