package com.keymakr.service;

import com.keymakr.model.log.ActionLog;
import com.keymakr.model.log.ActionLogFilter;
import com.keymakr.repository.ActionLogRepository;
import com.keymakr.repository.specification.ActionLogSpecs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

import static org.springframework.data.jpa.domain.Specifications.where;

@Service
public class ActionLogService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private ActionLogRepository actionLogRepository;

    @Autowired
    public ActionLogService(ActionLogRepository actionLogRepository) {
        this.actionLogRepository = actionLogRepository;
    }

    public List<ActionLog> getActionLogs(ActionLogFilter filter) {
        Specifications<ActionLog> specs = makeSpec(filter);

        return actionLogRepository.findAll(specs);
    }


    public Page<ActionLog> getActionLogs(Integer pageNumber, Integer pageSize, ActionLogFilter filter) {
        PageRequest pageRequest = new PageRequest(pageNumber - 1, pageSize);
        Specifications<ActionLog> specs = makeSpec(filter);

        return actionLogRepository.findAll(specs, pageRequest);
    }

    private Specifications<ActionLog> makeSpec(ActionLogFilter filter) {
        Specifications<ActionLog> specs = where(ActionLogSpecs.doesStreamBelongToLocation(filter.getLocationId()));

        if (filter.getIsMain() != null) {
            specs = specs.and(ActionLogSpecs.isLogRecordTimeline(filter.getIsMain()));
        }

        if (filter.getFrom() != null) {
            specs = specs.and(ActionLogSpecs.isLogRecordAfter(filter.getFrom() ));
        }

        if (filter.getTo()  != null) {
            specs = specs.and(ActionLogSpecs.isLogRecordBefore(filter.getTo() ));
        }

        if (filter.getStreamName()  != null && !filter.getStreamName().trim().isEmpty()) {
            specs = specs.and(ActionLogSpecs.doesStreamNameContain('%' + filter.getStreamName().trim().toLowerCase() + '%'));
        }

        if (filter.getActionName() != null && !filter.getActionName() .trim().isEmpty()) {
            specs = specs.and(ActionLogSpecs.doesActionNameContain('%' + filter.getActionName() .trim().toLowerCase() + '%'));
        }

        if (filter.getOperatorName()  != null && !filter.getOperatorName() .trim().isEmpty()) {
            specs = specs.and(ActionLogSpecs.doesOperatorNameContain('%' + filter.getOperatorName() .trim().toLowerCase() + '%'));
        }

        return specs;
    }
}
