package com.keymakr.service;

import com.keymakr.model.action.ActionParameter;
import com.keymakr.repository.ActionParameterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class ActionParameterService {

    private ActionParameterRepository actionParameterRepository;

    @Autowired
    public ActionParameterService(ActionParameterRepository actionParameterRepository) {
        this.actionParameterRepository = actionParameterRepository;
    }

    public ActionParameter saveActionParameter(ActionParameter actionParameter) {
        return actionParameterRepository.save(actionParameter);
    }

    public List<ActionParameter> getActionParameterByActionId(Long actionId) {
        return actionParameterRepository.findByActionId(actionId);
    }

    public Long deleteActionParameter(Long actionParameterId) {
        actionParameterRepository.delete(actionParameterId);
        return actionParameterId;
    }
}
