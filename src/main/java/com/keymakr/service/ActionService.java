package com.keymakr.service;

import com.keymakr.model.action.Action;
import com.keymakr.repository.ActionParameterRepository;
import com.keymakr.repository.ActionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Service
public class ActionService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private ActionRepository actionRepository;
    private ActionParameterRepository actionParameterRepository;

    @Autowired
    public ActionService(ActionRepository actionRepository, ActionParameterRepository actionParameterRepository) {
        this.actionRepository = actionRepository;
        this.actionParameterRepository = actionParameterRepository;
    }

    public List<Action> getDefaultActions() {
        return actionRepository.findByPredefinedTrueOrderById();
    }

    public Action saveAction(Action action) {
        return actionRepository.save(action);
    }

    @Transactional
    public Long deleteAction(Long actionId) {
        actionParameterRepository.findByActionId(actionId).forEach(actionParameter -> {
            actionParameterRepository.delete(actionParameter);
        });
        actionRepository.delete(actionId);
        return actionId;
    }
}
