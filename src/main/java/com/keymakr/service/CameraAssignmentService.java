package com.keymakr.service;

import com.keymakr.model.camera.CameraAssignment;
import com.keymakr.model.camera.CameraFilter;
import com.keymakr.repository.CameraAssignmentRepository;
import com.keymakr.repository.specification.CameraAssignmentSpecs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CameraAssignmentService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private CameraAssignmentRepository cameraAssignmentRepository;

    @Autowired
    public CameraAssignmentService(CameraAssignmentRepository cameraAssignmentRepository) {
        this.cameraAssignmentRepository = cameraAssignmentRepository;
    }

    public List<CameraAssignment> getCameraAssignments(CameraFilter filter) {
        Specification<CameraAssignment> specs = CameraAssignmentSpecs.isFiltered(filter);

        return cameraAssignmentRepository.findAll(specs);
    }


    public Page<CameraAssignment> getCameraAssignments(Integer pageNumber, Integer pageSize, CameraFilter filter) {
        PageRequest pageRequest = new PageRequest(pageNumber - 1, pageSize);
        Specification<CameraAssignment> specs = CameraAssignmentSpecs.isFiltered(filter);

        return cameraAssignmentRepository.findAll(specs, pageRequest);
    }
}
