package com.keymakr.service;

import com.keymakr.model.camera.Camera;
import com.keymakr.model.camera.CameraAssignment;
import com.keymakr.model.camera.CameraFilter;
import com.keymakr.model.camera.CameraOperator;
import com.keymakr.model.operator.Operator;
import com.keymakr.model.operator.OperatorRequest;
import com.keymakr.repository.CameraOperatorRepository;
import com.keymakr.repository.CameraRepository;
import com.keymakr.repository.OperatorRepository;
import com.keymakr.repository.OperatorRequestRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class CameraService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private CameraRepository cameraRepository;

    private CameraOperatorRepository cameraOperatorRepository;

    private OperatorRepository operatorRepository;

    private CameraAssignmentService cameraAssignmentService;

    private OperatorRequestRepository operatorRequestRepository;

    @Autowired
    public CameraService(CameraRepository cameraRepository,
                         CameraOperatorRepository cameraOperatorRepository,
                         OperatorRepository operatorRepository,
                         OperatorRequestRepository operatorRequestRepository,
                         CameraAssignmentService cameraAssignmentService) {

        this.cameraRepository = cameraRepository;
        this.cameraOperatorRepository = cameraOperatorRepository;
        this.operatorRepository = operatorRepository;
        this.operatorRequestRepository = operatorRequestRepository;
        this.cameraAssignmentService = cameraAssignmentService;
    }

    public List<Camera> getCameras() {
        return cameraRepository.findAll();
    }


    public Page<Camera> getCameras(Integer pageNumber, Integer pageSize, CameraFilter filter) {
        return cameraAssignmentService.getCameraAssignments(pageNumber, pageSize, filter)
                .map(CameraAssignment::getCamera);
    }

    public Camera saveCamera(Camera camera) {
        return cameraRepository.save(camera);
    }

    public void deleteCamera(Long cameraId) {
        cameraRepository.delete(cameraId);
    }

    public Camera getCameraById(Long cameraId) {
        return cameraRepository.findOne(cameraId);
    }


    public List<Camera> getCamerasByCustomerId(CameraFilter filter) {
        return cameraAssignmentService.getCameraAssignments(filter)
                .stream()
                .map(CameraAssignment::getCamera).collect(Collectors.toList());
    }

    public List<Camera> getCamerasByLocationId(CameraFilter filter) {
        return cameraAssignmentService.getCameraAssignments(filter)
                .stream()
                .map(CameraAssignment::getCamera).collect(Collectors.toList());
    }

    public List<CameraOperator> getCameraOperatorsByOperatorId(Long operatorId) {
        return cameraOperatorRepository.findByOperatorIdAndUnassignedAtIsNull(operatorId);
    }

    public List<CameraOperator> updateOperatorsOfCamera(Long cameraId, List<CameraOperator> cameraOperators) {
        List<CameraOperator> cameraOperatorSaved = new ArrayList<>();
        List<CameraOperator> mainCameraOperators;
        List<CameraOperator> itemsToDelete;
        Long mainCameraOperatorId = 0L;

        itemsToDelete = cameraOperatorRepository.findByCameraIdAndUnassignedAtIsNull(cameraId);

        for (CameraOperator cameraOperator : cameraOperators) {
            Long operatorId = cameraOperator.getOperator().getId();
            CameraOperator existingCameraOperator = cameraOperatorRepository.findOneByOperatorIdAndCameraIdAndUnassignedAtIsNull(operatorId, cameraId);

            if (existingCameraOperator == null) {
                cameraOperator.setIsMain(cameraOperator.getIsMain() != null && cameraOperator.getIsMain());
                cameraOperator = cameraOperatorRepository.save(cameraOperator);
            } else {
                if (cameraOperator.getIsMain() != existingCameraOperator.getIsMain()) {
                    existingCameraOperator.setIsMain(cameraOperator.getIsMain());
                    existingCameraOperator = cameraOperatorRepository.save(cameraOperator);
                }
                cameraOperator = existingCameraOperator;
            }

            if (cameraOperator.getIsMain()) {
                mainCameraOperatorId = cameraOperator.getId();
            }
            cameraOperatorSaved.add(cameraOperator);


            itemsToDelete.remove(cameraOperator);
        }

        cameraOperatorRepository.delete(itemsToDelete);

        //Check if we have more than 1 main operator
        mainCameraOperators = cameraOperatorRepository.findByCameraIdAndIsMainAndUnassignedAtIsNull(cameraId, true);
        if (mainCameraOperators.size() > 1) {
            for (CameraOperator mainCameraOperator : mainCameraOperators) {
                if (!Objects.equals(mainCameraOperatorId, mainCameraOperator.getId())) {
                    mainCameraOperator.setIsMain(false);
                    cameraOperatorRepository.save(mainCameraOperator);
                }
            }
        }

        return cameraOperatorSaved;
    }

    public CameraOperator assignCameraToOperator(Long cameraId, Long operatorId, Long permanentOperatorId, Boolean isMain) {
        List<CameraOperator> mainCameraOperators;
        CameraOperator cameraOperator = cameraOperatorRepository.findOneByOperatorIdAndCameraIdAndUnassignedAtIsNull(operatorId, cameraId);

        if (cameraOperator != null) {
            if (cameraOperator.getIsMain() != isMain) {
                cameraOperator.setUnassignedAt(new Date());
                cameraOperatorRepository.save(cameraOperator);
            }
            else {
                return cameraOperator;
            }
        }

        mainCameraOperators = cameraOperatorRepository.findByCameraIdAndIsMainAndUnassignedAtIsNull(cameraId, true);
        if (isMain && mainCameraOperators != null && !mainCameraOperators.isEmpty()) {
            for (CameraOperator previousMainCameraOperator: mainCameraOperators) {
                CameraOperator previousMainCameraOperatorNotMain;

                previousMainCameraOperatorNotMain = CameraOperator
                        .builder()
                        .camera(previousMainCameraOperator.getCamera())
                        .operator(previousMainCameraOperator.getOperator())
                        .isMain(false)
                        .assignedAt(new Date())
                        .build();

                previousMainCameraOperator.setUnassignedAt(new Date());
                cameraOperatorRepository.save(previousMainCameraOperator);
                cameraOperatorRepository.save(previousMainCameraOperatorNotMain);
            }
        }

        cameraOperator = CameraOperator
                .builder()
                .camera(cameraRepository.findOne(cameraId))
                .operator(operatorRepository.findOne(operatorId))
                .permanentOperator(permanentOperatorId == null ? null : operatorRepository.findOne(permanentOperatorId))
                .isMain(isMain)
                .assignedAt(new Date())
                .build();

        return cameraOperatorRepository.save(cameraOperator);
    }

    public Long unassignCameraFromOperator(Long cameraId, Long operatorId) {
        CameraOperator cameraOperator =  cameraOperatorRepository.findOneByOperatorIdAndCameraIdAndUnassignedAtIsNull(operatorId, cameraId);

        if (cameraOperator == null) {
            return null;
        }

        cameraOperator.setUnassignedAt(new Date());
        cameraOperatorRepository.save(cameraOperator);

        return cameraOperator.getId();
    }

    public CameraOperator reassignCamera(Long operatorRequestId, Long toOperatorId, Boolean isMain) {
        OperatorRequest operatorRequest = operatorRequestRepository.findOne(operatorRequestId);
        CameraOperator toCameraOperator;
        Camera camera = operatorRequest.getFromCamera2Operator().getCamera();
        Long permanentOperatorId =
                operatorRequest.getRequestType() == OperatorRequest.RequestType.TEMPORARY
                    ? operatorRequest.getFromCamera2Operator().getOperator().getId()
                    : null;


        unassignCameraFromOperator(camera.getId(), operatorRequest.getFromCamera2Operator().getOperator().getId());
        toCameraOperator = assignCameraToOperator(camera.getId(), toOperatorId, permanentOperatorId, isMain);

        operatorRequest.setChangedAt(new Date());
        operatorRequest.setToCamera2Operator(toCameraOperator);
        operatorRequestRepository.save(operatorRequest);

        return toCameraOperator;
    }
}
