package com.keymakr.service;

import com.keymakr.model.customer.Customer;
import com.keymakr.model.customer.CustomerStatus;
import com.keymakr.repository.CustomerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final CustomerRepository customerRepository;

    @Autowired
    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public List<Customer> getCustomers() {
        return customerRepository.findAll();
    }

    public Page<Customer> getCustomerList(Integer pageNumber, Integer pageSize, String searchQuery) {
        if (searchQuery != null && !searchQuery.isEmpty()) {
            searchQuery = "%" + searchQuery.toUpperCase() + "%";
            return customerRepository.findByNameOrEmailAddressAndCustomerStatusNot(new PageRequest(pageNumber - 1, pageSize), searchQuery, CustomerStatus.DELETED);
        }

        return customerRepository.findByCustomerStatusNot(new PageRequest(pageNumber - 1, pageSize), CustomerStatus.DELETED);
    }

    public Customer getCustomerById(Long customerId) {
        return customerRepository.findOne(customerId);
    }


    public Customer saveCustomer(Customer customer) {
        return customerRepository.save(customer);
    }

    public Long deleteCustomer(Long customerId) {
        Customer customer = customerRepository.findOne(customerId);
        customer.setCustomerStatus(CustomerStatus.DELETED);
        customerRepository.save(customer);
        //TODO all customer’s streams have to be stopped for all viewing operators with notification message 25 streamViewCancelled;
        // if the customer is logged in to the system at the moment of his account deletion, he is logged out of the system and taken to the Home page
        return customerId;
    }

    public Customer getCustomerByCameraId(Long cameraId) {
        return customerRepository.findByCameraId(cameraId);
    }

    public Customer getCustomerByLocationId(Long locationId) {
        return customerRepository.findByLocationId(locationId);
    }

}
