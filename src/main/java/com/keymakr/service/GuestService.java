package com.keymakr.service;

import com.keymakr.model.Identity;
import com.keymakr.repository.IdentityRepository;
import com.keymakr.repository.RoleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GuestService {

    private IdentityRepository identityRepository;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private RoleRepository roleRepository;

    @Autowired
    public GuestService(IdentityRepository identityRepository, RoleRepository roleRepository) {
        this.identityRepository = identityRepository;
        this.roleRepository = roleRepository;
    }

    public Identity findIdentityByUsername(String username) {
        logger.info("GuestService.findIdentityByUsername()");
        return identityRepository.findByUsername(username);
    }

}
