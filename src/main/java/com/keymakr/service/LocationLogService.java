package com.keymakr.service;

import com.keymakr.model.log.LocationLog;
import com.keymakr.model.log.LocationLogFilter;
import com.keymakr.repository.LocationLogRepository;
import com.keymakr.repository.specification.LocationLogSpecs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;

import java.util.List;

import static org.springframework.data.jpa.domain.Specifications.where;

@Service
public class LocationLogService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private LocationLogRepository locationLogRepository;

    @Autowired
    public LocationLogService(LocationLogRepository locationLogRepository) {
        this.locationLogRepository = locationLogRepository;
    }

    public List<LocationLog> getLocationLogs(LocationLogFilter filter) {
        Specifications<LocationLog> specs = makeSpec(filter);

        return locationLogRepository.findAll(specs);
    }


    public Page<LocationLog> getLocationLogs(Integer pageNumber, Integer pageSize, LocationLogFilter filter) {
        PageRequest pageRequest = new PageRequest(pageNumber - 1, pageSize);
        Specifications<LocationLog> specs = makeSpec(filter);

        return locationLogRepository.findAll(specs, pageRequest);
    }

    private Specifications<LocationLog> makeSpec(LocationLogFilter filter) {
        Specifications<LocationLog> specs = where(LocationLogSpecs.alwaysTrue());

        if (filter.getFrom() != null) {
            specs = specs.and(LocationLogSpecs.isLogRecordAfter(filter.getFrom()));
        }

        if (filter.getTo() != null) {
            specs = specs.and(LocationLogSpecs.isLogRecordBefore(filter.getTo()));
        }

        if (filter.getLocationName() != null && !filter.getLocationName() .trim().isEmpty()) {
            specs = specs.and(LocationLogSpecs.doesLocationNameContain('%' + filter.getLocationName() .trim().toLowerCase() + '%'));
        }

        if (filter.getCustomerId() != null) {
            specs = specs.and(LocationLogSpecs.doesLocationBelongCustomer(filter.getCustomerId()));
        }

        if (filter.getCameraId() != null) {
            specs = specs.and(LocationLogSpecs.doesLocationIncludeStream(filter.getCameraId()));
        }

        if (filter.getOperatorId() != null) {
            specs = specs.and(LocationLogSpecs.doesLocationIncludeStreamAssignedToOperator(filter.getOperatorId()));
        }

        return specs;
    }
}
