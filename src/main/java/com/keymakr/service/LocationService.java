package com.keymakr.service;

import com.keymakr.model.camera.Location;
import com.keymakr.repository.LocationRepository;
import com.keymakr.repository.OperatorAssignmentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LocationService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private LocationRepository locationRepository;

    private OperatorAssignmentRepository operatorAssignmentRepository;

    @Autowired
    public LocationService(LocationRepository locationRepository, OperatorAssignmentRepository operatorAssignmentRepository) {
        this.locationRepository = locationRepository;
        this.operatorAssignmentRepository = operatorAssignmentRepository;
    }

    public Location getLocationByCameraId(Long cameraId) {
        return locationRepository.findLocationByCameraId(cameraId);
    }

    public Location saveLocation(Location location) {
        return locationRepository.save(location);
    }

    public void deleteLocation(Long locationId) {
        locationRepository.delete(locationId);
    }

    public Location getLocationById(Long locationId) {
        return locationRepository.findOne(locationId);
    }

    public List<Location> getAllLocations() {
        return locationRepository.findAll();
    }

    public List<Location> getLocationsWithUnassignedCameras() {
        return null;
    }

    public List<Location> getLocationsByCustomerId(Long customerId) {
        return locationRepository.findByOwnerId(customerId);
    }
}
