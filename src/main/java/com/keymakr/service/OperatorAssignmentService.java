package com.keymakr.service;

import com.keymakr.model.operator.Operator;
import com.keymakr.model.operator.OperatorAssignment;
import com.keymakr.repository.OperatorAssignmentRepository;
import com.keymakr.repository.specification.OperatorAssignmentSpecs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;

import java.util.List;

import static org.springframework.data.jpa.domain.Specifications.where;

@Service
public class OperatorAssignmentService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final static Integer MAX_ASSIGNMENT_PER_OPERATOR = 6;

    private OperatorAssignmentRepository operatorAssignmentRepository;

    @Autowired
    public OperatorAssignmentService(OperatorAssignmentRepository operatorAssignmentRepository) {
        this.operatorAssignmentRepository = operatorAssignmentRepository;
    }

    public List<OperatorAssignment> getFilteredAssignments(Long locationId, Long cameraId, String searchQuery, Boolean returnSuggested) {
        Specifications<OperatorAssignment> specs =
                Specifications
                        .where(OperatorAssignmentSpecs.hasAssignmentsNotGreaterThan(MAX_ASSIGNMENT_PER_OPERATOR));

        if (searchQuery != null && !searchQuery.trim().isEmpty()) {
            specs = specs.and(OperatorAssignmentSpecs.doesNameContain(searchQuery));
        }
        if (returnSuggested) {
            specs = specs.and(OperatorAssignmentSpecs.isSuggested(locationId));
        } else {
            specs = specs.and(OperatorAssignmentSpecs.isOther(locationId));
        }

        specs = specs.and(OperatorAssignmentSpecs.isOperatorNotAssignedToCamera(cameraId));


        return operatorAssignmentRepository.findAll(specs);
    }

    public List<OperatorAssignment> getAssignmentsByNameOrStateIn(List<Operator.State> states, String searchQuery) {
        return operatorAssignmentRepository.findAll(makeSpec(states, searchQuery));
    }

    public Page<OperatorAssignment> getAssignmentsByNameAndStateIn(Integer pageNumber, Integer pageSize, List<Operator.State> states, String searchQuery) {

        return operatorAssignmentRepository.findAll(makeSpec(states, searchQuery), new PageRequest(pageNumber - 1, pageSize));
    }

    private Specifications<OperatorAssignment> makeSpec(List<Operator.State> states, String searchQuery) {
        Specifications<OperatorAssignment> specs = where(OperatorAssignmentSpecs.alwaysTrue());

        if (searchQuery != null && !searchQuery.trim().isEmpty()) {
            specs = specs.and(OperatorAssignmentSpecs.doesEmailOrNameContain("%" + searchQuery.trim().toLowerCase() + "%"));
        }

        if (states != null && !states.isEmpty()) {
            specs = specs.and(OperatorAssignmentSpecs.isStateIn(states));
        }

        return specs;
    }

}
