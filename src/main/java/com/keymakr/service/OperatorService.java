package com.keymakr.service;

import com.keymakr.model.Identity;
import com.keymakr.model.Role;
import com.keymakr.model.camera.CameraOperator;
import com.keymakr.model.operator.Operator;
import com.keymakr.model.operator.OperatorAssignment;
import com.keymakr.model.operator.OperatorRequest;
import com.keymakr.repository.*;
import com.keymakr.repository.specification.OperatorAssignmentSpecs;
import com.keymakr.repository.specification.OperatorRequestSpecs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class OperatorService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private CameraRepository cameraRepository;

    private RoleRepository roleRepository;

    private OperatorRepository operatorRepository;

    private ProfileRepository profileRepository;

    private IdentityRepository identityRepository;

    private OperatorRequestRepository operatorRequestRepository;

    private CameraOperatorRepository cameraOperatorRepository;

    private OperatorAssignmentService operatorAssignmentService;

    @Autowired
    public OperatorService(CameraRepository cameraRepository,
                           OperatorRepository operatorRepository,
                           ProfileRepository profileRepository,
                           IdentityRepository identityRepository,
                           RoleRepository roleRepository,
                           OperatorRequestRepository operatorRequestRepository,
                           CameraOperatorRepository cameraOperatorRepository,
                           OperatorAssignmentService operatorAssignmentService) {
        this.cameraRepository = cameraRepository;
        this.operatorRepository = operatorRepository;
        this.profileRepository = profileRepository;
        this.identityRepository = identityRepository;
        this.roleRepository = roleRepository;
        this.cameraOperatorRepository = cameraOperatorRepository;
        this.operatorAssignmentService = operatorAssignmentService;
        this.operatorRequestRepository = operatorRequestRepository;
    }

    public Page<Operator> getOperators(Integer pageNumber, Integer pageSize) {
        return operatorRepository.findAll(new PageRequest(pageNumber - 1, pageSize));
    }

    public List<Operator> getOperators() {
        return operatorRepository.findAll();
    }

    public Operator getOperatorById(Long operatorId) {
        return operatorRepository.findOne(operatorId);
    }


    public Operator saveOperator(Operator operator) {
        operator.setCameraOperators(null);
        if (operator.getId() == null) {
            Identity identity = Identity.builder()
                                        .password("")
                                        .username(operator.getProfile().getEmailAddress())
                                        .role(roleRepository.findOne(Role.OPERATOR))
                                        .build();
            operator.getProfile().setIdentity(identityRepository.save(identity));
        }
        operator.setProfile(profileRepository.save(operator.getProfile()));
        operator = operatorRepository.save(operator);

        return operatorRepository.save(operator);
    }

    public Long deleteOperator(Long operatorId) {
        operatorRepository.delete(operatorId);

        return operatorId;
    }

    public Page<Operator> getOperatorsByNameAndStateIn(Integer pageNumber, Integer pageSize, List<Operator.State> states, String searchQuery) {

        return operatorAssignmentService.getAssignmentsByNameAndStateIn(pageNumber, pageSize, states, searchQuery)
                .map(OperatorAssignment::getOperator);
    }

    public Operator getMainOperatorByCameraId(Long cameraId) {
        List<CameraOperator> cameraOperators = cameraOperatorRepository.findByCameraIdAndIsMainAndUnassignedAtIsNull(cameraId, true);

        return (cameraOperators == null || cameraOperators.isEmpty() ? null : cameraOperators.get(0).getOperator());
    }

    public List<CameraOperator> getCameraOperatorsByCameraId(Long cameraId) {
        return cameraOperatorRepository.findByCameraIdAndUnassignedAtIsNull(cameraId);
    }

    public Page<OperatorRequest> getOperatorRequests(Integer pageNumber, Integer pageSize) {
        PageRequest pageRequest = new PageRequest(pageNumber - 1, pageSize);
        Specifications<OperatorRequest> specs = Specifications.where(OperatorRequestSpecs.alwaysTrue());

        specs = specs.and(OperatorRequestSpecs.isNotProcessed());

        return operatorRequestRepository.findAll(specs, pageRequest);
    }

    public Boolean markRequestsAsViewed(List<Long> viewedRequests) {
        List<OperatorRequest> operatorRequests = operatorRequestRepository.findAll(OperatorRequestSpecs.isInList(viewedRequests));

        for (OperatorRequest operatorRequest: operatorRequests) {
            operatorRequest.setIsRequestViewed(true);
        }

        operatorRequestRepository.save(operatorRequests);
        //TODO: update through websocket amount of unviewed requests
        return true;
    }
}
