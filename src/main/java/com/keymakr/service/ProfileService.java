package com.keymakr.service;

import com.keymakr.model.Identity;
import com.keymakr.model.Profile;
import com.keymakr.model.ProfileView;
import com.keymakr.model.Role;
import com.keymakr.repository.IdentityRepository;
import com.keymakr.repository.ProfileRepository;
import com.keymakr.repository.ProfileViewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class ProfileService {

    private ProfileViewRepository profileViewRepository;
    private ProfileRepository profileRepository;
    private IdentityRepository identityRepository;

    @Autowired
    public ProfileService(ProfileViewRepository profileViewRepository, ProfileRepository profileRepository, IdentityRepository identityRepository) {
        this.profileViewRepository = profileViewRepository;
        this.profileRepository = profileRepository;
        this.identityRepository = identityRepository;
    }

    public Profile getProfile(Long identityId) {
        return profileRepository.findByIdentityId(identityId);
    }

    public Page<ProfileView> getAdminList(Integer pageNumber, Integer pageSize, Boolean custAdminFilterFl, Boolean operAdminFilterFl, String searchQuery) {
        List<Long> adminRoleIds = new ArrayList<>();

        if (custAdminFilterFl) {
            adminRoleIds.add(Role.CUSTOMER_ADMIN);
        } else if (operAdminFilterFl) {
            adminRoleIds.add(Role.OPERATOR_ADMIN);
        } else {
            adminRoleIds = Arrays.asList(Role.CUSTOMER_ADMIN, Role.OPERATOR_ADMIN, Role.MAIN_ADMIN);
        }

        if (searchQuery != null && !searchQuery.isEmpty()) {
            searchQuery = "%" + searchQuery.toUpperCase() + "%";
            return profileViewRepository.findByNameOrEmailAddressAndRoleIdIn(new PageRequest(pageNumber - 1, pageSize), searchQuery, searchQuery, adminRoleIds);
        } else {
            return profileViewRepository.findByRoleIdIn(new PageRequest(pageNumber - 1, pageSize), adminRoleIds);
        }
    }

    public Page<ProfileView> getOperatorList(Integer pageNumber, Integer pageSize, String searchQuery) {
        if (searchQuery != null && !searchQuery.isEmpty()) {
            searchQuery = "%" + searchQuery.toUpperCase() + "%";
            return profileViewRepository.findByNameOrEmailAddressAndRoleId(new PageRequest(pageNumber - 1, pageSize), searchQuery, Role.OPERATOR);
        }

        return profileViewRepository.findByRoleId(new PageRequest(pageNumber - 1, pageSize), Role.OPERATOR);
    }

    public Page<ProfileView> getCustomerList(Integer pageNumber, Integer pageSize, String searchQuery) {
        if (searchQuery != null && !searchQuery.isEmpty()) {
            searchQuery = "%" + searchQuery.toUpperCase() + "%";
            return profileViewRepository.findByNameOrEmailAddressAndRoleId(new PageRequest(pageNumber - 1, pageSize), searchQuery, Role.CUSTOMER);
        }

        return profileViewRepository.findByRoleId(new PageRequest(pageNumber - 1, pageSize), Role.CUSTOMER);
    }

    public Profile saveProfile(Profile profile) {
        if (profile.getIdentity().getId() != null) {
            profile.setIdentity(identityRepository.findOne(profile.getIdentity().getId()));
        } else {
            profile.getIdentity().setUsername(profile.getEmailAddress());
            profile.getIdentity().setPassword("123456"); // TBD change to generated password
        }

        Identity identity = identityRepository.save(profile.getIdentity());
        profile.setIdentity(identity);
        profileRepository.save(profile);

        return profile;
    }

}
