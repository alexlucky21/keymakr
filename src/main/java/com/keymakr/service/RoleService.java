package com.keymakr.service;

import com.keymakr.model.Role;
import com.keymakr.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class RoleService {

    private RoleRepository roleRepository;

    @Autowired
    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    public List<Role> getAdminRoles() {
        List<Long> adminRoleIds = Arrays.asList(Role.CUSTOMER_ADMIN, Role.OPERATOR_ADMIN, Role.MAIN_ADMIN);
        return roleRepository.findByIdIn(adminRoleIds);
    }

    public List<Role> getAllRoles() {
        return roleRepository.findAll();
    }

}
