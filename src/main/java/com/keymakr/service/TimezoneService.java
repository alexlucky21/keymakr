package com.keymakr.service;

import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.*;

@Service
public class TimezoneService {

    public List<String> getTimezones(){
        List<String> zoneList = new ArrayList<>(ZoneId.getAvailableZoneIds());
        return getAllOffsets(zoneList);
    }

    private List<String> getAllOffsets(List<String> zoneList) {
        List<String> offsets = new ArrayList<>();
        LocalDateTime dt = LocalDateTime.now();
        TreeSet<String> positiveOffset = new TreeSet<>();
        TreeSet<String> negativeOffset = new TreeSet<>();

        for (String zoneId : zoneList) {
            ZoneId zone = ZoneId.of(zoneId);
            ZonedDateTime zdt = dt.atZone(zone);
            ZoneOffset zos = zdt.getOffset();
            String offset = "UTC" + zos.getId().replaceAll("Z", "+00:00");
            if (offset.contains("-")){
                negativeOffset.add(offset);
            }else {
                positiveOffset.add(offset);
            }
        }

        Set<String> reverseNegativeOffsets = new HashSet<>();
        reverseNegativeOffsets = negativeOffset.descendingSet();
        offsets.addAll(reverseNegativeOffsets);
        offsets.addAll(positiveOffset);

        return offsets;
    }
}
