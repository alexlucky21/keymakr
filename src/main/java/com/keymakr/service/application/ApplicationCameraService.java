package com.keymakr.service.application;

import com.keymakr.model.Profile;
import com.keymakr.model.application.ApplicationCamera;
import com.keymakr.model.application.ApplicationCameraViewingHours;
import com.keymakr.repository.application.ApplicationCameraRepository;
import com.keymakr.repository.application.ApplicationCameraViewingHoursRepository;
import com.keymakr.service.email.EmailNotificationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Service
public class ApplicationCameraService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private ApplicationCameraRepository applicationCameraRepository;
    private ApplicationCameraViewingHoursRepository applicationCameraViewingHoursRepository;
    private EmailNotificationService emailNotificationService;

    @Autowired
    public ApplicationCameraService(ApplicationCameraRepository applicationCameraRepository, ApplicationCameraViewingHoursRepository applicationCameraViewingHoursRepository, EmailNotificationService emailNotificationService) {
        this.applicationCameraRepository = applicationCameraRepository;
        this.applicationCameraViewingHoursRepository = applicationCameraViewingHoursRepository;
        this.emailNotificationService = emailNotificationService;
    }

    public ApplicationCamera getApplicationCameraById(Long applicationCameraId) {
        return applicationCameraRepository.findOne(applicationCameraId);
    }

    public List<ApplicationCamera> getApplicationCamerasByApplicationLocationId(Long applicationLocationId) {
        return applicationCameraRepository.findByApplicationLocationIdAndDeletedAtIsNullOrderById(applicationLocationId);
    }

    @Transactional
    public Long deleteApplicationCamera(Long applicationCameraId) {
        List<ApplicationCameraViewingHours> viewingHours = applicationCameraViewingHoursRepository.findByApplicationCameraIdAndDeletedAtIsNull(applicationCameraId);
        viewingHours.forEach(applicationCameraViewingHours -> {
            applicationCameraViewingHours.setDeletedAt(new Date());
        });
        applicationCameraViewingHoursRepository.save(viewingHours);
        ApplicationCamera applicationCamera = applicationCameraRepository.findOne(applicationCameraId);
        applicationCamera.setDeletedAt(new Date());
        applicationCameraRepository.save(applicationCamera);
        return applicationCameraId;
    }

    @Transactional
    public ApplicationCamera activateApplicationCamera(ApplicationCamera applicationCamera) {
        applicationCamera.setActivationDate(new Date());
        ApplicationCamera activatedCamera = saveApplicationCamera(applicationCamera);
        Profile profile = Profile.builder().firstName(activatedCamera.getApplicationLocation().getOwnerApplication().getApplicationProfile().getFirstName())
                .lastName(activatedCamera.getApplicationLocation().getOwnerApplication().getApplicationProfile().getLastName())
                .emailAddress(activatedCamera.getApplicationLocation().getOwnerApplication().getApplicationProfile().getEmailAddress())
                .phone(activatedCamera.getApplicationLocation().getOwnerApplication().getApplicationProfile().getPhone()).build();
        emailNotificationService.sendCameraActivationNotification(profile, activatedCamera.getName());
        return activatedCamera;
    }

    @Transactional
    public ApplicationCamera saveApplicationCamera(ApplicationCamera applicationCamera) {
        List<ApplicationCameraViewingHours> currentViewingHours = applicationCameraViewingHoursRepository.findByApplicationCameraIdAndDeletedAtIsNull(applicationCamera.getId());
        ApplicationCamera savedApplicationCamera = applicationCameraRepository.save(applicationCamera);
        if (applicationCamera.getApplicationCameraViewingHours() != null) {
            applicationCamera.getApplicationCameraViewingHours().forEach(applicationCameraViewingHours -> applicationCameraViewingHours.setApplicationCamera(savedApplicationCamera));
            currentViewingHours.forEach(currentViewingHoursItem -> {
                if (!applicationCamera.getApplicationCameraViewingHours().contains(currentViewingHoursItem)) {
                    currentViewingHoursItem.setDeletedAt(new Date());
                    applicationCameraViewingHoursRepository.save(currentViewingHoursItem);
                }
            });
            applicationCameraViewingHoursRepository.save(applicationCamera.getApplicationCameraViewingHours());
        }else {
            currentViewingHours.forEach(currentViewingHoursItem -> {
                currentViewingHoursItem.setDeletedAt(new Date());
            });
            applicationCameraViewingHoursRepository.save(currentViewingHours);
        }
        if (applicationCamera.getUseDefaultViewingHours()) {
            //TODO copy viewing hours from application to application camera
        }
        return savedApplicationCamera;
    }
}
