package com.keymakr.service.application;

import com.keymakr.model.application.ApplicationCameraViewingHours;
import com.keymakr.model.application.ApplicationCameraViewingHoursView;
import com.keymakr.repository.application.ApplicationCameraViewingHoursRepository;
import com.keymakr.repository.application.ApplicationCameraViewingHoursViewRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class ApplicationCameraViewingHoursService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private ApplicationCameraViewingHoursViewRepository applicationCameraViewingHoursViewRepository;
    private ApplicationCameraViewingHoursRepository applicationCameraViewingHoursRepository;

    @Autowired
    public ApplicationCameraViewingHoursService(ApplicationCameraViewingHoursViewRepository applicationCameraViewingHoursViewRepository, ApplicationCameraViewingHoursRepository applicationCameraViewingHoursRepository) {
        this.applicationCameraViewingHoursViewRepository = applicationCameraViewingHoursViewRepository;
        this.applicationCameraViewingHoursRepository = applicationCameraViewingHoursRepository;
    }

    public List<ApplicationCameraViewingHoursView> getViewingHoursListByApplicationCameraId(Long applicationCameraId){
        return applicationCameraViewingHoursViewRepository.findByApplicationCameraId(applicationCameraId);
    }

    public Iterable<ApplicationCameraViewingHours> saveViewingHours(List<ApplicationCameraViewingHours> applicationCameraViewingHours) {
        return applicationCameraViewingHoursRepository.save(applicationCameraViewingHours);
    }

    public List<Long> deleteViewingHours(List<Long> applicationApplicationCameraViewingHoursIds) {
        applicationApplicationCameraViewingHoursIds.forEach(id -> {
            ApplicationCameraViewingHours applicationCameraViewingHours = applicationCameraViewingHoursRepository.findOne(id);
            applicationCameraViewingHours.setDeletedAt(new Date());
            applicationCameraViewingHoursRepository.save(applicationCameraViewingHours);
        });
        return applicationApplicationCameraViewingHoursIds;
    }
}
