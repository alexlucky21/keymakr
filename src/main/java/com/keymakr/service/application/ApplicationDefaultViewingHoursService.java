package com.keymakr.service.application;

import com.keymakr.model.application.ApplicationDefaultViewingHours;
import com.keymakr.model.application.ApplicationDefaultViewingHoursView;
import com.keymakr.repository.application.ApplicationDefaultViewingHoursRepository;
import com.keymakr.repository.application.ApplicationDefaultViewingHoursViewRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class ApplicationDefaultViewingHoursService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private ApplicationDefaultViewingHoursViewRepository applicationDefaultViewingHoursViewRepository;
    private ApplicationDefaultViewingHoursRepository applicationDefaultViewingHoursRepository;

    @Autowired
    public ApplicationDefaultViewingHoursService(ApplicationDefaultViewingHoursViewRepository applicationDefaultViewingHoursViewRepository, ApplicationDefaultViewingHoursRepository applicationDefaultViewingHoursRepository) {
        this.applicationDefaultViewingHoursViewRepository = applicationDefaultViewingHoursViewRepository;
        this.applicationDefaultViewingHoursRepository = applicationDefaultViewingHoursRepository;
    }

    public List<ApplicationDefaultViewingHoursView> getViewingHoursListByApplicationId(Long applicationId) {
        return applicationDefaultViewingHoursViewRepository.findByApplicationId(applicationId);
    }

    public Iterable<ApplicationDefaultViewingHours> saveViewingHours(List<ApplicationDefaultViewingHours> applicationDefaultViewingHoursList) {
        return applicationDefaultViewingHoursRepository.save(applicationDefaultViewingHoursList);
    }

    public List<Long> deleteViewingHours(List<Long> applicationDefaultViewingHoursIds) {
        applicationDefaultViewingHoursIds.forEach(id -> {
            ApplicationDefaultViewingHours applicationDefaultViewingHours = applicationDefaultViewingHoursRepository.findOne(id);
            applicationDefaultViewingHours.setDeletedAt(new Date());
            applicationDefaultViewingHoursRepository.save(applicationDefaultViewingHours);
        });
        return applicationDefaultViewingHoursIds;
    }
}
