package com.keymakr.service.application;

import com.keymakr.model.application.ApplicationCamera;
import com.keymakr.model.application.ApplicationLocation;
import com.keymakr.repository.application.ApplicationCameraRepository;
import com.keymakr.repository.application.ApplicationLocationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ApplicationLocationService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private ApplicationLocationRepository applicationLocationRepository;
    private ApplicationCameraRepository applicationCameraRepository;

    @Autowired
    public ApplicationLocationService(ApplicationCameraRepository applicationCameraRepository, ApplicationLocationRepository applicationLocationRepository) {
        this.applicationLocationRepository = applicationLocationRepository;
        this.applicationCameraRepository = applicationCameraRepository;
    }

    public List<ApplicationLocation> getApplicationLocationsByApplicationId(Long applicationId) {
        return applicationLocationRepository.findByOwnerApplicationId(applicationId);
    }

    public ApplicationLocation saveApplicationLocation(ApplicationLocation applicationLocation) {
        return applicationLocationRepository.save(applicationLocation);
    }

    public Long deleteApplicationLocation(Long applicationLocationId) {
        List<ApplicationCamera> applicationCameras = applicationCameraRepository.findByApplicationLocationIdAndDeletedAtIsNullOrderById(applicationLocationId);
        applicationCameraRepository.delete(applicationCameras);
        applicationLocationRepository.delete(applicationLocationId);
        return applicationLocationId;
    }
}
