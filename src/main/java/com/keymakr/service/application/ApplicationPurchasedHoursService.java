package com.keymakr.service.application;

import com.keymakr.model.application.ApplicationPurchasedHours;
import com.keymakr.model.application.ApplicationPurchasedHoursView;
import com.keymakr.repository.application.ApplicationPurchasedHoursRepository;
import com.keymakr.repository.application.ApplicationPurchasedHoursViewRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ApplicationPurchasedHoursService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private ApplicationPurchasedHoursRepository applicationPurchasedHoursRepository;
    private ApplicationPurchasedHoursViewRepository applicationPurchasedHoursViewRepository;

    @Autowired
    public ApplicationPurchasedHoursService(ApplicationPurchasedHoursRepository applicationPurchasedHoursRepository, ApplicationPurchasedHoursViewRepository applicationPurchasedHoursViewRepository) {
        this.applicationPurchasedHoursRepository = applicationPurchasedHoursRepository;
        this.applicationPurchasedHoursViewRepository = applicationPurchasedHoursViewRepository;
    }

    public ApplicationPurchasedHours saveApplicationPurchasedHours(ApplicationPurchasedHours applicationPurchasedHours) {
        return applicationPurchasedHoursRepository.save(applicationPurchasedHours);
    }

    public ApplicationPurchasedHoursView getApplicationPurchasedHoursViewByApplicationId(Long applicationId){
        return applicationPurchasedHoursViewRepository.findOne(applicationId);
    }
}
