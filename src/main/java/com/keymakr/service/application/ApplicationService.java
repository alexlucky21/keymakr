package com.keymakr.service.application;

import com.keymakr.model.Identity;
import com.keymakr.model.Profile;
import com.keymakr.model.Role;
import com.keymakr.model.application.*;
import com.keymakr.model.camera.Camera;
import com.keymakr.model.camera.Location;
import com.keymakr.model.customer.Customer;
import com.keymakr.model.customer.CustomerStatus;
import com.keymakr.repository.*;
import com.keymakr.repository.application.ApplicationCameraRepository;
import com.keymakr.repository.application.ApplicationLocationRepository;
import com.keymakr.repository.application.ApplicationRepository;
import com.keymakr.repository.application.ApplicationStatusRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

@Service
public class ApplicationService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private ApplicationRepository applicationRepository;
    private ApplicationStatusRepository applicationStatusRepository;
    private CustomerRepository customerRepository;
    private RoleRepository roleRepository;
    private IdentityRepository identityRepository;
    private ProfileRepository profileRepository;
    private ApplicationLocationRepository applicationLocationRepository;
    private LocationRepository locationRepository;
    private ApplicationCameraRepository applicationCameraRepository;
    private CameraRepository cameraRepository;

    @Autowired
    public ApplicationService(ApplicationCameraRepository applicationCameraRepository,
                              LocationRepository locationRepository,
                              ApplicationLocationRepository applicationLocationRepository,
                              ProfileRepository profileRepository,
                              IdentityRepository identityRepository,
                              RoleRepository roleRepository,
                              ApplicationRepository applicationRepository,
                              ApplicationStatusRepository applicationStatusRepository,
                              CustomerRepository customerRepository,
                              CameraRepository cameraRepository)
    {
        this.applicationRepository = applicationRepository;
        this.applicationStatusRepository = applicationStatusRepository;
        this.customerRepository = customerRepository;
        this.roleRepository = roleRepository;
        this.identityRepository = identityRepository;
        this.profileRepository = profileRepository;
        this.applicationLocationRepository = applicationLocationRepository;
        this.locationRepository = locationRepository;
        this.applicationCameraRepository = applicationCameraRepository;
        this.cameraRepository = cameraRepository;
    }

    public Page<Application> getApplications(Integer pageNumber, Integer pageSize) {
        return applicationRepository.findByApplicationStatusIdNotIn(new PageRequest(pageNumber - 1, pageSize), new ArrayList<>(Arrays.asList(3L, 4L)));
    }

    public Application getApplicationById(Long applicationId) {
        return applicationRepository.findOne(applicationId);
    }

    public Application saveApplication(Application application) {
        if (application.getApplicationStatus().getId() == ApplicationStatus.NEW) {
            application.setApplicationDate(new Date());
        }
        Application savedApplication = applicationRepository.save(application);
        if (application.getApplicationLocations() != null) {
            Set<ApplicationLocation> applicationLocations = new HashSet<>(application.getApplicationLocations());
            applicationLocations.forEach(applicationLocation ->
            {
                applicationLocation.setOwnerApplication(savedApplication);
                ApplicationLocation savedApplicationLocation = applicationLocationRepository.save(applicationLocation);
                if (applicationLocation.getApplicationCameras() != null) {
                    Set<ApplicationCamera> applicationCameras = new HashSet<>(applicationLocation.getApplicationCameras());
                    applicationCameras.forEach(applicationCamera ->
                    {
                        applicationCamera.setApplicationLocation(savedApplicationLocation);
                        ApplicationCamera savedApplicationCamera = applicationCameraRepository.save(applicationCamera);
                        //TODO Actions part
                    });
                }
            });
        }
        return savedApplication;
    }

    public Application declineApplication(Application application) {
        application.setApplicationStatus(applicationStatusRepository.findOne(ApplicationStatus.DELETED));
        return applicationRepository.save(application);
    }

    @Transactional
    public Customer activateAccount(Long applicationId) {
        Application application = applicationRepository.findOne(applicationId);
        Profile profile = createProfile(application.getApplicationProfile());
        Customer customer = createCustomer(profile);
        createLocations(applicationId, customer);

        application.setApplicationStatus(applicationStatusRepository.findOne(ApplicationStatus.APPLIED));
        applicationRepository.save(application);
        return customer;
    }

    private List<Location> createLocations(Long applicationId, Customer customer){
        List<ApplicationLocation> applicationLocations = applicationLocationRepository.findByOwnerApplicationId(applicationId);
        List<Location> locations = new ArrayList<>();
        for(ApplicationLocation applicationLocation : applicationLocations){
            Location location = Location.builder()
                    .address(applicationLocation.getAddress())
                    .comment(applicationLocation.getComment())
                    .name(applicationLocation.getName())
                    .owner(customer)
                    .timezone(applicationLocation.getTimezone())
                    .build();
            location = locationRepository.save(location);
            locations.add(location);
            createCameras(applicationLocation.getId(), location);
        }
        return locations;
    }

    private List<Camera> createCameras(Long applicationLocationId, Location location) {
        List<ApplicationCamera> applicationCameras = applicationCameraRepository.findByApplicationLocationIdAndDeletedAtIsNullOrderById(applicationLocationId);
        List<Camera> cameras = new ArrayList<>();
        for (ApplicationCamera applicationCamera : applicationCameras) {
            Camera camera = Camera.builder()
                    .comment(applicationCamera.getComment())
                    .location(location)
                    .name(applicationCamera.getName())
                    .url(applicationCamera.getUrl())
                    .login(applicationCamera.getLogin())
                    .password(applicationCamera.getPassword())
                    .build();
            camera = cameraRepository.save(camera);
            cameras.add(camera);
        }
        return cameras;
    }

    private Customer createCustomer(Profile profile){
        Customer customer = Customer.builder().profile(profile).registrationDate(new Date()).customerStatus(CustomerStatus.ACTIVE).build();
        return customerRepository.save(customer);
    }

    private Profile createProfile(ApplicationProfile applicationProfile){
        Identity identity = createCustomerIdentity(applicationProfile.getEmailAddress());
        Profile profile = populateProfile(applicationProfile, identity);
        return profileRepository.save(profile);
    }

    private Profile populateProfile(ApplicationProfile applicationProfile, Identity identity) {
        return Profile.builder().firstName(applicationProfile.getFirstName()).lastName(applicationProfile.getLastName())
                .emailAddress(applicationProfile.getEmailAddress()).phone(applicationProfile.getPhone()).identity(identity).build();
    }

    private Identity createCustomerIdentity(String email) {
        //String password = RandomStringUtils.randomAlphanumeric(10);
        String password = "123456"; // TBD change to generated password
        Identity identity = Identity.builder().username(email).password(password).role(roleRepository.findOne(Role.CUSTOMER)).build();
        return identityRepository.save(identity);
    }
}
