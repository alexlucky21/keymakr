package com.keymakr.service.email;

import com.keymakr.model.Profile;
import com.keymakr.model.application.Application;
import com.keymakr.model.customer.Customer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

@Service
public class EmailNotificationService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private MailContentBuilder mailContentBuilder;
    private EmailSenderThread emailSenderThread;

    @Value("${send.from.email}")
    private String fromEmail;

    @Autowired
    public EmailNotificationService(MailContentBuilder mailContentBuilder, EmailSenderThread emailSenderThread) {
        this.mailContentBuilder = mailContentBuilder;
        this.emailSenderThread = emailSenderThread;
    }

    public void sendDeclineNotification(Application application){
        String name = application.getApplicationProfile().getFirstName() + " " + application.getApplicationProfile().getLastName();
        String content = mailContentBuilder.buildDeclineMessage(name, application.getDeclineReason());
        MimeMessagePreparator messagePreparator = preparationMessagePreparator(application.getApplicationProfile().getEmailAddress(), "Keymakr support. Decline application", content);
        sendNotification(messagePreparator);
    }

    public void sendAccountActivationNotification(Customer customer){
        String name = customer.getProfile().getFirstName() + " " + customer.getProfile().getLastName();
        String content = mailContentBuilder.buildAccountActivationMessage(name, customer.getProfile().getIdentity().getPassword());
        MimeMessagePreparator messagePreparator = preparationMessagePreparator(customer.getProfile().getEmailAddress(), "Keymakr support. Account activated", content);
        sendNotification(messagePreparator);
    }

    public void sendCameraActivationNotification(Profile profile, String cameraName){
        String name = profile.getFirstName() + " " + profile.getLastName();
        String content = mailContentBuilder.buildActivationCameraNotificationMessage(name, cameraName);
        MimeMessagePreparator messagePreparator = preparationMessagePreparator(profile.getEmailAddress(), "Keymakr support. Camera activated", content);
        sendNotification(messagePreparator);
    }

    private void sendNotification(MimeMessagePreparator messagePreparator){
        emailSenderThread.setMessagePreparator(messagePreparator);
        emailSenderThread.start();
    }

    private MimeMessagePreparator preparationMessagePreparator(String emailTo, String subject, String content){
        MimeMessagePreparator messagePreparator = mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
            messageHelper.setFrom(fromEmail);
            messageHelper.setTo(emailTo);
            messageHelper.setSubject(subject);
            messageHelper.setText(content, true);
        };
        return messagePreparator;
    }
}
