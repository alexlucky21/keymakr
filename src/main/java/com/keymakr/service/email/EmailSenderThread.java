package com.keymakr.service.email;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

@Service
public class EmailSenderThread extends Thread {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private JavaMailSender javaMailSender;
    private MimeMessagePreparator messagePreparator;

    @Autowired
    public EmailSenderThread(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Query
    public void run()
    {
        try {
            javaMailSender.send(messagePreparator);
        }catch (MailException e){
            logger.error(e.getMessage(), e);
        }
    }

    public void setMessagePreparator(MimeMessagePreparator messagePreparator) {
        this.messagePreparator = messagePreparator;
    }
}
