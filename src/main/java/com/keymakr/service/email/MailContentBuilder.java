package com.keymakr.service.email;

import freemarker.template.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import java.util.HashMap;
import java.util.Map;

@Service
public class MailContentBuilder {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private Configuration freemarkerConfiguration;

    @Autowired
    public MailContentBuilder(Configuration freemarkerConfiguration) {
        this.freemarkerConfiguration = freemarkerConfiguration;
    }

    public String buildDeclineMessage(String customerName, String declineReason){
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("name", customerName);
        model.put("declineReason", declineReason);
        return buildMessage("declineTemplate.txt", model);
    }

    public String buildAccountActivationMessage(String customerName, String password) {
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("name", customerName);
        model.put("password", password);
        return buildMessage("activateAccountTemplate.txt", model);
    }

    public String buildActivationCameraNotificationMessage(String customerName, String cameraName){
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("name", customerName);
        model.put("cameraName", cameraName);
        return buildMessage("activateCameraTemplate.txt", model);
    }



    private String buildMessage(String templateName, Map<String, Object> model){
        String message = "";
        try {
            message = FreeMarkerTemplateUtils.processTemplateIntoString(freemarkerConfiguration.getTemplate(templateName), model);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return message;
    }
}
