INSERT INTO role(role_id, name, description) VALUES (1, 'CUSTOMER', 'Customer'), (2, 'OPERATOR', 'Operator'), (3, 'CUSTOMER_ADMIN', 'Customer Administrator'), (4, 'OPERATOR_ADMIN', 'Operator Administrator'), (5, 'MAIN_ADMIN', 'Main Administrator');
INSERT INTO identity(username, password, role_id) VALUES ('customer', '123456', 1), ('operator', '123456', 2), ('customer_admin', '123456', 3), ('operator_admin', '123456', 4), ('main_admin', '123456', 5), ('operator2', '123456', 2), ('operator3', '123456', 2);
INSERT INTO profile (identity_id,first_name, last_name, email_address, phone) VALUES (1, 'Customer','Single','customer@keymakr.com', '8 703 32 103 00'), (2, 'Operator','Single','operator@keymakr.com', '8 703 32 103 01'), (3, 'Customer','Admin','customer_admin@keymakr.com', '8 703 32 103 02'), (4, 'Operator','Admin','operator_admin@keymakr.com', '8 703 32 103 03'), (5, 'Main','Admin','main_admin@keymakr.com', '8 703 32 103 04'), (6, 'Operator2','Second','operator2@keymakr.com', '8 703 32 103 01'), (7, 'Operator3','Third','operator3@keymakr.com', '8 703 32 103 01');
CREATE OR REPLACE VIEW v_profile AS (SELECT i.identity_id, p.first_name, p.last_name, concat_ws(' ', p.first_name, p.last_name) AS name, p.email_address, p.phone, r.role_id, r.description AS role_name FROM profile p JOIN identity i on p.identity_id = i.identity_id JOIN role r on i.role_id = r.role_id);

INSERT INTO customer(profile_id, registration_date, customer_status) VALUES (1, '2016-03-12', 0);
INSERT INTO operator(state, profile_id) VALUES (2, 2), (0, 6), (1, 7);
INSERT INTO location(address, comment, name, time_zone, customer_id) VALUES ('2 Karazina str., Kharkiv, 61000','Brama','TC "Brama"', 'UTC+2', 1), ('64 Pobedy ave., 61000','Alekseevka, Klass','TRC "KLASS"', 'UTC+2', 1);

INSERT INTO camera(comment, name, location_id, state) VALUES ('Hall', 'Hall', 1, 1), ('Back door', 'Back door', 1, 1), ('roof', 'Roof', 1, 1), ('Store', 'Store', 2, 1);

INSERT INTO mtm_camera2operator(camera_id, operator_id, is_main, assigned_at) VALUES (1, 1, true, CURRENT_TIMESTAMP);

INSERT INTO application_profile(first_name, last_name, email_address, phone) VALUES ('Customer', 'Application', 'customer@keymark.com', '8 703 32 103 05');

INSERT INTO application_status(application_status_id, name, description) VALUES (1, 'NEW', 'New application from guest'), (2, 'READ', 'Application does not read customer administrator'), (3, 'DELETED', 'Declined application'), (4, 'APPLIED', 'Application applied. Account for customer created');

INSERT INTO application(application_date, application_profile_id, application_status_id) VALUES ('2016-03-12', 1, 1);

INSERT INTO application_location(application_id, address, comment, name, time_zone) VALUES (1, 'Kharkov, Morozova 25', 'Base location', 'Base', 'UTC+02:00');

CREATE OR REPLACE VIEW v_operator_assignment AS SELECT o.operator_id, COUNT(c2o.camera2operator_id) AS total, SUM(CASE WHEN c2o.is_main THEN 1 ELSE 0 END) AS main_total FROM operator o LEFT OUTER JOIN mtm_camera2operator c2o ON o.operator_id = c2o.operator_id   AND c2o.unassigned_at IS NULL GROUP BY o.operator_id;

CREATE OR REPLACE VIEW v_camera_assignment AS SELECT c.camera_id, COUNT(c2o.camera2operator_id) AS total FROM camera c LEFT OUTER JOIN mtm_camera2operator c2o ON c.camera_id = c2o.camera_id AND c2o.unassigned_at IS NULL GROUP BY c.camera_id;

INSERT INTO application_camera(comment, name, application_location_id, use_default_viewing_hours) VALUES ('', 'First Camera', 1, TRUE), ('storeroom', 'Second Camera', 1, TRUE);

CREATE OR REPLACE VIEW v_application_default_viewing_hours AS (SELECT row_number() OVER (ORDER BY a.application_id, dow.day_of_week) AS v_application_default_viewing_hours_id, a.application_id, dow.day_of_week, advh.application_default_viewing_hours_id, advh.from_time, advh.to_time, CASE WHEN advh.application_default_viewing_hours_id ISNULL THEN FALSE ELSE TRUE END AS active FROM application a CROSS JOIN (SELECT 0 AS day_of_week UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6) AS dow LEFT JOIN application_default_viewing_hours advh ON a.application_id = advh.application_id AND dow.day_of_week = advh.day_of_week AND advh.deleted_at ISNULL);

CREATE OR REPLACE VIEW v_application_camera_viewing_hours AS (SELECT row_number() OVER (ORDER BY ac.application_camera_id, dow.day_of_week) AS v_application_camera_viewing_hours_id, ac.application_camera_id, dow.day_of_week, acdvh.application_camera_viewing_hours_id, acdvh.from_time, acdvh.to_time, CASE WHEN acdvh.application_camera_viewing_hours_id ISNULL THEN FALSE ELSE TRUE END AS active FROM application_camera ac CROSS JOIN (SELECT 0 AS day_of_week UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6) AS dow LEFT JOIN application_camera_viewing_hours acdvh ON ac.application_camera_id = acdvh.application_camera_id AND dow.day_of_week = acdvh.day_of_week AND acdvh.deleted_at ISNULL);

INSERT INTO application_purchased_hours(amount_purchased_hours, purchase_date, application_id) VALUES (5, '2016-03-12', 1);

CREATE OR REPLACE VIEW v_application_purchased_hours AS (SELECT aph.application_id, sum(aph.amount_purchased_hours) AS purchased_hours FROM application_purchased_hours aph GROUP BY (aph.application_id));

INSERT INTO action (name, metadata, predefined) VALUES ('Customer entered', 'This action will be recorded when customer entered is seen in the video stream', TRUE), ('Fire', 'This action will be recorded when fire is seen in the video stream', TRUE), ('Theft', 'This action will be recorded when theft is seen in the video stream', TRUE);

INSERT INTO action_parameter (name, action_id) VALUES ('Man', 1), ('Woman', 1), ('Teenage', 1), ('Elderly person', 1);

CREATE OR REPLACE VIEW V_LOCATION_LOG AS SELECT l.location_id, MAX(al.date_time_start) AS last_log_at FROM location l LEFT OUTER JOIN camera c ON l.location_id = c.location_id LEFT OUTER JOIN mtm_camera2action c2a ON c.camera_id = c2a.camera_id LEFT OUTER JOIN action_log al ON c2a.camera2action_id = al.camera_action_id GROUP BY l.location_id;

INSERT INTO mtm_camera2action (action_id, camera_id) VALUES (1, 1), (1, 2), (2, 2), (2, 3), (3, 1), (3, 3), (1, 4);

INSERT INTO action_log (date_time_start, is_main, action_parameter_id, camera_action_id, operator_id) VALUES (TO_DATE('12-12-2016', 'dd-mm-YYYY'), TRUE, NULL, 1, 1), (TO_DATE('01-12-2016', 'dd-mm-YYYY'), TRUE, NULL, 2, 1), (TO_DATE('01-06-2015', 'dd-mm-YYYY'), FALSE, NULL, 3, 1), (TO_DATE('01-06-2016', 'dd-mm-YYYY'), FALSE, NULL, 3, 1), (TO_DATE('01-06-2016', 'dd-mm-YYYY'), TRUE, NULL, 7, 1);

INSERT INTO operator_request (request_type, requested_at, camera2operator_id, is_viewed) VALUES (1, CURRENT_TIMESTAMP, 1, false);