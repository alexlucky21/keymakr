import { Component, OnInit, ViewChild } from '@angular/core';
import { ActionService } from "../../shared/service/action.service";
import { ModalDirective } from "ng2-bootstrap/ng2-bootstrap";
import { Action } from "../../shared/model/action/action.model";
import { ActionParameter } from "../../shared/model/action/action.parameter.model";

@Component({
    selector: 'actions',
    templateUrl: './app/customer-admin/actions/actions.component.html',
    providers: []
})

export class ActionsComponent implements OnInit {
    @ViewChild('addActionModal') public addActionModal: ModalDirective;

    actions: Action[];
    newAction: Action;

    constructor(private actionService: ActionService) {
    }

    ngOnInit(): void {
        this.newAction = new Action();
        this.refreshActions();
    }

    refreshActions() {
        this.actionService.getDefaultActions().subscribe(actions => {
            actions.forEach(action => {
                this.refreshActionParameters(action);
            });
            this.actions = actions;
        })
    }

    refreshActionParameters(action: Action) {
        this.actionService.getActionParameterByActionId(action.id)
        .subscribe(parameters => {
            this.actions[ this.actions.findIndex(act => act.id == action.id) ].parameters = parameters;
        })
    }

    deleteAction(action: Action) {
        this.actionService.deleteAction(action.id)
        .subscribe(id => {
            this.refreshActions();
        })
    }

    saveAction() {
        this.newAction.predefined = true;
        this.actionService.saveAction(this.newAction)
        .subscribe(action => {
            this.addActionModal.hide();
            this.newAction = new Action();
            this.refreshActions();
        })
    }

    updateAction(action: Action) {
        this.actionService.saveAction(action)
        .subscribe(savedAction => {
            this.refreshActions();
        })
    }

    deleteActionParameter(parameter: ActionParameter) {
        this.actionService.deleteActionParameter(parameter.id)
        .subscribe(id => {
            this.refreshActionParameters(parameter.action);
        })
    }

    addActionParameter(action: Action) {
        let parameter = new ActionParameter();
        parameter.action = action;
        parameter.editFl = true;
        this.actions[ this.actions.indexOf(action) ].parameters.push(parameter);
    }

    updateActionParameter(parameter: ActionParameter) {
        parameter.action.parameters = null;
        this.actionService.saveActionParameter(parameter)
        .subscribe(actionParameter => {
            this.refreshActionParameters(parameter.action);
        })
    }

}