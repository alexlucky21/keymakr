import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from "@angular/common";
import { Application } from "../../../shared/model/application/application.model";
import { ApplicationService } from "../../../shared/service/application/application.service";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { ApplicationLocation } from "../../../shared/model/application/application.location.model";
import { ApplicationCamera } from "../../../shared/model/application/application.camera.model";
import { ApplicationLocationService } from "../../../shared/service/application/application-location.service";
import { ModalDirective } from "ng2-bootstrap/ng2-bootstrap";
import { TimezoneService } from "../../../shared/service/timezone.service";
import { ApplicationCameraService } from "../../../shared/service/application/application-camera.service";
import { ApplicationDefaultViewingHours } from "../../../shared/model/application/application.default-viewing-hours.model";
import { ViewingTime } from "../../../shared/model/viewing-time.model";
import { ApplicationDefaultViewingHoursView } from "../../../shared/model/application/application.default-viewing-hours.view.model";
import { ApplicationPurchasedHours } from "../../../shared/model/application/application.purchased-hours.model";
import { ApplicationCameraProperties } from "../../../shared/model/application/application.camera.properties.model";

@Component({
    selector: 'application',
    templateUrl: './app/customer-admin/applications/application/application.component.html',
    providers: []
})

export class ApplicationComponent implements OnInit {
    @ViewChild('addLocationModal')
    public addLocationModal: ModalDirective;

    @ViewChild('confirmDeleteApplicationLocationModal')
    public confirmDeleteApplicationLocationModal: ModalDirective;

    @ViewChild('addPurchasedHoursModal')
    public addPurchasedHoursModal: ModalDirective;

    @ViewChild('defaultViewingHoursModal')
    public defaultViewingHoursModal: ModalDirective;

    application: Application;
    applicationLocations: ApplicationLocation[];
    applicationCameras: ApplicationCamera[];
    newApplicationLocation: ApplicationLocation;
    amountOfHoursPurchased: string;
    amountOfViewingTime: string = '00:00';
    amountOfUsedHours: string = '00:00';
    defaultViewingHoursView: ApplicationDefaultViewingHoursView[];
    selectedApplicationLocation: ApplicationLocation;
    timezones: string[];

    viewingHoursView: ApplicationDefaultViewingHoursView[];
    hoursList: string[];
    minutesList: string[];

    applicationPurchasedHours: ApplicationPurchasedHours;

    datepickerOpts = {
        autoclose: true,
        todayBtn: 'linked',
        todayHighlight: true,
        assumeNearbyYear: true,
        format: 'D, d MM yyyy'
    };

    constructor(private router: Router, private location: Location, private route: ActivatedRoute, private timezoneService: TimezoneService,
                private applicationService: ApplicationService, private applicationLocationService: ApplicationLocationService, private applicationCameraService: ApplicationCameraService) {
    }

    ngOnInit(): void {
        this.application = new Application();
        this.newApplicationLocation = new ApplicationLocation();
        this.defaultViewingHoursView = [];
        this.applicationPurchasedHours = new ApplicationPurchasedHours();
        this.initApplication();
        this.refreshApplicationLocations();
        this.refreshDefaultViewingHours();
        this.refreshAmountOfHoursPurchased();
    }

    initApplication(): void {
        this.route.params.forEach((params: Params) => {
            let id = +params[ 'id' ];
            if (!isNaN(id)) {
                this.applicationService.getApplication(id).subscribe(application => {
                    this.application = application;
                });
            }
        });
    };

    refreshApplicationLocations(): void {
        this.route.params.forEach((params: Params) => {
            let id = +params[ 'id' ];
            if (!isNaN(id)) {
                this.applicationService.getApplicationLocationsByApplication(id)
                .subscribe(applicationLocations => this.applicationLocations = applicationLocations);
            }
        })
    };

    initTimezones(){
        this.timezoneService.getAllTimezones()
        .subscribe(timezones => this.timezones = timezones);
    }

    getApplicationCamerasByApplicationLocation(applicationLocation: ApplicationLocation, isOpen: boolean) {
        if (isOpen) {
            let index = this.applicationLocations.indexOf(applicationLocation);
            if (index != -1) {
                this.applicationCameraService.getApplicationCamerasByApplicationLocation(applicationLocation.id)
                .subscribe(applicationCameras => {
                    applicationCameras.forEach(applicationCamera => {
                        applicationCamera.properties = new ApplicationCameraProperties();
                        applicationCamera.properties.assignedTo = 'Not assigned';
                        applicationCamera.activationDate ? applicationCamera.properties.activationStatus = 'Activated' : applicationCamera.properties.activationStatus = 'Not activated';
                    });
                    this.applicationLocations[ index ].applicationCameras = applicationCameras
                });
            }
        }
    };

    editPersonalInfo() {
        this.router.navigate([ "customer-admin/applications/" + this.application.id + "/personal-info" ]);
    };

    doDeleteApplicationLocation(){
        this.applicationLocationService.deleteApplicationLocation(this.selectedApplicationLocation.id).subscribe(applicationLocationId=>{
            this.selectedApplicationLocation = undefined;
            this.confirmDeleteApplicationLocationModal.hide();
            this.refreshApplicationLocations();
        });
    };

    openConfirmDeleteApplicationLocationModal(applicationLocation: ApplicationLocation){
        this.selectedApplicationLocation = applicationLocation;
        this.confirmDeleteApplicationLocationModal.show();
    }

    openAddLocationModal(){
        this.initTimezones();
        this.addLocationModal.show();
    }

    saveApplicationLocation(applicationLocation: ApplicationLocation) {
        applicationLocation.ownerApplication = this.application;
        this.applicationLocationService.saveApplicationLocation(applicationLocation).subscribe(applicationLocation => {
            this.addLocationModal.hide();
            this.refreshApplicationLocations()
        });
    };

    onHideConfirmDeleteApplicationLocationModal(): void {
        this.selectedApplicationLocation = undefined;
    }

    openAddPurchasedHoursModal(){
        this.applicationPurchasedHours.purchaseDate = new Date();
        this.applicationPurchasedHours.application = this.application;
        this.addPurchasedHoursModal.show();
    }

    addPurchasedHours(){
        this.applicationService.saveApplicationPurchasedHours(this.applicationPurchasedHours).
            subscribe(()=>{
            this.applicationPurchasedHours = new ApplicationPurchasedHours();
            this.addPurchasedHoursModal.hide();
            this.refreshAmountOfHoursPurchased();
        });
    }

    onEvent(event) {
        event.preventDefault();
        event.stopPropagation();
    }

    openDefaultViewingHoursModal(){
        this.initViewingHours();
        this.defaultViewingHoursModal.show();
    }

    saveDefaultViewingHours() {
        let listToSave = [];
        let listToDelete = [];
        this.viewingHoursView.forEach(viewingHoursItem => {
                if (viewingHoursItem.active) {
                    let defaultViewingHours = this.populateApplicationDefaultViewingHours(viewingHoursItem);
                    listToSave.push(defaultViewingHours);
                } else {
                    if (viewingHoursItem.applicationDefaultViewingHoursId != null) {
                        listToDelete.push(viewingHoursItem.applicationDefaultViewingHoursId);
                    }
                }
            }
        );
        this.applicationService.saveDefaultViewingHours(listToSave).subscribe(() =>
            this.applicationService.deleteDefaultViewingHours(listToDelete).subscribe(() =>
                this.refreshDefaultViewingHours())
        );

        this.defaultViewingHoursModal.hide();
        this.viewingHoursView = [];
    }

    onChangedFromHours(fromHour: string, viewingHoursItem: ApplicationDefaultViewingHoursView) {
        if (viewingHoursItem.to.hours < fromHour) {
            viewingHoursItem.to.hours = fromHour;
        }
        if(viewingHoursItem.to.hours == fromHour) {
            if (viewingHoursItem.to.minutes <= viewingHoursItem.from.minutes) {
                viewingHoursItem.to.minutes = this.minutesList[ this.minutesList.indexOf(viewingHoursItem.from.minutes) + 1 ];
            }
        }
    }
    onChangedFromMinutes(fromMinutes: string, viewingHoursItem: ApplicationDefaultViewingHoursView){
        if(viewingHoursItem.to.hours == viewingHoursItem.from.hours){
            if(viewingHoursItem.to.minutes <= fromMinutes){
                viewingHoursItem.to.minutes = this.minutesList[this.minutesList.indexOf(fromMinutes) + 1];
            }
        }
    }

    refreshAmountOfHoursPurchased(){
        this.route.params.forEach((params: Params) => {
            let id = +params[ 'id' ];
            if (!isNaN(id)) {
                this.applicationService.getApplicationPurchasedHoursViewByApplicationId(id).subscribe(applicationPurchasedHoursView => {
                    this.amountOfHoursPurchased = applicationPurchasedHoursView.purchasedHours + ':00';
                });
            }
        });
    }

    refreshDefaultViewingHours(): void{
        this.route.params.forEach((params: Params) => {
            let id = +params[ 'id' ];
            if (!isNaN(id)) {
                this.applicationService.getDefaultViewingHoursView(id).subscribe(defaultViewingHours => {
                    this.defaultViewingHoursView = [];
                    defaultViewingHours.forEach(defaultViewingHoursItem =>{
                        if(defaultViewingHoursItem.active){
                            this.defaultViewingHoursView.push(defaultViewingHoursItem);
                        }
                    });
                });
            }
        });
    }

    initViewingHours() {
        this.applicationService.getDefaultViewingHoursView(this.application.id).subscribe(viewingHoursList => {
            if (this.defaultViewingHoursView.length == 0){
                viewingHoursList.forEach(viewingHoursItem => viewingHoursItem.active = true)
            }
            viewingHoursList.forEach(viewingHoursItem => {
                viewingHoursItem.from = ViewingTime.splitToTime(viewingHoursItem.fromTime);
                viewingHoursItem.to = ViewingTime.splitToTime(viewingHoursItem.toTime, true);
            });
            this.viewingHoursView = viewingHoursList;
            this.hoursList = ViewingTime.hoursList;
            this.minutesList = ViewingTime.minutesList;
        });
    }

    populateApplicationDefaultViewingHours(applicationDefaultViewingHoursView: ApplicationDefaultViewingHoursView): ApplicationDefaultViewingHours{
        let defaultViewingHours = new ApplicationDefaultViewingHours();
        defaultViewingHours.id = applicationDefaultViewingHoursView.applicationDefaultViewingHoursId;
        defaultViewingHours.application = this.application;
        defaultViewingHours.dayOfWeek = applicationDefaultViewingHoursView.dayOfWeek;
        defaultViewingHours.fromTime = ViewingTime.joinToString(applicationDefaultViewingHoursView.from);
        defaultViewingHours.toTime = ViewingTime.joinToString(applicationDefaultViewingHoursView.to);
        return defaultViewingHours;
    }
}