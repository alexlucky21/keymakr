import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from "@angular/common";
import { ApplicationCamera } from "../../../../shared/model/application/application.camera.model";
import { ActivatedRoute, Params } from "@angular/router";
import { ApplicationCameraService } from "../../../../shared/service/application/application-camera.service";
import { ApplicationLocation } from "../../../../shared/model/application/application.location.model";
import { ApplicationService } from "../../../../shared/service/application/application.service";
import { ModalDirective } from "ng2-bootstrap/ng2-bootstrap";
import { ApplicationCameraViewingHours } from "../../../../shared/model/application/application.camera.viewing-hours.model";
import { ViewingTime } from "../../../../shared/model/viewing-time.model";
import { ApplicationCameraViewingHoursView } from "../../../../shared/model/application/application.camera.viewing-hours.view.model";

@Component({
    selector: 'activate-camera',
    templateUrl: './app/customer-admin/applications/application/camera-info/camera-info.component.html',
    providers: []
})

export class CameraInfoComponent implements OnInit {
    @ViewChild('confirmDeleteCameraModal')
    public confirmDeleteCameraModal: ModalDirective;

    @ViewChild('confirmDeactivateCameraModal')
    public confirmDeactivateCameraModal: ModalDirective;

    editFl: boolean;
    applicationCamera: ApplicationCamera;
    applicationLocations: ApplicationLocation[];
    viewingHoursView: ApplicationCameraViewingHoursView[];
    hoursList: string[];
    minutesList: string[];

    constructor(private location: Location, private route: ActivatedRoute, private applicationCameraService: ApplicationCameraService, private applicationService: ApplicationService) {
    }

    ngOnInit(): void {
        this.applicationCamera = new ApplicationCamera();
        this.hoursList = ViewingTime.hoursList;
        this.minutesList = ViewingTime.minutesList;
        this.editFl = false;
        this.initApplicationCamera();
        this.initApplicationLocations();
        this.initApplicationCameraViewingHours();
    }

    initApplicationCamera() {
        this.route.params.forEach((params: Params) => {
            let id = +params[ 'cameraId' ];
            if (!isNaN(id)) {
                this.editFl = true;
                this.applicationCameraService.getApplicationCamera(id).subscribe(applicationCamera => {
                    this.applicationCamera = applicationCamera;
                });
            }
        });
    }

    initApplicationLocations() {
        this.route.params.forEach((params: Params) => {
            let id = +params[ 'id' ];
            if (!isNaN(id)) {
                this.editFl = true;
                this.applicationService.getApplicationLocationsByApplication(id).subscribe(applicationLocations => {
                    this.applicationLocations = applicationLocations;
                });
            }
        });
    }

    doDeleteApplicationCamera() {
        if (this.editFl) {
            this.applicationCameraService.deleteApplicationCamera(this.applicationCamera.id).subscribe(applicationCameraId => {
                this.confirmDeleteCameraModal.hide();
                this.location.back();
            })
        } else {
            this.cancel();
        }
    }

    doDeactivateApplicationCamera(){
        this.applicationCamera.activationDate = null;
        this.applicationCameraService.saveApplicationCamera(this.applicationCamera).subscribe(applicationCamera=>{
            this.applicationCamera = applicationCamera;
            this.confirmDeactivateCameraModal.hide();
        });
    }

    initApplicationCameraViewingHours() {
        this.route.params.forEach((params: Params) => {
            let id = +params[ 'cameraId' ];
            if (!isNaN(id)) {
                this.editFl = true;
                this.applicationCameraService.getViewingHoursView(id).subscribe(viewingHoursList => {
                    if(this.applicationCamera.useDefaultViewingHours == true){
                        viewingHoursList.forEach(viewingHoursItem => {
                            viewingHoursItem.active = true;
                        })
                    }
                    viewingHoursList.forEach(viewingHoursItem => {
                        viewingHoursItem.from = ViewingTime.splitToTime(viewingHoursItem.fromTime);
                        viewingHoursItem.to = ViewingTime.splitToTime(viewingHoursItem.toTime, true);
                    });
                    this.viewingHoursView = viewingHoursList;
                });
            }else{
                this.generateViewingHoursView();
            }
        });
    }

    onChangedFromHours(fromHour: string, viewingHoursItem: ApplicationCameraViewingHoursView) {
        if (viewingHoursItem.to.hours < fromHour) {
            viewingHoursItem.to.hours = fromHour;
        }
        if(viewingHoursItem.to.hours == fromHour) {
            if (viewingHoursItem.to.minutes <= viewingHoursItem.from.minutes) {
                viewingHoursItem.to.minutes = this.minutesList[ this.minutesList.indexOf(viewingHoursItem.from.minutes) + 1 ];
            }
        }
    }
    onChangedFromMinutes(fromMinutes: string, viewingHoursItem: ApplicationCameraViewingHoursView){
        if(viewingHoursItem.to.hours == viewingHoursItem.from.hours){
            if(viewingHoursItem.to.minutes <= fromMinutes){
                viewingHoursItem.to.minutes = this.minutesList[this.minutesList.indexOf(fromMinutes) + 1];
            }
        }
    }

    cancel(): void {
        this.location.back();
    }

    onSubmit(): void {
        let listToSave = [];
        this.viewingHoursView.forEach(viewingHoursItem => {
            if (viewingHoursItem.active) {
                let applicationCameraViewingHours = this.populateApplicationCameraViewingHours(viewingHoursItem);
                listToSave.push(applicationCameraViewingHours);
            }
        });
        if (this.applicationCamera.useDefaultViewingHours) {
            this.applicationCamera.applicationCameraViewingHours = null;
        } else {
            this.applicationCamera.applicationCameraViewingHours = listToSave;
        }
        if (this.applicationCamera.activationDate) {
            this.applicationCameraService.saveApplicationCamera(this.applicationCamera).subscribe(applicationCamera => this.location.back());
        }
        else {
            this.applicationCameraService.activateApplicationCamera(this.applicationCamera).subscribe(applicationCamera => this.location.back());
        }
    }

    private populateApplicationCameraViewingHours(applicationCameraViewingHoursView: ApplicationCameraViewingHoursView): ApplicationCameraViewingHours {
        let applicationCameraViewingHours = new ApplicationCameraViewingHours();
        applicationCameraViewingHours.id = applicationCameraViewingHoursView.applicationCameraViewingHoursId;
        applicationCameraViewingHours.dayOfWeek = applicationCameraViewingHoursView.dayOfWeek;
        applicationCameraViewingHours.fromTime = ViewingTime.joinToString(applicationCameraViewingHoursView.from);
        applicationCameraViewingHours.toTime = ViewingTime.joinToString(applicationCameraViewingHoursView.to);
        return applicationCameraViewingHours;
    }

    private generateViewingHoursView() {
        this.viewingHoursView = [];
        let dayOfWeek = ['MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY'];
        for(let i=0; i<7;i++){
            let item = new ApplicationCameraViewingHoursView();
            item.active = true;
            item.dayOfWeek = dayOfWeek[i];
            item.from = ViewingTime.splitToTime(null);
            item.to = ViewingTime.splitToTime(null, true);
            this.viewingHoursView.push(item);
        }
    }


}