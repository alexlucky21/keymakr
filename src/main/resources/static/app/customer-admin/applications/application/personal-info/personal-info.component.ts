import { Component, OnInit, Input } from '@angular/core';
import { Location } from "@angular/common";
import { ActivatedRoute, Params } from "@angular/router";
import { ApplicationService } from "../../../../shared/service/application/application.service";
import { Application } from "../../../../shared/model/application/application.model";

@Component({
    selector: 'personal-info',
    templateUrl: './app/customer-admin/applications/application/personal-info/personal-info.component.html',
    providers: []
})

export class PersonalInfoComponent implements OnInit {

    application: Application;

    constructor(private location: Location, private route: ActivatedRoute, private applicationService: ApplicationService) {
    }

    ngOnInit(): void {
        this.application = new Application();
        this.initApplicationProfile();
    }

    initApplicationProfile() {
        this.route.params.forEach((params: Params) => {
            let id = +params[ 'id' ];
            if (!isNaN(id)) {
                this.applicationService.getApplication(id).subscribe(application => {
                    this.application = application;
                });
            }
        });
    }

    cancel(): void {
        this.location.back();
    }

    onSubmit(): void {
        this.applicationService.saveApplication(this.application).subscribe(application => this.location.back());
    }
}