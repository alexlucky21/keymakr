import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { ApplicationService } from "../../shared/service/application/application.service";
import { Application } from "../../shared/model/application/application.model";
import { ModalDirective } from "ng2-bootstrap/ng2-bootstrap";
import { ApplicationStatus, APPLICATION_STATUS } from "../../shared/model/application/application-status.model";

@Component({
    selector: 'applications',
    templateUrl: './app/customer-admin/applications/applications.component.html',
    providers: []
})

export class ApplicationsComponent implements OnInit {

    @ViewChild('confirmModal') public confirmModal:ModalDirective;

    applicationList: Application[];
    applicationToDecline: Application;
    currentPage: number;
    itemsPerPage: number;
    totalItems: number;

    constructor(viewContainerRef:ViewContainerRef, private applicationService: ApplicationService) {
        this.applicationToDecline = new Application();
    }

    ngOnInit(): void {
        this.currentPage = 1;
        this.itemsPerPage = 2;

        this.refreshApplicationList(this.currentPage, this.itemsPerPage);
    }

    refreshApplicationList(pageNumber: number, pageSize: number): void {
        this.applicationService.getApplications(pageNumber, pageSize).subscribe(tableData => {
            this.applicationList = tableData.content;
            this.totalItems = tableData.totalElements;
        });
    }

    activateAccount(application: Application){
        this.applicationService.activateAccount(application)
            .subscribe(customer => this.refreshApplicationList(this.currentPage, this.itemsPerPage),
            error => console.log(error.message))
    }

    declineApplication(){
        this.applicationService.declineApplication(this.applicationToDecline)
            .subscribe(declinedApplication => {
                this.hideConfirmModal();
                this.refreshApplicationList(this.currentPage, this.itemsPerPage)
            });
    }

    markApplicationAsRead(application: Application): Application {
        if (application.applicationStatus.id != APPLICATION_STATUS.READ) {
            application.applicationStatus.id = APPLICATION_STATUS.READ;
            this.applicationService.saveApplication(application)
            .subscribe(savedApplication => application = savedApplication);
        }
        return application;
    }

    public showConfirmModal(application: Application):void {
        this.markApplicationAsRead(application);
        this.applicationToDecline = application;
        this.confirmModal.show();
    }

    public hideConfirmModal():void {
        this.applicationToDecline.declineReason = null;
        this.applicationToDecline = new Application();
        this.confirmModal.hide();
    }

    public pageChanged(event: any): void {
        this.refreshApplicationList(event.page, this.itemsPerPage);
    };
}