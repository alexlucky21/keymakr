import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CustomerAdminComponent } from "./customer-admin.component";
import { CustomerListComponent } from "./customer-list/customer-list.component";
import { ActionsComponent } from "./actions/actions.component";
import { ApplicationsComponent } from "./applications/applications.component";
import { ApplicationComponent } from "./applications/application/application.component";
import { PersonalInfoComponent } from "./applications/application/personal-info/personal-info.component";
import { CameraInfoComponent } from "./applications/application/camera-info/camera-info.component";

const routes: Routes = [
    {
        path: '', component: CustomerAdminComponent,
        children: [
            { path: '', redirectTo: 'customers', pathMatch: 'full' },
            { path: 'customers', component: CustomerListComponent },
            { path: 'actions', component: ActionsComponent },
            { path: 'applications', component: ApplicationsComponent },
            { path: 'applications/:id', component: ApplicationComponent },
            { path: 'applications/:id/personal-info', component: PersonalInfoComponent },
            { path: 'applications/:id/camera-info/add', component: CameraInfoComponent },
            { path: 'applications/:id/camera-info/:cameraId', component: CameraInfoComponent }/*,
            { path: 'special-reports', component: SpecialReportsComponent },
            { path: 'alerts', component: AlertsComponent },
            { path: 'manage-support', component: ManageSupportComponent },
            { path: 'profile', component: profileComponent },
            */
        ]
    } ];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ]
})

export class CustomerAdminRoutingModule {
}