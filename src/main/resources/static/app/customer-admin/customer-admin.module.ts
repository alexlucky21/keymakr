import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { CustomerAdminComponent } from "./customer-admin.component";
import { CustomerAdminRoutingModule } from "./customer-admin-routing.module";
import { CustomerListComponent } from "./customer-list/customer-list.component";
import { ActionsComponent } from "./actions/actions.component";
import { SharedTranslateModule } from "../shared/shared-translate.module";
import { LogoutModule } from "../shared/logout/logout.module";
import { PaginationModule, ModalModule } from "ng2-bootstrap/ng2-bootstrap";
import { ApplicationsComponent } from "./applications/applications.component";
import { CustomerService } from "../shared/service/customer.service";
import { ApplicationComponent } from "./applications/application/application.component";
import { ApplicationService } from "../shared/service/application/application.service";
import { AccordionModule } from 'ng2-bootstrap/ng2-bootstrap';
import { PersonalInfoComponent } from "./applications/application/personal-info/personal-info.component";
import { ApplicationLocationService } from "../shared/service/application/application-location.service";
import { TimezoneService } from "../shared/service/timezone.service";
import { NKDatetimeModule } from "ng2-datetime/ng2-datetime";
import { ApplicationCameraService } from "../shared/service/application/application-camera.service";
import { CameraInfoComponent } from "./applications/application/camera-info/camera-info.component";
import { ViewingHoursPipe } from "../shared/pipe/viewing-hours.to-hours.pipe";
import { ViewingMinutesPipe } from "../shared/pipe/viewing-hours.to-minutes.pipe";
import { ActionService } from "../shared/service/action.service";

@NgModule({
    imports: [ CommonModule, CustomerAdminRoutingModule, SharedTranslateModule, LogoutModule, PaginationModule, ModalModule, AccordionModule, NKDatetimeModule ],
    declarations: [ CustomerAdminComponent, CustomerListComponent, ApplicationsComponent, ActionsComponent, ApplicationComponent, PersonalInfoComponent, CameraInfoComponent, ViewingHoursPipe, ViewingMinutesPipe ],
    providers: [ CustomerService, ApplicationService, ApplicationLocationService, TimezoneService, ApplicationCameraService, ActionService ]
})

export class CustomerAdminModule {
}