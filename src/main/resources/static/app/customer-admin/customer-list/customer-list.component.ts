import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { Customer } from "../../shared/model/customer.model";
import { CustomerService } from "../../shared/service/customer.service";
import { ModalDirective } from "ng2-bootstrap/ng2-bootstrap";

@Component({
    selector: 'customer-list',
    templateUrl: './app/customer-admin/customer-list/customer-list.component.html',
    providers: []
})

export class CustomerListComponent implements OnInit {
    @ViewChild('confirmDeleteModal')
    public confirmDeleteModal: ModalDirective;

    searchQuery: string;
    customerList: Customer[];
    selectedCustomer: Customer;
    currentPage: number;
    itemsPerPage: number;
    totalItems: number;

    constructor(private viewContainerRef:ViewContainerRef, private customerService: CustomerService) {
    }

    ngOnInit(): void {
        this.initTableData();
    }

    initTableData() {
        this.currentPage = 1;
        this.itemsPerPage = 2;
        this.searchQuery = '';

        this.refreshCustomerList(this.currentPage, this.itemsPerPage);
    }

    refreshCustomerList(pageNumber: number, pageSize: number): void {
        this.customerService.getFilteredCustomers(pageNumber, pageSize, this.searchQuery).subscribe(tableData => {
            this.customerList = tableData.content;
            this.totalItems = tableData.totalElements;
        });
    }

    search() {
        this.currentPage = 1;
        this.refreshCustomerList(this.currentPage, this.itemsPerPage);
    }

    clearSearch() {
        this.currentPage = 1;
        this.searchQuery = '';
        this.refreshCustomerList(this.currentPage, this.itemsPerPage);
    }

    onSearchKey(event: KeyboardEvent) {
        if (event.keyCode == 13) {
            this.currentPage = 1;
            this.search();
        }
    }

    public pageChanged(event: any): void {
        this.refreshCustomerList(event.page, this.itemsPerPage);
    };

    public showConfirmDeleteModal(customer: Customer):void {
        this.selectedCustomer = customer;
        this.confirmDeleteModal.show();
    }

    doDeleteCustomer(){
        this.customerService.deleteCustomer(this.selectedCustomer.id)
            .subscribe(customerId => {
                this.selectedCustomer = undefined;
                this.confirmDeleteModal.hide();
                this.refreshCustomerList(this.currentPage, this.itemsPerPage);
            });
    }

    onHide(): void {
        this.selectedCustomer = undefined;
    }
}