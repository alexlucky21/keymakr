import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { CustomerComponent } from "./customer.component";
import { CustomerRoutingModule } from "./customer-routing.module";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { ReportListComponent } from "./dashboard/report-list/report-list.component";
import { ProfileComponent } from "./dashboard/profile/profile.component";
import { LogoutModule } from "../shared/logout/logout.module";
import { SharedTranslateModule } from "../shared/shared-translate.module";

@NgModule({
    imports: [ CommonModule, CustomerRoutingModule, LogoutModule, SharedTranslateModule ],
    declarations: [ CustomerComponent, DashboardComponent, ReportListComponent, ProfileComponent ]
})

export class CustomerModule {
}
