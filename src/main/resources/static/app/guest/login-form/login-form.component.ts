import { Component } from "@angular/core";
import { Router } from "@angular/router";

import { AuthenticationService } from "../../shared/service/authentication.service";

@Component({
    selector: "login",
    templateUrl: "./app/guest/login-form/login-form.component.html"
})

export class LoginComponent {
    username: string = 'customer';
    password: string = '123456';
    error: string = '';

    constructor(private auth: AuthenticationService, private router: Router) {
    }

    onSubmit() {
        this.auth.login(this.username, this.password).subscribe(
            (user) => {
                if (user.username) {
                    this.auth.role = user.authorities[ 0 ].authority;
                    this.auth.isLoggedIn = true;

                    this.router.navigate([ this.auth.routePathByRole() ]);
                }
                else {

                }
            },
            (error) => {
                this.auth.role = undefined;
                this.auth.isLoggedIn = false;
                this.error = 'Username or password is incorrect';
                this.router.navigate([ "/" ]);
            }
        );

        return false;
    }
}