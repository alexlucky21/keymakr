import moment = require("moment");
import { Component, OnInit } from '@angular/core';
import { Location } from "@angular/common";
import { RecaptchaService } from "../../shared/service/recaptcha.service";
import { ApplicationService } from "../../shared/service/application/application.service";
import { Application } from "../../shared/model/application/application.model";
import { APPLICATION_STATUS } from "../../shared/model/application/application-status.model";
import { Router } from "@angular/router";
import { ApplicationLocation } from "../../shared/model/application/application.location.model";

@Component({
    selector: 'registration-form',
    templateUrl: './app/guest/registration-form/registration-form.component.html',
    providers: []
})

export class RegistrationFormComponent implements OnInit {

    application: Application;
    successful: boolean;

    recap: string;
    recaptchaError: string = '';

    constructor(private router: Router, private reCaptchaService: RecaptchaService, private applicationService: ApplicationService) {
    }

    ngOnInit(): void {
        this.successful = false;
        this.application = new Application();
        this.application.applicationStatus.id = APPLICATION_STATUS.NEW;
    }

    handleCorrectCaptcha(captchaResponse: any) {
        this.recap = captchaResponse;
        this.recaptchaError = '';
    }

    sendApplication() {
        if (!this.recap) {
            this.recaptchaError = 'Captcha is required';
        } else {
            this.reCaptchaService.checkCaptcha(this.recap)
            .subscribe(
                data => this.applicationService.saveApplication(this.application)
                    .subscribe(application => this.successful = true),
                error => this.recaptchaError = error.message
            );
        }
    }

    addLocation(){
        this.application.applicationLocations.push(new ApplicationLocation());
    }

    deleteApplicationLocation(applicationLocation: ApplicationLocation){
        let index = this.application.applicationLocations.indexOf(applicationLocation);
        if (index > -1) {
            this.application.applicationLocations.splice(index, 1);
        }
    }

    goBackHome(){
        this.router.navigate(['']);
    }
}