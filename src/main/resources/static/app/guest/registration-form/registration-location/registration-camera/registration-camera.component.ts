import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ApplicationCamera } from "../../../../shared/model/application/application.camera.model";

@Component({
    selector: 'registration-camera',
    templateUrl: './app/guest/registration-form/registration-location/registration-camera/registration-camera.component.html',
    providers: []
})

export class RegistrationCameraComponent implements OnInit {

    @Input() applicationCamera: ApplicationCamera;
    @Input() cameraIndex: number;
    @Output() deleteCamera: EventEmitter<ApplicationCamera>;

    constructor() {
        this.deleteCamera = new EventEmitter<ApplicationCamera>();
    }

    ngOnInit(): void {
    }

    removeCamera(){
        this.deleteCamera.emit(this.applicationCamera);
    }

}