import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TimezoneService } from "../../../shared/service/timezone.service";
import { ApplicationLocation } from "../../../shared/model/application/application.location.model";
import { ApplicationCamera } from "../../../shared/model/application/application.camera.model";

@Component({
    selector: 'registration-location',
    templateUrl: './app/guest/registration-form/registration-location/registration-location.component.html',
    providers: []
})

export class RegistrationLocationComponent implements OnInit {

    @Input() applicationLocation: ApplicationLocation;
    @Input() locationIndex: number;
    @Output() deleteLocation: EventEmitter<ApplicationLocation>;

    timezones: string[];

    constructor(private timezoneService: TimezoneService) {
        this.deleteLocation = new EventEmitter<ApplicationLocation>();
    }

    ngOnInit(): void {
        this.initTimezones();
    }

    initTimezones(){
        this.timezoneService.getAllTimezones()
            .subscribe(timezones => this.timezones = timezones);
    }

    removeLocation(){
        this.deleteLocation.emit(this.applicationLocation);
    }

    addCamera(){
        this.applicationLocation.applicationCameras.push(new ApplicationCamera());
    }

    deleteCamera(applicationCamera: ApplicationCamera){
        let index = this.applicationLocation.applicationCameras.indexOf(applicationCamera);
        if (index > -1) {
            this.applicationLocation.applicationCameras.splice(index, 1);
        }
    }
}