import { Component, OnInit } from '@angular/core';
import { Location } from "@angular/common";
import { ActivatedRoute, Params } from "@angular/router";
import { Profile } from "../../../shared/model/profile.model";
import { ProfileViewService } from "../../../shared/service/profile-view.service";
import { Role } from "../../../shared/model/role.model";
import { RoleService } from "../../../shared/service/role.service";

@Component({
    selector: 'admin-form',
    templateUrl: './app/main-admin/admins/admin-form/admin-form.component.html',
    providers: []
})

export class AdminFormComponent implements OnInit {

    profile: Profile;
    roles: Role[];
    editFl: boolean;

    constructor(private location: Location, private route: ActivatedRoute,
                private profileService: ProfileViewService, private roleService: RoleService) {
    }

    ngOnInit(): void {
        this.profile = new Profile();
        this.editFl = false;
        this.initProfile();
        this.initRoles();
    }

    initProfile(): void {
        this.route.params.forEach((params: Params) => {
            let id = +params[ 'id' ];
            if (!isNaN(id)) {
                this.editFl = true;
                this.profileService.getProfile(id).subscribe(profile => {
                    this.profile = profile;
                });
            }
        });
    };

    initRoles(): void {
        this.roleService.getAdminRoles().subscribe(roles => this.roles = roles);
    }

    cancel(): void {
        this.location.back();
    }

    onSubmit(): void {
        this.profileService.saveProfile(this.profile).subscribe(profile => this.location.back());
    }

}