import { Component, OnInit } from '@angular/core';

import { ProfileView } from "../../shared/model/profile-view.model";
import { ProfileViewService } from "../../shared/service/profile-view.service";

@Component({
    selector: 'admins',
    templateUrl: './app/main-admin/admins/admins.component.html',
    providers: []
})

export class AdminsComponent implements OnInit {

    adminList: ProfileView[];
    currentPage: number;
    itemsPerPage: number;
    totalItems: number;

    custAdminFilterFl: boolean;
    operAdminFilterFl: boolean;
    searchQuery: string;

    constructor(private profileViewService: ProfileViewService) {
    }

    ngOnInit(): void {
        this.initTableData();
    }

    initTableData(): void {
        this.currentPage = 1;
        this.itemsPerPage = 2;

        this.custAdminFilterFl = false;
        this.operAdminFilterFl = false;
        this.searchQuery = '';

        this.refreshAdminsList(this.currentPage, this.itemsPerPage);
    }

    refreshAdminsList(pageNumber: number, pageSize: number): void {
        this.profileViewService.getAdminList(pageNumber, pageSize, this.custAdminFilterFl, this.operAdminFilterFl, this.searchQuery).subscribe(tableData => {
            this.adminList = tableData.content;
            this.totalItems = tableData.totalElements;
        });
    }

    filterByAll() {
        this.currentPage = 1;
        this.custAdminFilterFl = false;
        this.operAdminFilterFl = false;
        this.refreshAdminsList(this.currentPage, this.itemsPerPage);
    }

    filterByCustomerAdminList() {
        this.currentPage = 1;
        this.custAdminFilterFl = true;
        this.operAdminFilterFl = false;
        this.refreshAdminsList(this.currentPage, this.itemsPerPage);
    }

    filterByOperatorAdminList() {
        this.currentPage = 1;
        this.custAdminFilterFl = false;
        this.operAdminFilterFl = true;
        this.refreshAdminsList(this.currentPage, this.itemsPerPage);
    }

    search() {
        this.currentPage = 1;
        this.refreshAdminsList(this.currentPage, this.itemsPerPage);
    }

    clearSearch() {
        this.currentPage = 1;
        this.searchQuery = '';
        this.refreshAdminsList(this.currentPage, this.itemsPerPage);
    }

    onSearchKey(event: KeyboardEvent) {
        if (event.keyCode == 13) {
            this.currentPage = 1;
            this.search();
        }
    }

    public pageChanged(event: any): void {
        this.refreshAdminsList(event.page, this.itemsPerPage);
    };

}