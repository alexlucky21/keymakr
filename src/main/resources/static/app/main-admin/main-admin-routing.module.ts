import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MainAdminComponent } from "./main-admin.component";
import { AdminsComponent } from "./admins/admins.component";
import { AdminFormComponent } from "./admins/admin-form/admin-form.component";

const routes: Routes = [
    {
        path: '', component: MainAdminComponent,
        children: [
            { path: '', redirectTo: 'admins', pathMatch: 'full' },
            { path: 'admins', component: AdminsComponent },
            { path: 'admins/add', component: AdminFormComponent },
            { path: 'admins/:id', component: AdminFormComponent }
        ]
    } ];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ]
})

export class MainAdminRoutingModule {
}