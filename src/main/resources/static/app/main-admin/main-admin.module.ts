import { NgModule }      from '@angular/core';
import { CommonModule } from "@angular/common";

import { MainAdminComponent } from "./main-admin.component";
import { MainAdminRoutingModule } from "./main-admin-routing.module";
import { SharedTranslateModule } from "../shared/shared-translate.module";
import { AdminsComponent } from "./admins/admins.component";
import { LogoutModule } from "../shared/logout/logout.module";
import { PaginationModule } from "ng2-bootstrap/ng2-bootstrap";
import { ProfileViewService } from "../shared/service/profile-view.service";
import { AdminFormComponent } from "./admins/admin-form/admin-form.component";
import { RoleService } from "../shared/service/role.service";
import { CustomFormsModule } from "ng2-validation";

@NgModule({
    imports: [ CommonModule, MainAdminRoutingModule, SharedTranslateModule, LogoutModule, PaginationModule, CustomFormsModule ],
    declarations: [ MainAdminComponent, AdminsComponent, AdminFormComponent ],
    providers: [ ProfileViewService, RoleService ]
})

export class MainAdminModule {
}