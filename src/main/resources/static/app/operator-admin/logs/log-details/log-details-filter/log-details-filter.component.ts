import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";

import { LogDetailsFilter } from "../../../../shared/model/log-details-filter.model";


declare const module;

@Component({
    moduleId: module.id,
    selector: "log-details-filter",
    templateUrl: "./log-details-filter.component.html"
})
export class LogDetailsFilterComponent implements OnInit {
    @Input()
    filter: LogDetailsFilter;
    oldFilterString: string;

    @Output()
    search:  EventEmitter<LogDetailsFilter>;
    @Output()
    reset:  EventEmitter<LogDetailsFilter>;

    filterLogDetailsForm: FormGroup;

    constructor() {
        this.filter = new LogDetailsFilter();
        this.search = new EventEmitter<LogDetailsFilter>();
        this.reset = new EventEmitter<LogDetailsFilter>();
        this.oldFilterString = "";
    }

    ngOnInit(): void {
        this.filterLogDetailsForm = new FormGroup({
            actionSearchString: new FormControl(this.filter.actionSearchString),
            streamSearchString: new FormControl(this.filter.streamSearchString),
            operatorSearchString: new FormControl(this.filter.operatorSearchString),
            isMainTimeline: new FormControl(this.filter.isMainTimeline),
            to: new FormControl(this.filter.to),
            from: new FormControl(this.filter.from)
        });

        this.filterLogDetailsForm.controls["actionSearchString"].valueChanges.debounceTime(400).subscribe(searchString => this.onSearch());
        this.filterLogDetailsForm.controls["streamSearchString"].valueChanges.debounceTime(400).subscribe(searchString => this.onSearch());
        this.filterLogDetailsForm.controls["operatorSearchString"].valueChanges.debounceTime(400).subscribe(searchString => this.onSearch());
        this.filterLogDetailsForm.controls["isMainTimeline"].valueChanges.subscribe(state => this.onSearch());
        this.filterLogDetailsForm.controls["from"].valueChanges.subscribe(state => this.onSearch());
        this.filterLogDetailsForm.controls["to"].valueChanges.subscribe(state => this.onSearch());
    }

    populateFormFromModel(): void {
        this.filterLogDetailsForm.controls["actionSearchString"].setValue(this.filter.actionSearchString, {emitEvent: false});
        this.filterLogDetailsForm.controls["streamSearchString"].setValue(this.filter.streamSearchString, {emitEvent: false});
        this.filterLogDetailsForm.controls["operatorSearchString"].setValue(this.filter.operatorSearchString, {emitEvent: false});
        this.filterLogDetailsForm.controls["isMainTimeline"].setValue(this.filter.isMainTimeline, {emitEvent: false});
        this.filterLogDetailsForm.controls["from"].setValue(this.filter.from, {emitEvent: false});
        this.filterLogDetailsForm.controls["to"].setValue(this.filter.to, {emitEvent: false});
    }

    populateModelFromForm(): void {
        this.filter.actionSearchString = this.filterLogDetailsForm.controls["actionSearchString"].value;
        this.filter.streamSearchString = this.filterLogDetailsForm.controls["streamSearchString"].value;
        this.filter.operatorSearchString = this.filterLogDetailsForm.controls["operatorSearchString"].value;
        this.filter.isMainTimeline = this.filterLogDetailsForm.controls["isMainTimeline"].value;
        this.filter.from = this.filterLogDetailsForm.controls["from"].value;
        this.filter.to = this.filterLogDetailsForm.controls["to"].value;
    }

    onSearch() {
        this.populateModelFromForm();
        if (this.oldFilterString != JSON.stringify(this.filter)) {
            this.oldFilterString = JSON.stringify(this.filter);
            this.search.emit(this.filter);
        }
    }

    onReset() {
        this.filter.resetFilter();
        this.populateFormFromModel();
        this.reset.emit(this.filter);
    }

}