import { Component, OnInit } from '@angular/core';
import { Location } from "@angular/common";
import { ActivatedRoute, Params } from "@angular/router";

import { LogDetailsFilter } from "../../../shared/model/log-details-filter.model";
import { CameraLocation } from "../../../shared/model/camera-location.model";
import { LocationService } from "../../../shared/service/location.service";
import { ActionLog } from "../../../shared/model/log/action-log.model";

declare const module;

@Component({
    moduleId: module.id,
    templateUrl: './log-details.component.html',
    providers: []
})
export class LogDetailsComponent implements OnInit {
    logDetailsFilter: LogDetailsFilter;

    logsDetails: ActionLog[];
    currentLocation: CameraLocation;


    currentPage: number;
    itemsPerPage: number;
    totalItems: number;

    constructor(private route: ActivatedRoute,
                private location: Location,
                private locationService: LocationService) {
        this.logDetailsFilter = new LogDetailsFilter();
        this.logsDetails = [];
    }

    ngOnInit(): void {
        this.logDetailsFilter = new LogDetailsFilter();

        this.route.params.forEach((params: Params) => {
            let id = +params[ 'id' ];

            if (id) {
                this.logDetailsFilter.locationId = id;
                this.locationService.getLocation(id).subscribe(location => {this.currentLocation = location;});
                this.initTableData();
            }
            else {
                this.location.back();
            }
        });
    }

    initTableData(): void {
        this.currentPage = 1;
        this.itemsPerPage = 15;

        this.refreshList(this.currentPage, this.itemsPerPage);
    }

    refreshList(pageNumber: number, pageSize: number): void {
        this.locationService.getLogDetails(pageNumber, pageSize, this.logDetailsFilter).subscribe(tableData => {
            this.logsDetails = tableData.content;
            this.totalItems = tableData.totalElements;
        });
    }

    search() {
        this.currentPage = 1;
        this.refreshList(this.currentPage, this.itemsPerPage);
    }

    doViewDetails(logDetails: ActionLog) {

    }

    public pageChanged(event: any): void {
        this.refreshList(event.page, this.itemsPerPage);
    };
}