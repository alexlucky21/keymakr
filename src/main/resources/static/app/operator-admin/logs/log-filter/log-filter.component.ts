import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";

import { LogFilter } from "../../../shared/model/log-filter.model";
import { Operator } from "../../../shared/model/operator.model";
import { Camera } from "../../../shared/model/camera.model";
import { Customer } from "../../../shared/model/customer.model";
import { OperatorService } from "../../../shared/service/operator.service";
import { CameraService } from "../../../shared/service/camera.service";
import { CustomerService } from "../../../shared/service/customer.service";

declare const module;

@Component({
    moduleId: module.id,
    selector: "log-filter",
    templateUrl: "./log-filter.component.html"
})
export class LogFilterComponent implements OnInit {
    @Input()
    filter: LogFilter;
    oldFilterString: string;

    @Output()
    search:  EventEmitter<LogFilter>;
    @Output()
    reset:  EventEmitter<LogFilter>;

    allCameras: Camera[];
    allOperators: Operator[];
    allCustomers: Customer[];

    filterLogForm: FormGroup;

    constructor(private cameraService: CameraService,
                private operatorService: OperatorService,
                private customerService: CustomerService) {
        this.filter = new LogFilter();
        this.search = new EventEmitter<LogFilter>();
        this.reset = new EventEmitter<LogFilter>();
        this.oldFilterString = "";
    }

    ngOnInit(): void {
        this.cameraService.getCameras().subscribe(cameras => { this.allCameras = cameras; });
        this.operatorService.getOperators().subscribe(operators => { this.allOperators = operators; });
        this.customerService.getCustomers().subscribe(customers => { this.allCustomers = customers; });

        this.filterLogForm = new FormGroup({
            searchString: new FormControl(this.filter.searchString),
            operator: new FormControl(this.filter.operatorId),
            customer: new FormControl(this.filter.customerId),
            camera: new FormControl(this.filter.cameraId),
            from: new FormControl(this.filter.from),
            to: new FormControl(this.filter.to)
        });

        this.filterLogForm.controls["searchString"].valueChanges.debounceTime(400).subscribe(searchString => this.onSearch());
        this.filterLogForm.controls["operator"].valueChanges.subscribe(state => this.onSearch());
        this.filterLogForm.controls["customer"].valueChanges.subscribe(state => this.onSearch());
        this.filterLogForm.controls["camera"].valueChanges.subscribe(state => this.onSearch());
        this.filterLogForm.controls["from"].valueChanges.subscribe(state => this.onSearch());
        this.filterLogForm.controls["to"].valueChanges.subscribe(state => this.onSearch());
    }

    populateFormFromModel(): void {
        this.filterLogForm.controls["searchString"].setValue(this.filter.searchString, {emitEvent: false});
        this.filterLogForm.controls["operator"].setValue(this.filter.operatorId, {emitEvent: false});
        this.filterLogForm.controls["customer"].setValue(this.filter.customerId, {emitEvent: false});
        this.filterLogForm.controls["camera"].setValue(this.filter.cameraId, {emitEvent: false});
        this.filterLogForm.controls["from"].setValue(this.filter.from, {emitEvent: false});
        this.filterLogForm.controls["to"].setValue(this.filter.to, {emitEvent: false});
    }

    populateModelFromForm(): void {
        this.filter.searchString = this.filterLogForm.controls["searchString"].value;
        this.filter.operatorId = this.filterLogForm.controls["operator"].value;
        this.filter.customerId = this.filterLogForm.controls["customer"].value;
        this.filter.cameraId = this.filterLogForm.controls["camera"].value;
        this.filter.from = this.filterLogForm.controls["from"].value;
        this.filter.to = this.filterLogForm.controls["to"].value;
    }

    onSearch() {
        this.populateModelFromForm();
        if (this.oldFilterString != JSON.stringify(this.filter)) {
            this.oldFilterString = JSON.stringify(this.filter);
            this.search.emit(this.filter);
        }
    }

    onReset() {
        this.filter.resetFilter();
        this.populateFormFromModel();
        this.reset.emit(this.filter);
    }

}