import { Component, OnInit } from '@angular/core';
import { LogFilter } from "../../shared/model/log-filter.model";
import { Router, ActivatedRoute, Params } from "@angular/router";
import { LocationService } from "../../shared/service/location.service";
import { LocationLog } from "../../shared/model/log/location-log.model";

declare const module;

@Component({
    moduleId: module.id,
    templateUrl: './logs.component.html',
    providers: []
})
export class LogsComponent implements OnInit {
    logFilter: LogFilter;

    locationLogs: LocationLog[];

    currentPage: number;
    itemsPerPage: number;
    totalItems: number;

    constructor(private router: Router, private route: ActivatedRoute, private locationService: LocationService) {
        this.locationLogs = [];
    }

    ngOnInit(): void {
        this.logFilter = new LogFilter();
        this.route.params.subscribe((params: Params) => {
            let id = +params[ 'operatorId' ];

            if (!isNaN(id)) {
                this.logFilter.operatorId = id;
            }
        });
        this.initTableData();
    }

    initTableData(): void {
        this.currentPage = 1;
        this.itemsPerPage = 15;

        this.refreshList(this.currentPage, this.itemsPerPage);
    }

    refreshList(pageNumber: number, pageSize: number): void {
        this.locationService.getLocationLog(pageNumber, pageSize, this.logFilter).subscribe(tableData => {
             this.locationLogs = tableData.content;
             this.totalItems = tableData.totalElements;
         });
    }

    doViewDetails(id?: number) {
        this.router.navigate([ "operator-admin/logs/location-details", id])
    }

    public search() {
        this.currentPage = 1;
        this.refreshList(this.currentPage, this.itemsPerPage);
    }

    public pageChanged(event: any): void {
        this.refreshList(event.page, this.itemsPerPage);
    };
}