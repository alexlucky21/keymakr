import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { OperatorAdminComponent } from "./operator-admin.component";
import { OperatorsComponent } from "./operators/operators.component";
import { OperatorFormComponent } from "./operators/operator-form/operator-form.component";
import { LogsComponent } from "./logs/logs.component";
import { StreamsComponent } from "./streams/streams.component";
import { AlertsComponent } from "./alerts/alerts.component";
import { RequestsComponent } from "./requests/requests.component";
import { ProfileComponent } from "./profile/profile.component";
import { LogDetailsComponent } from "./logs/log-details/log-details.component";

const routes: Routes = [
    {
        path: '', component: OperatorAdminComponent,
        children: [
            { path: '', redirectTo: 'operators', pathMatch: 'full' },
            { path: 'operators', component: OperatorsComponent },
            { path: 'operators/edit-operator/:id', component: OperatorFormComponent },
            { path: 'operators/edit-operator', component: OperatorFormComponent },
            { path: 'streams', component: StreamsComponent },
            { path: 'logs/location-details/:id', component: LogDetailsComponent },
            { path: 'logs', component: LogsComponent },
            { path: 'requests', component: RequestsComponent },
            { path: 'alerts', component: AlertsComponent },
            { path: 'profile', component: ProfileComponent }
        ]
    } ];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ]
})

export class OperatorAdminRoutingModule {
}