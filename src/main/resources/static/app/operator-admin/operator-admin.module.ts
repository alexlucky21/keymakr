import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { PaginationModule, ModalModule } from "ng2-bootstrap/ng2-bootstrap";
import { NKDatetimeModule } from 'ng2-datetime/ng2-datetime';

import { OperatorAdminComponent } from "./operator-admin.component";
import { OperatorAdminRoutingModule } from "./operator-admin-routing.module";
import { SharedTranslateModule } from "../shared/shared-translate.module";
import { OperatorsComponent } from "./operators/operators.component";
import { StreamsComponent } from "./streams/streams.component";
import { LogsComponent } from "./logs/logs.component";
import { LogoutModule } from "../shared/logout/logout.module";
import { RequestsComponent } from "./requests/requests.component";
import { AlertsComponent } from "./alerts/alerts.component";
import { ProfileComponent } from "./profile/profile.component";
import { OperatorFormComponent } from "./operators/operator-form/operator-form.component";
import { StreamFilterComponent } from "./streams/stream-filter/stream-filter.component";
import { StreamListItemComponent } from "./streams/stream-list-item.component";
import { AssignOperatorComponent } from "./streams/assign-modal/assign-operator-modal.component";
import { CameraPreviewComponent } from "./streams/camera-preview-modal.component";
import { OperatorFilterComponent } from "./operators/operator-filter/operator-filter.component";
import { ReactiveFormsModule } from "@angular/forms";
import { LogFilterComponent } from "./logs/log-filter/log-filter.component";
import { LogDetailsFilterComponent } from "./logs/log-details/log-details-filter/log-details-filter.component";
import { LogDetailsComponent } from "./logs/log-details/log-details.component";
import { RequestFilterComponent } from "./requests/request-filter/request-filter.component";
import { ViewRequestReasonComponent } from "./requests/view-reason-modal/view-reason-modal.component";

@NgModule({
    imports: [
        CommonModule,
        OperatorAdminRoutingModule,
        SharedTranslateModule,
        LogoutModule,
        PaginationModule,
        ModalModule,
        ReactiveFormsModule,
        NKDatetimeModule
    ],
    declarations: [
        OperatorAdminComponent,
        OperatorsComponent, OperatorFilterComponent, OperatorFormComponent,
        StreamsComponent, StreamFilterComponent, StreamListItemComponent, AssignOperatorComponent, CameraPreviewComponent,
        LogsComponent, LogFilterComponent, LogDetailsComponent, LogDetailsFilterComponent,
        RequestsComponent, RequestFilterComponent, ViewRequestReasonComponent,
        AlertsComponent, ProfileComponent ]
})

export class OperatorAdminModule {
}