import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";

import { OperatorFilter } from "../../../shared/model/operator-filter.model";

declare const module;

@Component({
moduleId: module.id,
selector: "operator-filter",
    templateUrl: "operator-filter.component.html"
})
export class OperatorFilterComponent implements OnInit {
    @Input()
    filter: OperatorFilter;
    oldFilterString: string;

    @Output()
    search:  EventEmitter<OperatorFilter>;
    @Output()
    reset:  EventEmitter<OperatorFilter>;

    filterOperatorForm: FormGroup;

    constructor() {
        this.filter = new OperatorFilter();
        this.search = new EventEmitter<OperatorFilter>();
        this.reset = new EventEmitter<OperatorFilter>();
        this.oldFilterString = "";
    }

    ngOnInit(): void {
        this.filterOperatorForm = new FormGroup({
            searchString: new FormControl(this.filter.searchString),
            state: new FormControl(this.filter.state)
        });

        this.filterOperatorForm.controls["searchString"].valueChanges.debounceTime(400).subscribe(searchString => this.onSearch());
        this.filterOperatorForm.controls["state"].valueChanges.subscribe(state => this.onSearch());
    }

    populateFormFromModel(): void {
        this.filterOperatorForm.controls["searchString"].setValue(this.filter.searchString, {emitEvent: false});
        this.filterOperatorForm.controls["state"].setValue(this.filter.state, {emitEvent: false});
    }

    populateModelFromForm(): void {
        this.filter.searchString = this.filterOperatorForm.controls["searchString"].value;
        this.filter.state = this.filterOperatorForm.controls["state"].value;
    }

    onSearch() {
        this.populateModelFromForm();
        if (this.oldFilterString != JSON.stringify(this.filter)) {
            this.oldFilterString = JSON.stringify(this.filter);
            this.search.emit(this.filter);
        }
    }

    onReset() {
        this.filter.resetFilter();
        this.populateFormFromModel();
        this.reset.emit(this.filter);
    }

}