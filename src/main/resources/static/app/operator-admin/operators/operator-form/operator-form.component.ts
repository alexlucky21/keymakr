import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Params, Router } from "@angular/router";

import { Operator } from "../../../shared/model/operator.model";
import { Camera } from "../../../shared/model/camera.model";
import { OperatorService } from "../../../shared/service/operator.service";

declare const module;

@Component({
moduleId: module.id,
    templateUrl: "./operator-form.component.html"
})

export class OperatorFormComponent implements OnInit {
    operator: Operator;
    camerasAssignedToOperator: Camera[];

    isNew: boolean;

    constructor(private route: ActivatedRoute,
                private router: Router,
                private operatorService: OperatorService) {
        this.operator = new Operator();
        this.camerasAssignedToOperator = [];
    }

    ngOnInit() {
        this.route.params.forEach((params: Params) => {
            let id = +params[ 'id' ];
            this.isNew = isNaN(id);
            if (!this.isNew) {
                this.getOperator(id);
            }
        });
    }

    getOperator(id: number) {
        this.operatorService.getOperator(id).subscribe(operator => {
            this.operator = operator;
            this.operatorService.getCamerasOfOperator(id).subscribe(cameras => {
                this.camerasAssignedToOperator = cameras;
            });
        });
    }


    onSubmit(): void {
        this.operatorService.saveOperator(this.operator).subscribe(operator => {
            this.router.navigate(["operator-admin/operators"]);
        });
        //TODO: Processing of Server error
    }

    onCancel(): void {
        this.router.navigate(["operator-admin/operators"]);
    }
}