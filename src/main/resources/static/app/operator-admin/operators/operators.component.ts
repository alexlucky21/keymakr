import { Component, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { OperatorFilter } from "../../shared/model/operator-filter.model";
import { OperatorService } from "../../shared/service/operator.service";
import { Operator } from "../../shared/model/operator.model";
import { ModalDirective } from "ng2-bootstrap/ng2-bootstrap";

declare const module;

@Component({
    moduleId: module.id,
    templateUrl: './operators.component.html',
    providers: []
})

export class OperatorsComponent implements OnInit {
    @ViewChild('confirmDeleteModal')
    public confirmDeleteModal: ModalDirective;

    operatorFilter: OperatorFilter;

    operators: Operator[];
    selectedOperator: Operator;

    currentPage: number;
    itemsPerPage: number;
    totalItems: number;

    constructor(private router: Router,
                private operatorService: OperatorService) {
    }

    ngOnInit(): void {
        this.operatorFilter = new OperatorFilter();
        this.initTableData();
    }

    initTableData(): void {
        this.currentPage = 1;
        this.itemsPerPage = 15;

        this.refreshList(this.currentPage, this.itemsPerPage);
    }

    public search() {
        this.currentPage = 1;
        this.refreshList(this.currentPage, this.itemsPerPage);
    }

    refreshList(pageNumber: number, pageSize: number): void {
        this.operatorService.getFilteredOperators(pageNumber, pageSize, this.operatorFilter).subscribe(tableData => {
            this.operators = tableData.content;
            this.totalItems = tableData.totalElements;
        });
    }

    isOperatorObservable(operator: Operator): boolean {
        return (operator.state != "NOT_WORKING");
    }

    doEdit(id?: number) {
        this.router.navigate([ "operator-admin/operators/edit-operator", id])
    }

    doDelete() {
        this.operatorService.deleteOperator(this.selectedOperator.id).subscribe(operatorId=>{
            this.selectedOperator = undefined;
            this.confirmDeleteModal.hide();
            this.refreshList(this.currentPage, this.itemsPerPage);
        });
    }

    doViewLog(operatorId: number) {
        this.router.navigate(["operator-admin/logs", { operatorId: operatorId }]);
    }

    doObserve(operatorId: number) {
        console.log("doObserve : not yet implemented");
    }

    public pageChanged(event: any): void {
        this.refreshList(event.page, this.itemsPerPage);
    };

    //Delete confirmation modal methods
    public showConfirmDeleteModal(operator: Operator):void {
        this.selectedOperator = operator;
        this.confirmDeleteModal.show();
    }

    onHide(): void {
        this.selectedOperator = undefined;
    }

}