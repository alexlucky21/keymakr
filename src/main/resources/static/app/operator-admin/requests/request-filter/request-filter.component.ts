import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { FormGroup } from "@angular/forms";

import { RequestFilter } from "../../../shared/model/request-filter.model";

declare const module;

@Component({
moduleId: module.id,
selector: "request-filter",
    templateUrl: "request-filter.component.html"
})
export class RequestFilterComponent implements OnInit {
    @Input()
    filter: RequestFilter;
    oldFilterString: string;

    @Output()
    search:  EventEmitter<RequestFilter>;
    @Output()
    reset:  EventEmitter<RequestFilter>;

    filterOperatorForm: FormGroup;

    constructor() {
        this.filter = new RequestFilter();
        this.search = new EventEmitter<RequestFilter>();
        this.reset = new EventEmitter<RequestFilter>();
        this.oldFilterString = "";
    }

    ngOnInit(): void {
        this.filterOperatorForm = new FormGroup({
        });

        // this.filterOperatorForm.controls["searchString"].valueChanges.debounceTime(400).subscribe(searchString => this.onSearch());
    }

    populateFormFromModel(): void {
        // this.filterOperatorForm.controls["searchString"].setValue(this.filter.searchString, {emitEvent: false});
    }

    populateModelFromForm(): void {
        // this.filter.searchString = this.filterOperatorForm.controls["searchString"].value;
    }

    onSearch() {
        this.populateModelFromForm();
        if (this.oldFilterString != JSON.stringify(this.filter)) {
            this.oldFilterString = JSON.stringify(this.filter);
            this.search.emit(this.filter);
        }
    }

    onReset() {
        this.filter.resetFilter();
        this.populateFormFromModel();
        this.reset.emit(this.filter);
    }

}