import { Component, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { Subscription } from "rxjs";
import { RequestFilter } from "../../shared/model/request-filter.model";
import { OperatorService } from "../../shared/service/operator.service";
import { OperatorRequest } from "../../shared/model/operator-request.model";
import { AssignOperatorComponent } from "../streams/assign-modal/assign-operator-modal.component";
import { CameraOperator } from "../../shared/model/camera-operator.model";
import { CameraService } from "../../shared/service/camera.service";
import { ViewRequestReasonComponent } from "./view-reason-modal/view-reason-modal.component";

declare const module;

@Component({
    moduleId: module.id,
    selector: 'requests',
    templateUrl: './requests.component.html',
    providers: []
})

export class RequestsComponent implements OnInit {
    @ViewChild('assignModal')
    assignModal: AssignOperatorComponent;

    @ViewChild('viewReasonModal')
    public viewReasonModal: ViewRequestReasonComponent;

    requestFilter: RequestFilter;

    requests: OperatorRequest[];

    assignModalSubscription: Subscription;
    currentPage: number;
    itemsPerPage: number;
    totalItems: number;

    constructor(private router: Router,
                private cameraService: CameraService,
                private operatorService: OperatorService) {
    }

    ngOnInit(): void {
        this.requestFilter = new RequestFilter();
        this.initTableData();
    }

    initTableData(): void {
        this.currentPage = 1;
        this.itemsPerPage = 15;

        this.refreshList(this.currentPage, this.itemsPerPage);
    }

    public search() {
        this.currentPage = 1;
        this.refreshList(this.currentPage, this.itemsPerPage);
    }

    refreshList(pageNumber: number, pageSize: number): void {
        this.operatorService.getOperatorRequests(pageNumber, pageSize, this.requestFilter).subscribe(tableData => {
            let justViewedRequests: number[];

            this.requests = tableData.content;
            this.totalItems = tableData.totalElements;
            justViewedRequests = this.requests
                                        .filter(operatorRequest=>!operatorRequest.isRequestViewed)
                                        .map(operatorRequest=>operatorRequest.id);
            this.operatorService.markRequestsAsViewed(justViewedRequests);
        });
    }

    doViewReason(request: OperatorRequest) {
        this.viewReasonModal.show(request);
    }

    doChangeOperator(request: OperatorRequest) {
        this.assignModal.show(request.fromCamera2Operator.camera);
        this.assignModalSubscription = this.assignModal.assign.subscribe((cameraOperator: CameraOperator) =>{

            this.cameraService.reassignCamera(request, cameraOperator).subscribe(cameraOperatorSaved => {
                this.assignModal.hide();
                this.assignModalSubscription.unsubscribe();
                this.search();
            });
        })
    }



    public pageChanged(event: any): void {
        this.refreshList(event.page, this.itemsPerPage);
    };

}