import { Component, OnInit, ViewChild, Output, EventEmitter } from "@angular/core";
import { ModalDirective } from "ng2-bootstrap/ng2-bootstrap";
import { ApplicationSettingsService } from "../../../shared/service/application-settings.service";
import { OperatorRequest } from "../../../shared/model/operator-request.model";

declare const module;

@Component({
    moduleId: module.id,
    selector: "view-reason-modal",
    templateUrl: "view-reason-modal.component.html"
})
export class ViewRequestReasonComponent implements OnInit {
    @ViewChild('viewReasonModal')
    modal: ModalDirective;

    @Output()
    reassign: EventEmitter<OperatorRequest>;

    operatorRequest: OperatorRequest;

    constructor(private settingsService: ApplicationSettingsService) {
        this.reassign = new EventEmitter<OperatorRequest>();
    }

    ngOnInit(): void {
        this.hide();
    }

    show(operatorRequest: OperatorRequest): void {
        this.operatorRequest = operatorRequest;
        this.modal.show();
    }

    hide(): void {
        this.modal.hide();
    }

    onReassign(): void {
        this.hide();
        this.reassign.emit(this.operatorRequest);
        this.operatorRequest = undefined;
    }
}