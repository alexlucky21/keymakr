import { Component, OnInit, ViewChild, EventEmitter, Output } from "@angular/core";
import { ModalDirective } from "ng2-bootstrap/ng2-bootstrap";
import { Camera } from "../../../shared/model/camera.model";
import { Operator } from "../../../shared/model/operator.model";
import { OperatorService } from "../../../shared/service/operator.service";
import { CameraOperator } from "../../../shared/model/camera-operator.model";
import { OperatorAssignment } from "../../../shared/model/operator-assignment.model";
import { ApplicationSettingsService } from "../../../shared/service/application-settings.service";

declare const module;

@Component({
    moduleId: module.id,
    selector: "assign-operator-modal",
    templateUrl: "./assign-operator-modal.component.html"
})
export class AssignOperatorComponent implements OnInit {
    MAX_CAMERAS_PER_OPERATOR: number;

    cameraOperator: CameraOperator;
    suggestedOperatorsFiltered: OperatorAssignment[];
    otherOperatorsFiltered: OperatorAssignment[];

    suggestedOperators: OperatorAssignment[];
    otherOperators: OperatorAssignment[];
    searchString: string;

    @Output()
    assign: EventEmitter<CameraOperator>;

    @ViewChild('assignOperatorModal')
    modal: ModalDirective;

    constructor(private operatorService: OperatorService, private settingsService: ApplicationSettingsService) {
        this.assign = new EventEmitter<CameraOperator>();
        this.searchString = "";
        this.suggestedOperators = [];
        this.otherOperators = [];
        this.suggestedOperatorsFiltered = [];
        this.otherOperatorsFiltered = [];
        this.MAX_CAMERAS_PER_OPERATOR = this.settingsService.get("MAX_CAMERAS_PER_OPERATOR");
    }

    ngOnInit(): void {
        this.hide();
    }

    show(camera: Camera): void {
        this.cameraOperator = new CameraOperator();
        this.cameraOperator.camera = camera;
        this.cameraOperator.isMain = true;
        this.searchString = "";
        this.getOperatorListsFromServer();
        this.modal.show();
    }


    getOperatorListsFromServer(): void {
        this.suggestedOperatorsFiltered = [];
        this.otherOperatorsFiltered = [];
        this.operatorService.getSuggestedOperatorAssignments(this.cameraOperator.camera.location.id, this.cameraOperator.camera.id, this.searchString)
        .subscribe(operatorAssignments => {
            this.suggestedOperators = operatorAssignments;
            this.suggestedOperatorsFiltered = this.suggestedOperators.map(item => item);
        });
        this.operatorService.getOtherOperatorAssignments(this.cameraOperator.camera.location.id, this.cameraOperator.camera.id, this.searchString)
        .subscribe(operatorAssignments => {
            this.otherOperators = operatorAssignments;
            this.otherOperatorsFiltered = this.otherOperators.map(item => item);
        });
    }

    hide(): void {
        this.modal.hide();
        this.cameraOperator = null;
        this.suggestedOperatorsFiltered = [];
        this.otherOperatorsFiltered = [];
        this.suggestedOperators = [];
        this.otherOperators = [];
    }

    filterOperatorLists(): void {
        if (this.searchString && this.searchString.trim()) {
            this.suggestedOperatorsFiltered = this.suggestedOperators.filter((item: OperatorAssignment) => this.isNameOfOperatorLike(item, this.searchString));
            this.otherOperatorsFiltered = this.otherOperators.filter((item: OperatorAssignment) => this.isNameOfOperatorLike(item, this.searchString));
        }
        else {
            this.suggestedOperatorsFiltered = this.suggestedOperators.map(item => item);
            this.otherOperatorsFiltered = this.otherOperators.map(item => item);
        }
    }

    isNameOfOperatorLike(operatorAssignment: OperatorAssignment, queryString): boolean {
        var name = (operatorAssignment.operator.profile.firstName + " " + operatorAssignment.operator.profile.lastName).toLowerCase();

        queryString = queryString.trim().toLowerCase();

        return (name.indexOf(queryString) != -1);
    }

    onSelectOperator(operator: Operator): void {
        this.cameraOperator.operator = operator;
    }

    onAssign(): void {
        this.assign.emit(this.cameraOperator);
    }
}