import { Component, OnInit, ViewChild } from "@angular/core";
import { ModalDirective } from "ng2-bootstrap/ng2-bootstrap";

import { Camera } from "../../shared/model/camera.model";

declare const module;

@Component({
moduleId: module.id,
    selector: "camera-preview-modal",
    templateUrl: "./camera-preview-modal.component.html"
})
export class CameraPreviewComponent implements OnInit {
    camera: Camera;

    @ViewChild('cameraPreviewModal')
    modal: ModalDirective;

    constructor() {
    }

    ngOnInit(): void {
        this.modal.hide();
    }

    hide(): void {
        this.modal.hide();
        this.camera = null;
    }

    show(camera: Camera): void {
        this.camera = camera;
        this.modal.show();
    }


}