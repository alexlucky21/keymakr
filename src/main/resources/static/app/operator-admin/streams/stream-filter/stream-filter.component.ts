import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { FormGroup, FormControl } from "@angular/forms";

import { StreamFilter } from "../../../shared/model/stream-filter.model";
import { Camera } from "../../../shared/model/camera.model";
import { CameraLocation } from "../../../shared/model/camera-location.model";
import { Operator } from "../../../shared/model/operator.model";
import { Customer } from "../../../shared/model/customer.model";
import { CustomerService } from "../../../shared/service/customer.service";
import { OperatorService } from "../../../shared/service/operator.service";
import { LocationService } from "../../../shared/service/location.service";
import { CameraService } from "../../../shared/service/camera.service";

declare const module;

@Component({
    moduleId: module.id,
    selector: "stream-filter",
    templateUrl: "./stream-filter.component.html"
})
export class StreamFilterComponent implements OnInit {
    @Input()
    filter: StreamFilter;
    oldFilterString: string;

    @Output()
    search:  EventEmitter<StreamFilter>;

    @Output()
    reset:  EventEmitter<StreamFilter>;

    allCameras: Camera[];
    allLocations: CameraLocation[];
    allOperators: Operator[];
    allCustomers: Customer[];

    filterStreamForm: FormGroup;

    constructor(private cameraService: CameraService,
                private locationService: LocationService,
                private operatorService: OperatorService,
                private customerService: CustomerService) {
        this.filter = new StreamFilter();
        this.search = new EventEmitter<StreamFilter>();
        this.reset = new EventEmitter<StreamFilter>();
        this.oldFilterString = "";
    }

    ngOnInit(): void {
        this.cameraService.getCameras().subscribe(cameras => { this.allCameras = cameras; });
        this.locationService.getLocations().subscribe(locations => { this.allLocations = locations; });
        this.operatorService.getOperators().subscribe(operators => { this.allOperators = operators; });
        this.customerService.getCustomers().subscribe(customers => { this.allCustomers = customers; });

        this.filterStreamForm = new FormGroup({
            searchString: new FormControl(this.filter.searchString),
            state: new FormControl(this.filter.state),
            operator: new FormControl(this.filter.operatorId),
            camera: new FormControl(this.filter.cameraId),
            location: new FormControl(this.filter.locationId),
            customer: new FormControl(this.filter.customerId)
        });

        this.filterStreamForm.controls["searchString"].valueChanges.debounceTime(400).subscribe(searchString => this.onSearch());
        this.filterStreamForm.controls["state"].valueChanges.subscribe(state => this.onSearch());
        this.filterStreamForm.controls["operator"].valueChanges.subscribe(operator => this.onSearch());
        this.filterStreamForm.controls["camera"].valueChanges.subscribe(camera => this.onSearch());
        this.filterStreamForm.controls["location"].valueChanges.subscribe(location => this.onSearch());
        this.filterStreamForm.controls["customer"].valueChanges.subscribe(customer => this.onSearch());
    }

    populateFormFromModel(): void {
        this.filterStreamForm.controls["searchString"].setValue(this.filter.searchString, {emitEvent: false});
        this.filterStreamForm.controls["state"].setValue(this.filter.state, {emitEvent: false});
        this.filterStreamForm.controls["operator"].setValue(this.filter.operatorId, {emitEvent: false});
        this.filterStreamForm.controls["camera"].setValue(this.filter.cameraId, {emitEvent: false});
        this.filterStreamForm.controls["location"].setValue(this.filter.locationId, {emitEvent: false});
        this.filterStreamForm.controls["customer"].setValue(this.filter.customerId, {emitEvent: false});
    }

    populateModelFromForm(): void {
        this.filter.searchString = this.filterStreamForm.controls["searchString"].value;
        this.filter.state = this.filterStreamForm.controls["state"].value;
        this.filter.operatorId = this.filterStreamForm.controls["operator"].value;
        this.filter.cameraId = this.filterStreamForm.controls["camera"].value;
        this.filter.locationId = this.filterStreamForm.controls["location"].value;
        this.filter.customerId = this.filterStreamForm.controls["customer"].value;
    }

    onSearch() {
        this.populateModelFromForm();
        if (this.oldFilterString != JSON.stringify(this.filter)) {
            this.oldFilterString = JSON.stringify(this.filter);
            this.search.emit(this.filter);
        }
    }

    onReset() {
        this.filter.resetFilter();
        this.populateFormFromModel();

        this.reset.emit(this.filter);
    }

}