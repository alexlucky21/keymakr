import { Component, OnInit, Input } from "@angular/core";
import { Subscription } from "rxjs";

import { OperatorService } from "../../shared/service/operator.service";
import { CameraService } from "../../shared/service/camera.service";
import { CameraAssignment } from "../../shared/model/camera-assignment.model";
import { CameraOperator } from "../../shared/model/camera-operator.model";
import { AssignOperatorComponent } from "./assign-modal/assign-operator-modal.component";
import { CameraPreviewComponent } from "./camera-preview-modal.component";

declare const module;

@Component({
    moduleId: module.id,
    selector: "stream-list-item",
    templateUrl: "./stream-list-item.component.html"
})
export class StreamListItemComponent implements OnInit {
    @Input()
    cameraAssignment: CameraAssignment;
    @Input()
    isHeader: boolean;
    @Input()
    assignModal: AssignOperatorComponent;
    @Input()
    previewModal: CameraPreviewComponent;

    assignModalSubscription: Subscription;
    cameraOperators: CameraOperator[];
    deletedCameraOperators: CameraOperator[];
    isDetailsShown: boolean;
    isOperatorsShown: boolean;

    constructor(private cameraService: CameraService, private operatorService: OperatorService) {
        this.isDetailsShown = false;
        this.isOperatorsShown = false;
        this.isHeader = false;
        this.cameraOperators = [];
        this.deletedCameraOperators = [];
    }

    ngOnInit(): void {

    }

    onPreview() {
        this.previewModal.show(this.cameraAssignment.camera);
    }

    onAssign() {
        this.assignModal.show(this.cameraAssignment.camera);
        this.assignModalSubscription = this.assignModal.assign.subscribe((cameraOperator: CameraOperator) =>{

            this.operatorService.assignCameraToOperator(cameraOperator).subscribe(cameraOperatorSaved => {
                this.assignModal.hide();
                this.assignModalSubscription.unsubscribe();
                this.updateOperatorList();
            });
        })
    }

    updateOperatorList() {
        this.cameraService.getCameraOperatorsByCameraId(this.cameraAssignment.camera.id).subscribe(cameraOperators => {
            this.cameraOperators = cameraOperators;
            this.cameraAssignment.total = cameraOperators.length;
        });
    }


    onMakeMainOperator(cameraOperator: CameraOperator) {
        cameraOperator.isMain = true;

        this.cameraService.assignOperatorToCamera(cameraOperator).subscribe(cameraOperatorSaved=>{
            this.updateOperatorList();
        });
    }

    onDeleteOperator(cameraOperator: CameraOperator) {
        this.cameraService.unassignOperatorFromCamera(cameraOperator).subscribe(cameraOperatorSaved=>{
            cameraOperator.isMain = false;
            this.deletedCameraOperators.push(cameraOperator);
            this.updateOperatorList();
        });
    }

    onUnDeleteOperator(cameraOperator: CameraOperator) {
        this.cameraService.assignOperatorToCamera(cameraOperator).subscribe(cameraOperatorSaved=>{
            this.deletedCameraOperators = this.deletedCameraOperators.filter(deletedCameraOperatorSaved=>{
                return deletedCameraOperatorSaved.id !== cameraOperator.id;
            });
            this.updateOperatorList();
        });
    }

    toggleDetails() {
        this.isDetailsShown = !this.isDetailsShown;
    }

    toggleOperators() {
        this.isOperatorsShown = !this.isOperatorsShown;
        if (this.isOperatorsShown) {
            this.cameraService.getCameraOperatorsByCameraId(this.cameraAssignment.camera.id).subscribe(cameraOperators => this.cameraOperators = cameraOperators);
        }
    }
}