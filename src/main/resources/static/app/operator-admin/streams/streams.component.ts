declare const module;

import { Component, OnInit, ViewChild } from "@angular/core";
import { StreamFilter } from "../../shared/model/stream-filter.model";
import { AssignOperatorComponent } from "./assign-modal/assign-operator-modal.component";
import { CameraPreviewComponent } from "./camera-preview-modal.component";
import { CameraService } from "../../shared/service/camera.service";
import { CameraAssignment } from "../../shared/model/camera-assignment.model";

@Component({
    moduleId: module.id,
    selector: 'operators',
    templateUrl: './streams.component.html',
    providers: []
})

export class StreamsComponent implements OnInit {
    @ViewChild('assignModal')
    assignModal: AssignOperatorComponent;

    @ViewChild('previewModal')
    previewModal: CameraPreviewComponent;

    streamFilter: StreamFilter;

    currentPage: number;
    itemsPerPage: number;
    totalItems: number;

    cameraAssignments: CameraAssignment[];

    constructor(private cameraService: CameraService) {
    }

    ngOnInit(): void {
        this.currentPage = 1;
        this.itemsPerPage = 15;

        this.streamFilter = new StreamFilter();

        this.refreshCameraList(this.currentPage, this.itemsPerPage);
    }

    private refreshCameraList(pageNumber: number, pageSize: number) {
        this.cameraService.getFilteredCameraAssignments(pageNumber, pageSize, this.streamFilter).subscribe(tableData => {
            this.cameraAssignments = tableData.content;
            this.totalItems = tableData.totalElements;
        });
    }

    public search() {
        this.currentPage = 1;
        this.refreshCameraList(this.currentPage, this.itemsPerPage);
    }

    public pageChanged(event: any): void {
        this.refreshCameraList(event.page, this.itemsPerPage);
    };
}