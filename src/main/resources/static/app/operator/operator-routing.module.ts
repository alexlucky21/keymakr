import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { OperatorComponent } from "./operator.component";
import { StreamsComponent } from "./streams/streams.component";

const routes: Routes = [
    {
        path: '', component: OperatorComponent,
        children: [
            { path: '', redirectTo: 'streams', pathMatch: 'full' },
            { path: 'streams', component: StreamsComponent }
        ]
    } ];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ]
})

export class OperatorRoutingModule {
}