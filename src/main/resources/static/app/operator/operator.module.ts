import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { OperatorComponent } from "./operator.component";
import { OperatorRoutingModule } from "./operator-routing.module";
import { StreamsComponent } from "./streams/streams.component";
import { LogoutModule } from "../shared/logout/logout.module";
import { SharedTranslateModule } from "../shared/shared-translate.module";
import { StreamComponent } from "./streams/stream/stream.component";
import { StreamService } from "../shared/service/stream/stream.service";
import { FormsModule } from "@angular/forms";

@NgModule({
    imports: [ CommonModule, OperatorRoutingModule, FormsModule, LogoutModule, SharedTranslateModule ],
    declarations: [ OperatorComponent, StreamsComponent, StreamComponent ],
    providers: [ StreamService ]
})

export class OperatorModule {
}