import {
    Component, OnInit, ElementRef, Input, AfterViewInit, OnDestroy, ViewChild, Output,
    EventEmitter
} from '@angular/core';
declare var videojs;

@Component({
    selector: 'stream',
    templateUrl: './app/operator/streams/stream/stream.component.html',
    providers: [],
    host: {
        '(window:resize)': 'onResize($event)'
    }
})

export class StreamComponent implements AfterViewInit, OnInit, OnDestroy {

    private _elementRef: ElementRef;
    private videoJSplayer : any;

    @Input()
    streamName: string;

    @Input()
    streamUrl: string;

    @ViewChild('player')
    player: any;

    @Output()
    onAfterInit: EventEmitter<boolean>;

    playerWidth: number;
    playerHeight: number;

    constructor(elementRef: ElementRef) {
        this._elementRef = elementRef;
        this.onAfterInit = new EventEmitter<boolean>();
    }

    ngOnInit() {
        this.playerWidth = this._elementRef.nativeElement.parentNode.clientWidth - 10;
        this.playerHeight = this.playerWidth * 0.69;
    }

    onResize(event) {
        this.doResize();
    }

    ngAfterViewInit(): void {
        this.videoJSplayer = videojs(this.player.nativeElement, {}, function() {
            // This is functionally the same as the previous example.
        });
        this.videoJSplayer.play();
        this.onAfterInit.emit(true);
    }

    doResize(): void {
        let playerWidthL = this._elementRef.nativeElement.parentNode.clientWidth - 10;
        let playerHeightL = playerWidthL * 0.69;

        this.videoJSplayer.width(playerWidthL);
        this.videoJSplayer.height(playerHeightL);
    }


    ngOnDestroy() {
        this.videoJSplayer.dispose();
    }
}