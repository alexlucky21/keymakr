import { Component, OnInit, ViewChild, ElementRef, ViewChildren, QueryList, AfterViewInit } from '@angular/core';
import { Stream } from "../../shared/model/stream/stream.model";
import { StreamComponent } from "./stream/stream.component";
import { StreamService } from "../../shared/service/stream/stream.service";

@Component({
    selector: 'streams',
    templateUrl: './app/operator/streams/streams.component.html',
    providers: []
})

export class StreamsComponent implements OnInit {

    streamList: Stream[];
    streamCount: number;

    defaultStreamList: Stream[];
    selectedStreamId: number;
    removeStreamId: number;

    firstStreamClass: string;
    secondStreamClass: string;
    thirdStreamClass: string;
    fourthStreamClass: string;
    fifthStreamClass: string;
    sixthStreamClass: string;

    @ViewChildren('streams')
    streams: QueryList<StreamComponent>;



    constructor(private elementRef: ElementRef,
                private streamService: StreamService) {
        this.streamList = [];
        this.defaultStreamList = [];
        this.selectedStreamId = 0;
        this.removeStreamId = 0;
        this.streamCount = 0;
        this.firstStreamClass = '';
        this.secondStreamClass = '';
        this.thirdStreamClass = '';
        this.fourthStreamClass = '';
        this.fifthStreamClass = '';
        this.sixthStreamClass = '';
    }

    ngOnInit(): void {
        this.initStreamList();

        let jwStream = new Stream();
        jwStream.id = 1;
        jwStream.name = 'JWPlayer';
        jwStream.url = 'https://wowza.jwplayer.com/live/jelly.stream/playlist.m3u8?DVR';
        this.defaultStreamList.push(jwStream);

        let wowStream = new Stream();
        wowStream.id = 2;
        wowStream.name = 'Cartoon';
        wowStream.url = 'https://wowzaec2demo.streamlock.net/live/bigbuckbunny/playlist.m3u8';
        this.defaultStreamList.push(wowStream);
    }

    initStreamList(): void {
        this.streamService.getStreams()
            .subscribe(streamList => {
                this.streamList = streamList;
            });
    }

    addStream(): void {
        let selectedStream = this.defaultStreamList.filter(stream => (stream.id == this.selectedStreamId))[0];

        this.streamService.addStream(selectedStream.name, selectedStream.url);
        this.streamCount++;
        this.selectedStreamId = 0;
    }

    removeStream(): void {
        this.streamCount--;
        this.streamService.removeStream(this.removeStreamId)
            .subscribe(result => setTimeout(() => this.refreshLayout(), 50));
        this.removeStreamId = 0;
    }

    getClass(index: number): string {
        let className = '';
        if (index == 0) {
            className = this.getFirstStreamClass();
        } else if (index == 1) {
            className = this.getSecondStreamClass();
        } else if (index == 2) {
            className = this.getThirdStreamClass();
        } else if (index == 3) {
            className = this.getFourthStreamClass();
        } else if (index == 4) {
            className = this.getFifthStreamClass();
        } else if (index == 5) {
            className = this.getSixthStreamClass();
        }

        return className;
    }

    getFirstStreamClass(): string {
        let className: string = '';
        if (this.streamCount == 1) {
            className = "col-md-8 col-md-offset-2";
        } else if (this.streamCount == 2) {
            className = "col-md-6";
        } else if (this.streamCount == 3 || this.streamCount == 4) {
            className = "col-md-4 col-md-offset-2";
        } else if (this.streamCount == 5 || this.streamCount == 6) {
            className = "col-md-4";
        }

        return className;
    }

    getSecondStreamClass(): string {
        let className: string = '';
        if (this.streamCount == 1) {
            className = "col-md-2";
        } else if (this.streamCount == 2) {
            className = "col-md-6";
        } else if (this.streamCount == 3 || this.streamCount == 4) {
            className = "col-md-4";
        } else if (this.streamCount == 5 || this.streamCount == 6) {
            className = "col-md-4";
        }

        return className;
    }

    getThirdStreamClass(): string {
        let className: string = '';
        if (this.streamCount == 3) {
            className = "col-md-4 col-md-offset-4";
        } else if (this.streamCount == 4) {
            className = "col-md-4 col-md-offset-2";
        } else if (this.streamCount == 5 || this.streamCount == 6) {
            className = "col-md-4";
        }

        return className;
    }

    getFourthStreamClass(): string {
        let className: string = '';
        if (this.streamCount == 4 || this.streamCount == 6) {
            className = "col-md-4";
        } else if (this.streamCount == 5) {
            className = "col-md-4 col-md-offset-2";
        }

        return className;
    }

    getFifthStreamClass(): string {
        let className: string = '';
        if (this.streamCount == 5 || this.streamCount == 6) {
            className = "col-md-4";
        }

        return className;
    }

    getSixthStreamClass(): string {
        let className: string = '';
        if (this.streamCount == 6) {
            className = "col-md-4";
        }

        return className;
    }

    refreshLayout(): void {
        if (this.streams) {
            this.streams.forEach(stream => stream.doResize());
        }
    }

}