import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { GuestComponent } from "./guest/guest.component";
import { RegistrationFormComponent } from "./guest/registration-form/registration-form.component";

//TODO add CanActivate options
const routes: Routes = [
    { path: 'customer', loadChildren: 'app/customer/customer.module#CustomerModule' },
    { path: 'operator', loadChildren: 'app/operator/operator.module#OperatorModule' },
    { path: 'customer-admin', loadChildren: 'app/customer-admin/customer-admin.module#CustomerAdminModule' },
    { path: 'operator-admin', loadChildren: 'app/operator-admin/operator-admin.module#OperatorAdminModule' },
    { path: 'main-admin', loadChildren: 'app/main-admin/main-admin.module#MainAdminModule' },
    { path: '', component: GuestComponent },
    { path: 'registration', component: RegistrationFormComponent }
];

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})

export class RoutingModule {
}