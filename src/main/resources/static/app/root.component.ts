import { Component, OnInit, ViewContainerRef } from "@angular/core";
import { Router } from "@angular/router";
import { TranslateService } from 'ng2-translate';

import { AuthenticationService } from "./shared/service/authentication.service";

@Component({
    selector: "root",
    templateUrl: "./app/root.component.html"
})

export class RootComponent implements OnInit {
    constructor(private auth: AuthenticationService,
                private router: Router,
                private translate: TranslateService) {
    }

    ngOnInit(): void {
        this.initTranslation();
        this.auth.restoreSessionData().subscribe( (user) => {
            if (user && user.username) {
                this.auth.role = user.authorities[ 0 ].authority;
                this.auth.isLoggedIn = true;
            }
            else {
                this.auth.role = undefined;
                this.auth.isLoggedIn = false;
                this.router.navigate([ this.auth.routePathByRole() ]);
            }
        });
    }

    initTranslation() {
        var userLang = this.translate.getBrowserLang();

        userLang = /(en|ru)/gi.test(userLang) ? userLang : 'en';

        this.translate.addLangs(['en', 'ru']);
        this.translate.setDefaultLang('en');
        this.translate.use(userLang);
    }

}