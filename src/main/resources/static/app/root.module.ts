import "rxjs/add/operator/map";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { HttpModule } from "@angular/http";
import { RootComponent } from "./root.component";
import { RoutingModule } from "./root-routing.module";
import { GuestComponent } from "./guest/guest.component";
import { LoginComponent } from "./guest/login-form/login-form.component";
import { AuthenticationService } from "./shared/service/authentication.service";
import { SharedTranslateModule } from "./shared/shared-translate.module";
import { RegistrationFormComponent } from "./guest/registration-form/registration-form.component";
import { ReCaptchaModule } from 'angular2-recaptcha';
import { RecaptchaService } from "./shared/service/recaptcha.service";
import { ApplicationService } from "./shared/service/application/application.service";
import { CustomFormsModule } from "ng2-validation";
import { TimezoneService } from "./shared/service/timezone.service";
import { RegistrationLocationComponent } from "./guest/registration-form/registration-location/registration-location.component";
import { RegistrationCameraComponent } from "./guest/registration-form/registration-location/registration-camera/registration-camera.component";
import { ModalModule } from "ng2-bootstrap/ng2-bootstrap";
import { CameraService } from "./shared/service/camera.service";
import { CustomerService } from "./shared/service/customer.service";
import { OperatorService } from "./shared/service/operator.service";
import { LocationService } from "./shared/service/location.service";
import { ApplicationSettingsService } from "./shared/service/application-settings.service";

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        RoutingModule,
        ReCaptchaModule,
        CustomFormsModule,
        ModalModule,
        SharedTranslateModule.forRoot()
    ],
    exports: [ ],
    declarations: [
        RootComponent,
        GuestComponent,
        LoginComponent,
        RegistrationFormComponent,
        RegistrationLocationComponent,
        RegistrationCameraComponent
    ],
    providers: [
        AuthenticationService,
        ApplicationSettingsService,
        RecaptchaService,
        ApplicationService,
        TimezoneService,
        CameraService,
        CustomerService,
        OperatorService,
        LocationService
    ],
    bootstrap: [ RootComponent ]
})

export class RootModule {
}

