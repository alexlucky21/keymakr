import { Component } from "@angular/core";
import { TranslateService } from "ng2-translate";

@Component({
    selector: "language-switcher",
    templateUrl: "./app/shared/language-switcher/language-switcher.component.html"
})
export class LanguageSwitcherComponent {
    supportedLanguages: any;
    languages: string[];

    constructor(private translateService: TranslateService) {
        this.supportedLanguages = { "en": "English", "ru": "Русский" };
        this.languages = translateService.getLangs();
    }

    get currentLang():string {
        return this.translateService.currentLang;
    }

    set currentLang(lang: string) {
        this.translateService.use(lang);
    }

}