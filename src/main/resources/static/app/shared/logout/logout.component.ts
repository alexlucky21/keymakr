import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

import { AuthenticationService } from "../service/authentication.service";

@Component({
    selector: 'logout',
    templateUrl: './app/shared/logout/logout.component.html',
    providers: []
})

export class LogoutComponent implements OnInit {

    constructor(private authService: AuthenticationService, private router: Router) {
    }

    ngOnInit(): void {
    }

    logout() {
        this.authService.logout().subscribe((value) => {
            this.authService.role = undefined;
            this.authService.isLoggedIn = false;
            this.router.navigate([ "/" ]);
        });
    }

}