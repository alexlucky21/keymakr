import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LogoutComponent } from "./logout.component";
import { SharedTranslateModule } from "../shared-translate.module";

@NgModule({
    imports: [ CommonModule, SharedTranslateModule ],
    declarations: [ LogoutComponent ],
    exports: [ LogoutComponent ]
})

export class LogoutModule {
}
