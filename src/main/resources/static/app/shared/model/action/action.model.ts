import { ActionParameter } from "./action.parameter.model";
export class Action{

    id: number;
    name: string;
    metadata: string;
    predefined: boolean;
    parameters: ActionParameter[];
    editFl: boolean;

    constructor() {
        this.id = null;
        this.name = null;
        this.metadata = null;
        this.predefined = null;
        this.parameters = [];
    }
}
