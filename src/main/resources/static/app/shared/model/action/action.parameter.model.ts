import { Action } from "./action.model";
export class ActionParameter{
    id: number;
    name: string;
    action: Action;
    editFl: boolean;
}