import { Camera } from "../camera.model";
import { Action } from "./action.model";

export class CameraAction {
    id: number;
    camera: Camera;
    action: Action;

    constructor() {
    }
}
