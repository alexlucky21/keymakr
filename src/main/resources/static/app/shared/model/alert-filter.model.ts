export class AlertFilter {
    alertType: string;

    constructor() {
        this.resetFilter();
    }

    resetFilter(): void {
        this.alertType     = "PAUSE";
    }
}