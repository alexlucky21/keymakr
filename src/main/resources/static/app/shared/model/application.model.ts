export class ApplicationModel{
    id: number;
    name: string;
    email: string;
    applicationDate: string;
    status: string;
}
