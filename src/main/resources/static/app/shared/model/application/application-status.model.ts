export enum APPLICATION_STATUS {NEW = 1, READ = 2}

export class ApplicationStatus{
    id: number;
    name: string;
    description: string;
}