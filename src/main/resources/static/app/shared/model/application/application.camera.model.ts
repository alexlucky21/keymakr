import { ApplicationLocation } from "./application.location.model";
import { ApplicationCameraViewingHours } from "./application.camera.viewing-hours.model";
import { ApplicationCameraProperties } from "./application.camera.properties.model";
export class ApplicationCamera {
    id: number;
    name: string;
    comment: string;
    applicationLocation: ApplicationLocation;
    url: string;
    login: string;
    password: string;
    useDefaultViewingHours: boolean;
    applicationCameraViewingHours: ApplicationCameraViewingHours[];
    activationDate: string;
    properties: ApplicationCameraProperties;

    constructor() {
        this.id = null;
        this.name = null;
        this.comment = null;
        this.applicationLocation = new ApplicationLocation();
        this.url = null;
        this.login = null;
        this.password = null;
        this.useDefaultViewingHours = true;
        this.applicationCameraViewingHours = [];
        this.activationDate = null;
        this.properties = new ApplicationCameraProperties();
    }
}