export class ApplicationCameraProperties{
    activationStatus: string;
    assignedTo: string;
    disconnectedStatus: string;
}