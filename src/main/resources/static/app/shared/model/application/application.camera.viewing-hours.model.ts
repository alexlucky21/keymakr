import { ApplicationCamera } from "./application.camera.model";

export class ApplicationCameraViewingHours {
    id: number;
    applicationCamera: ApplicationCamera;
    dayOfWeek: string;
    fromTime: string;
    toTime: string;
}
