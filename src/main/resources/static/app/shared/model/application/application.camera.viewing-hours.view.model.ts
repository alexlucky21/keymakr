import { ViewingTime } from "../viewing-time.model";
export class ApplicationCameraViewingHoursView {
    id: number;
    applicationCameraId: number;
    dayOfWeek: string;
    applicationCameraViewingHoursId: number;
    fromTime: string;
    from: ViewingTime;
    toTime: string;
    to: ViewingTime;
    active: boolean;

    constructor() {
        this.id = null;
        this.applicationCameraId = null;
        this.dayOfWeek = null;
        this.applicationCameraViewingHoursId = null;
        this.fromTime = null;
        this.from = null;
        this.toTime = null;
        this.to = null;
        this.active = null;
    }
}