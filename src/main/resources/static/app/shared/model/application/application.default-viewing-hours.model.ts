import { Application } from "./application.model";
export class ApplicationDefaultViewingHours {
    id: number;
    application: Application;
    dayOfWeek: string;
    fromTime: string;
    toTime: string;
}
