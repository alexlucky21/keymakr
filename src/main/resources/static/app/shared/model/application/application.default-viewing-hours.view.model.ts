import { ViewingTime } from "../viewing-time.model";
export class ApplicationDefaultViewingHoursView {
    id: number;
    applicationId: number;
    dayOfWeek: string;
    applicationDefaultViewingHoursId: number;
    fromTime: string;
    toTime: string;
    active: boolean;
    from: ViewingTime;
    to: ViewingTime;
}