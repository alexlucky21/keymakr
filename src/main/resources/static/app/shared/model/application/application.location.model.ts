import { ApplicationCamera } from "./application.camera.model";
import { Application } from "./application.model";

export class ApplicationLocation {
    id: number;
    name: string;
    address: string;
    timezone: string;
    comment: string;
    ownerApplication: Application;
    applicationCameras: ApplicationCamera[];

    constructor() {
        this.applicationCameras = [];
    }
}