import { ApplicationStatus } from "./application-status.model";
import { ApplicationProfile } from "./application.profile.model";
import { ApplicationLocation } from "./application.location.model";

export class Application {
    id: number;
    applicationDate: string;
    applicationStatus: ApplicationStatus;
    applicationProfile: ApplicationProfile;
    applicationLocations: ApplicationLocation[];
    declineReason: string;

    constructor() {
        this.applicationStatus = new ApplicationStatus();
        this.applicationProfile = new ApplicationProfile();
        this.applicationDate = null;
        this.applicationLocations = [];
        this.declineReason = null;
    }
}

