export class ApplicationProfile {
    id: number;
    firstName: string;
    lastName: string;
    emailAddress: string;
    phone: string;

    constructor() {
        this.id = null;
        this.firstName = null;
        this.lastName = null;
        this.emailAddress = null;
        this.phone = null;
    }

}