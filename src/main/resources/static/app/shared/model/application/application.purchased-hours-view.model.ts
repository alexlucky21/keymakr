export class ApplicationPurchasedHoursView {
    applicationId: number;
    purchasedHours: number;
}
