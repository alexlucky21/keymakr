import { Application } from "./application.model";
export class ApplicationPurchasedHours{
    id: number;
    application: Application;
    purchaseDate: Date;
    amountPurchasedHours: number;

}