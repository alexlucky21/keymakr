import { Camera } from "./camera.model";

export class CameraAssignment {
    id: number;
    camera: Camera;
    total: number;

    constructor() { }
}