import { Customer } from "./customer.model";
import { Camera } from "./camera.model";

export class CameraLocation {
    id: number;
    name: string;
    address: string;
    timezone: string;
    comment: string;
    owner: Customer;
    cameras: Camera[];

    constructor() {
        this.cameras = [];
    }
}