import { Operator } from "./operator.model";
import { Camera } from "./camera.model";

export class CameraOperator {
    id: number;
    isMain: boolean;
    operator: Operator;
    camera: Camera;
    permanentOperator: Operator;
    assignedAt: Date;
    unassignedAt: Date;
}