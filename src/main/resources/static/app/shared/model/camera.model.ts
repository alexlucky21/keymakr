import { CameraLocation } from "./camera-location.model";
import { CameraOperator } from "./camera-operator.model";

export class Camera {
    id: number;
    name: string;
    comment: string;
    location: CameraLocation;
    cameraOperators: CameraOperator[];

    constructor() {
        this.cameraOperators = [];
    }
}