import { ROLES } from "./role.model";
import { Profile } from "./profile.model";
import { CameraLocation } from "./camera-location.model";

export class Customer {
    id: number;
    profile: Profile;
    locations: CameraLocation[];
    registrationDate: string;

    constructor(){
        this.profile = new Profile();
        this.profile.identity.role.id = ROLES.CUSTOMER;
        this.locations = [];
        this.registrationDate = null;
    }
}