import { Role } from "./role.model";

export class Identity {
    id: number;
    username: number;
    role: Role;

    constructor() {
        this.id = null;
        this.username = null;
        this.role = new Role();
    }
}