export class LogDetailsFilter {
    locationId: number;
    isMainTimeline: string;
    streamSearchString: string;
    actionSearchString: string;
    operatorSearchString: string;
    from: Date;
    to: Date;

    constructor() {
        this.resetFilter();
    }

    resetFilter(): void {
        this.isMainTimeline = "";
        this.streamSearchString = "";
        this.actionSearchString = "";
        this.operatorSearchString = "";
        this.from = null;
        this.to = null;
    }
}
