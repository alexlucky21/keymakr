export class LogFilter {
    operatorId: number;
    cameraId: number;
    customerId: number;
    searchString: string;
    from: Date;
    to: Date;

    constructor() {
        this.resetFilter();
    }

    resetFilter(): void {
        this.operatorId = 0;
        this.cameraId = 0;
        this.customerId = 0;
        this.searchString = "";
        this.from = null;
        this.to = null;
    }
}
