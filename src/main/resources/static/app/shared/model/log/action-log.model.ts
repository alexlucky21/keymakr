import { Operator } from "../operator.model";
import { ActionParameter } from "../action/action.parameter.model";
import { CameraAction } from "../action/camera-action.model";

export class ActionLog {
    id: number;
    dateTimeStart: Date;
    operator: Operator;
    isMain: boolean;
    cameraAction: CameraAction;
    actionParameter: ActionParameter;

    constructor() {
    }
}