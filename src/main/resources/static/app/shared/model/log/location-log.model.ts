import { CameraLocation } from "../camera-location.model";

export class LocationLog {
    id: number;
    location: CameraLocation;
    dateTimeLastLog: Date;

    constructor() {
    }

}