import { Operator } from "./operator.model";

export class OperatorAssignment {
    id: number;
    operator: Operator;
    mainTotal: number;
    total: number;

    constructor() { }
}