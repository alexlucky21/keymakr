export class OperatorFilter {
    state: string;
    searchString: string;

    constructor() {
        this.resetFilter();
    }

    resetFilter(): void {
        this.state          = "";
        this.searchString   = "";
    }
}