import { CameraOperator } from "./camera-operator.model";

export class OperatorRequest {
    id: number;
    fromCamera2Operator: CameraOperator;
    requestType: String;
    requestedAt: Date;
    toCamera2Operator: CameraOperator;
    changedAt: Date;
    isRequestViewed: boolean;
}