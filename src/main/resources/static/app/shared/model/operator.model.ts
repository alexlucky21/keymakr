import { Profile } from "./profile.model";
import { ROLES } from "./role.model";
import { CameraOperator } from "./camera-operator.model";

export class Operator {
    id: number;
    state: string;
    profile: Profile;
    cameraOperators: CameraOperator[];

    constructor() {
        this.state = 'NOT_WORKING';
        this.profile = new Profile();
        this.profile.identity.role.id = ROLES.OPERATOR;
        this.cameraOperators = [];
    }
}