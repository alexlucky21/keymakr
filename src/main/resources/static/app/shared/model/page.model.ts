export interface IPage {
    content: any[];
    totalPages: number;
    totalElements: number;
    numberOfElements: number;
    size: number;
    number: number;
    first:boolean;
    last:boolean;
    sort:string;
}