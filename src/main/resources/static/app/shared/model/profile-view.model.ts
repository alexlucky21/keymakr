export interface ProfileView {
    identityId: number;
    name: string;
    firstName: string;
    lastName: string;
    emailAddress: string;
    phone: string;
    roleId: number;
    roleName: string;
}