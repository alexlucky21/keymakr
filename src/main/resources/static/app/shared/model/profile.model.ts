import { Identity } from "./identity.model";

export class Profile {
    id: number;
    firstName: string;
    lastName: string;
    emailAddress: string;
    phone: string;
    identity: Identity;

    constructor() {
        this.id = null;
        this.firstName = null;
        this.lastName = null;
        this.emailAddress = null;
        this.phone = null;
        this.identity = new Identity();
    }

}