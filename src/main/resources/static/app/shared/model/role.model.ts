export enum ROLES { CUSTOMER = 1,  OPERATOR, CUSTOMER_ADMIN, OPERATOR_ADMIN, MAIN_ADMIN};

export class Role {
    id: number;
    name: string;
    description: string;

    constructor() {
        this.id = 0;
        this.name = null;
        this.description = null;
    }
}