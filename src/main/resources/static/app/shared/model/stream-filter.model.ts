export class StreamFilter {
    operatorId: number;
    locationId: number;
    cameraId: number;
    customerId: number;
    state: string;
    searchString: string;

    constructor() {
        this.resetFilter();
    }

    resetFilter(): void {
        this.operatorId     = 0;
        this.locationId     = 0;
        this.cameraId       = 0;
        this.customerId     = 0;
        this.state          = "";
        this.searchString   = "";
    }
}