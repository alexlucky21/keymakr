export class Stream {
    id: number;
    name: string;
    url: string;
}