export class ViewingTime {
    hours: string;
    minutes: string;
    seconds: string;

    constructor(hours: string, minutes: string, seconds: string) {
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = seconds;
    }

    static splitToTime(fullTime: string, isToTime?: boolean): ViewingTime {
        if (fullTime == null) {
            if(isToTime){
                return new ViewingTime('23', '55', '00');
            }else {
                return new ViewingTime('00', '00', '00');
            }
        } else {
            return new ViewingTime(fullTime.split(':')[ 0 ].trim(), fullTime.split(':')[ 1 ], fullTime.split(':')[ 2 ]);
        }
    };

    static joinToString(time: ViewingTime): string {
        return time.hours + ':' + time.minutes + ':' + time.seconds;
    };

    static hoursList: string[] = ['00', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23'];
    static minutesList: string[] = ['00', '05', '10', '15', '20', '25', '30', '35', '40', '45', '50', '55'];
}
