import { Pipe, PipeTransform } from "@angular/core";
@Pipe({
    name: 'hourToFilter'
})
export class ViewingHoursPipe implements PipeTransform {
    transform(hoursTo: string[], hourFrom: string): string[]{
        return hoursTo.filter(hour => hour >= hourFrom);
    }
}