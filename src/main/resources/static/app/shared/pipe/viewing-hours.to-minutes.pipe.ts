import { Pipe, PipeTransform } from "@angular/core";
@Pipe({
    name: 'minutesToFilter'
})
export class ViewingMinutesPipe implements PipeTransform {
    transform(minutesTo: string[], hourFrom: string, hourTo: string, minutesFrom: string): string[] {
        if (hourFrom != hourTo) {
            return minutesTo;
        }else {
            return minutesTo.filter(minute => minute > minutesFrom);
        }
    }
}