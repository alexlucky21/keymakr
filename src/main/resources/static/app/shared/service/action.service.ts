import { Http, Response } from "@angular/http";
import { Observable } from "rxjs";
import { Action } from "../model/action/action.model";
import { ActionParameter } from "../model/action/action.parameter.model";
import { Injectable } from "@angular/core";

@Injectable()
export class ActionService {
    private actionsUrl: string = 'actions';

    constructor(private http: Http) {
    }

    getDefaultActions(): Observable<Action[]> {
        return this.http.get(this.actionsUrl + '/default').map((res: Response) => res.json());
    }

    saveAction(action: Action): Observable<Action>{
        return this.http.post(this.actionsUrl, action).map((res: Response) => res.json());
    }

    deleteAction(actionId: number): Observable<number>{
        return this.http.delete(this.actionsUrl + '/' + actionId).map((res: Response) => res.json());
    }

    getActionParameterByActionId(actionId: number): Observable<ActionParameter[]>{
        return this.http.get(this.actionsUrl + '/' + actionId + '/parameters').map((res: Response) => res.json());
    }

    saveActionParameter(actionParameter: ActionParameter): Observable<ActionParameter>{
        return this.http.post(this.actionsUrl + '/parameters', actionParameter).map((res: Response) => res.json());
    }

    deleteActionParameter(actionParameterId: number): Observable<number>{
        return this.http.delete(this.actionsUrl + '/parameters/' + actionParameterId).map((res: Response) => res.json());
    }
}
