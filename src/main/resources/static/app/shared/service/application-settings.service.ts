import { Injectable } from "@angular/core";
import { Http } from "@angular/http";

@Injectable()
export class ApplicationSettingsService {

    private settings: any;

    constructor(private http: Http) {
        this.settings = { "PAGINATION_PAGE_SIZE": 25 , "MAX_CAMERAS_PER_OPERATOR": 6};
    }

    get(name: string): any {
        return this.settings[ name ];
    }
}
