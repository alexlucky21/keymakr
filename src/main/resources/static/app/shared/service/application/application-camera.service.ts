import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { Observable } from "rxjs";
import { ApplicationCamera } from "../../model/application/application.camera.model";
import { ApplicationCameraViewingHours } from "../../model/application/application.camera.viewing-hours.model";
import { ApplicationCameraViewingHoursView } from "../../model/application/application.camera.viewing-hours.view.model";

@Injectable()
export class ApplicationCameraService {
    private applicationCameraUrl: string = 'applications/applicationLocations';

    constructor(private http: Http) {
    }

    getApplicationCamera(applicationCameraId: number): Observable<ApplicationCamera>{
        return this.http.get(`${this.applicationCameraUrl}/applicationCameras/${applicationCameraId}`).map((res: Response)=> res.json());
    }

    getApplicationCamerasByApplicationLocation(applicationLocationId: number): Observable<ApplicationCamera[]>{
        return this.http.get(`${this.applicationCameraUrl}/${applicationLocationId}/applicationCameras`).map((res: Response)=> res.json());
    }

    saveApplicationCamera(applicationCamera: ApplicationCamera): Observable<ApplicationCamera> {
        return this.http.post(`${this.applicationCameraUrl}/applicationCameras`, applicationCamera).map((res: Response)=> res.json());
    }

    deleteApplicationCamera(applicationCameraId: number): Observable<ApplicationCamera> {
        return this.http.delete(`${this.applicationCameraUrl}/applicationCameras/${applicationCameraId}`).map((res: Response)=> res.json());
    }

    getViewingHoursView(applicationCameraId: number): Observable<ApplicationCameraViewingHoursView[]> {
        return this.http.get(`${this.applicationCameraUrl}/applicationCameras/${applicationCameraId}/viewingHours`).map((res: Response)=> res.json());
    }

    saveViewingHours(applicationCameraViewingHoursList: ApplicationCameraViewingHours[]): Observable<ApplicationCameraViewingHours[]>{
        return this.http.post(this.applicationCameraUrl + '/applicationCameras/applicationCameraViewingHours', applicationCameraViewingHoursList).map((res: Response)=> res.json());
    }

    deleteViewingHours(applicationCameraViewingHoursIds: number[]): Observable<number[]>{
        return this.http.put(this.applicationCameraUrl + '/applicationCameras/applicationCameraViewingHours', applicationCameraViewingHoursIds).map((res: Response)=> res.json());
    }

    activateApplicationCamera(applicationCamera: ApplicationCamera) : Observable<ApplicationCamera> {
        return this.http.post(`${this.applicationCameraUrl}/applicationCameras/activate`, applicationCamera).map((res: Response)=> res.json());
    }
}
