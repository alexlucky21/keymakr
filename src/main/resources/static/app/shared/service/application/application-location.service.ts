import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { Observable } from "rxjs";
import { ApplicationLocation } from "../../model/application/application.location.model";

@Injectable()
export class ApplicationLocationService {

    private applicationLocationUrl: string = 'applications/applicationLocations';

    constructor(private http: Http) {
    }

    saveApplicationLocation(applicationLocation: ApplicationLocation): Observable<ApplicationLocation>{
        return this.http.post(`${this.applicationLocationUrl}`, applicationLocation).map((res: Response)=> res.json());
    }

    deleteApplicationLocation(applicationLocationId: number): Observable<number>{
        return this.http.delete(`${this.applicationLocationUrl}/${applicationLocationId}`).map((res: Response)=> res.json());
    }

}