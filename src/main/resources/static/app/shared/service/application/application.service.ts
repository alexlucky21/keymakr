import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { IPage } from "../../model/page.model";
import { Observable } from "rxjs";
import { Application } from "../../model/application/application.model";
import { Customer } from "../../model/customer.model";
import { ApplicationLocation } from "../../model/application/application.location.model";
import { ApplicationDefaultViewingHours } from "../../model/application/application.default-viewing-hours.model";
import { ApplicationDefaultViewingHoursView } from "../../model/application/application.default-viewing-hours.view.model";
import { ApplicationPurchasedHours } from "../../model/application/application.purchased-hours.model";
import { ApplicationPurchasedHoursView } from "../../model/application/application.purchased-hours-view.model";

@Injectable()
export class ApplicationService {

    private applicationUrl: string = 'applications';

    constructor(private http: Http) {
    }

    getApplications(pageNumber: number, pageSize: number): Observable<IPage> {
        let url: string = `${this.applicationUrl}?pageNumber=${pageNumber}&pageSize=${pageSize}`;
        return this.http.get(url).map((res: Response)=> res.json());
    }

    saveApplication(application: Application): Observable<Application> {
        return this.http.post(this.applicationUrl, application).map((res: Response)=> res.json());
    }

    activateAccount(application: Application): Observable<Customer> {
        return this.http.post(this.applicationUrl + '/activateAccount', application).map((res: Response)=> res.json());
    }

    declineApplication(application: Application): Observable<Application> {
        return this.http.post(this.applicationUrl + '/decline', application).map((res: Response)=> res.json());
    }

    getApplication(applicationId: number): Observable<Application> {
        return this.http.get(`${this.applicationUrl}/${applicationId}`).map((res: Response)=> res.json());
    }

    getApplicationLocationsByApplication(applicationId: number): Observable<ApplicationLocation[]>{
        return this.http.get(`${this.applicationUrl}/${applicationId}/applicationLocations`).map((res: Response)=> res.json());
    }

    getDefaultViewingHoursView(applicationId: number): Observable<ApplicationDefaultViewingHoursView[]> {
        return this.http.get(`${this.applicationUrl}/${applicationId}/viewingHours`).map((res: Response)=> res.json());
    }

    saveDefaultViewingHours(applicationDefaultViewingHoursList: ApplicationDefaultViewingHours[]): Observable<ApplicationDefaultViewingHours[]>{
        return this.http.post(this.applicationUrl + '/viewingHours', applicationDefaultViewingHoursList).map((res: Response)=> res.json());
    }

    deleteDefaultViewingHours(applicationDefaultViewingHoursId: number[]): Observable<number[]>{
        return this.http.put(this.applicationUrl + '/viewingHours', applicationDefaultViewingHoursId).map((res: Response)=> res.json());
    }

    saveApplicationPurchasedHours(applicationPurchasedHours: ApplicationPurchasedHours): Observable<ApplicationPurchasedHours>{
        return this.http.post(this.applicationUrl + '/purchasedHours', applicationPurchasedHours).map((res: Response)=> res.json());
    }
    getApplicationPurchasedHoursViewByApplicationId(applicationId: number): Observable<ApplicationPurchasedHoursView>{
        return this.http.get(`${this.applicationUrl}/${applicationId}/purchasedHours`).map((res: Response)=> res.json());
    }
}
