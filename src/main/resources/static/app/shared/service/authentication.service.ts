import { Injectable } from "@angular/core";
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs";

@Injectable()
export class AuthenticationService {
    isLoggedIn: boolean;
    role: string;

    constructor(private http: Http) { }

    login(username: string, password: string): Observable<any> {
        var credentials = `username=${username}&password=${password}`;
        var headers = new Headers();

        headers.append('Content-Type', 'application/x-www-form-urlencoded');

        return this.http.post("/login", credentials, { headers: headers }).map((res: Response)=> res.json());
    }

    restoreSessionData(): Observable<any> {
        return this.http.get("/login").map((res: Response) => { return (res.text() ? res.json() : {}); });
    }

    logout(): Observable<any> {
        return this.http.get("/logout");
    }

    routePathByRole(roleName?: string): string {
        var routePath: string;

        switch (roleName || this.role) {
            case 'ROLE_CUSTOMER':
                routePath = "/customer";
                break;
            case 'ROLE_OPERATOR':
                routePath = "/operator";
                break;
            case 'ROLE_CUSTOMER_ADMIN':
                routePath = "/customer-admin";
                break;
            case 'ROLE_OPERATOR_ADMIN':
                routePath = "/operator-admin";
                break;
            case 'ROLE_MAIN_ADMIN':
                routePath = "/main-admin";
                break;
            default:
                routePath = "/"
        }

        return routePath;
    }
}