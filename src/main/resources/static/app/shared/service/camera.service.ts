import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { Observable } from "rxjs";
import { Camera } from "../model/camera.model";
import { IPage } from "../model/page.model";
import { StreamFilter } from "../model/stream-filter.model";
import { CameraOperator } from "../model/camera-operator.model";
import { Operator } from "../model/operator.model";
import { OperatorRequest } from "../model/operator-request.model";

@Injectable()
export class CameraService {

    private camerasUrl: string = 'streams/cameras';
    private cameraAssignmentsUrl: string = 'cameraAssignments';

    constructor(private http: Http) {
    }

    getCameras(): Observable<Camera[]> {
        let url: string = `${this.camerasUrl}/all`;

        return this.http.get(url).map((res: Response) => res.json());
    }

    getFilteredCameras(pageNumber: number, pageSize: number, streamFilter: StreamFilter): Observable<IPage> {
        let url: string = `${this.camerasUrl}`;
        let parameters: string = "";

        parameters += (pageNumber ? `&pageNumber=${pageNumber}` : "")
            + (pageSize ? `&pageSize=${pageSize}` : "")
            + (streamFilter.operatorId > 0 ? `&operatorId=${streamFilter.operatorId}` : "")
            + (streamFilter.locationId > 0 ? `&locationId=${streamFilter.locationId}` : "")
            + (streamFilter.cameraId > 0 ? `&cameraId=${streamFilter.cameraId}` : "")
            + (streamFilter.customerId > 0 ? `&customerId=${streamFilter.customerId}` : "")
            + (streamFilter.state ? `&state=${streamFilter.state}` : "")
            + (streamFilter.searchString && streamFilter.searchString.trim() ? `&searchQuery=${streamFilter.searchString}` : "");

        if (parameters) {
            url += "?" + parameters.substr(1);
        }

        return this.http.get(url).map((res: Response) => res.json());
    }

    getFilteredCameraAssignments(pageNumber: number, pageSize: number, streamFilter: StreamFilter): Observable<IPage> {

        let url: string = `${this.cameraAssignmentsUrl}`;
        let parameters: string = "";

        parameters += (pageNumber ? `&pageNumber=${pageNumber}` : "")
            + (pageSize ? `&pageSize=${pageSize}` : "")
            + (streamFilter.operatorId > 0 ? `&operatorId=${streamFilter.operatorId}` : "")
            + (streamFilter.locationId > 0 ? `&locationId=${streamFilter.locationId}` : "")
            + (streamFilter.cameraId > 0 ? `&cameraId=${streamFilter.cameraId}` : "")
            + (streamFilter.customerId > 0 ? `&customerId=${streamFilter.customerId}` : "")
            + (streamFilter.state ? `&state=${streamFilter.state}` : "")
            + (streamFilter.searchString && streamFilter.searchString.trim() ? `&searchQuery=${streamFilter.searchString}` : "");

        if (parameters) {
            url += "?" + parameters.substr(1);
        }

        return this.http.get(url).map((res: Response) => res.json());
    }

    getCamera(cameraId: number): Observable<Camera> {
        let url: string = `${this.camerasUrl}/${cameraId}`;

        return this.http.get(url).map((res: Response) => res.json());
    }


    getCameraOperatorsByCameraId(cameraId: number): Observable<CameraOperator[]> {
        let url: string = `${this.camerasUrl}/${cameraId}/operators`;

        return this.http.get(url).map((res: Response) => res.json());
    }

    getOperatorsOfCamera(cameraId: number): Observable<Operator[]> {
        return this.getCameraOperatorsByCameraId(cameraId).map(
            (cameraOperators: CameraOperator[]) => cameraOperators.map(
                (cameraOperators: CameraOperator) => cameraOperators.operator)
        );
    }

    reassignCamera(operatorRequest: OperatorRequest, toCameraOperator: CameraOperator): Observable<CameraOperator> {
        let url: string = `${this.camerasUrl}/${operatorRequest.fromCamera2Operator.camera.id}/reassign`;
        let parameters: string = "";

        parameters += `&operatorRequestId=${operatorRequest.id}`
            + `&toOperatorId=${toCameraOperator.operator.id}`
            + `&isMain=${toCameraOperator.isMain}`;

        url += "?" + parameters.substr(1);

        return this.http.get(url).map((res: Response) => res.json());
    }

    assignOperatorToCamera(cameraOperator: CameraOperator): Observable<CameraOperator> {
        let url: string = `${this.camerasUrl}/${cameraOperator.camera.id}/assign?operatorId=${cameraOperator.operator.id}` +
            `&isMain=${cameraOperator.isMain}`;

        return this.http.get(url).map((res: Response) => res.json());
    }

    unassignOperatorFromCamera(cameraOperator: CameraOperator): Observable<CameraOperator> {
        let url: string = `${this.camerasUrl}/${cameraOperator.camera.id}/unassign?operatorId=${cameraOperator.operator.id}`;

        return this.http.get(url).map((res: Response) => res.json());
    }

    deleteCamera(cameraId: number): Observable<number> {
        let url: string = `${this.camerasUrl}/${cameraId}`;

        return this.http.delete(url).map((res: Response) => res.json());
    }

    saveCamera(camera: Camera): Observable<Camera> {
        let url: string = `${this.camerasUrl}`;

        return this.http.post(url, JSON.stringify(camera)).map((res: Response) => res.json());
    }
}