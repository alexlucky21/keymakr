import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { Observable } from "rxjs";
import { IPage } from "../model/page.model";
import { Customer } from "../model/customer.model";

@Injectable()
export class CustomerService{

    private customersUrl: string = 'customers';

    constructor(private http: Http) { }

    getCustomers(): Observable<Customer[]> {
        let url: string = `${this.customersUrl}/all`;
        return this.http.get(url).map((res: Response)=> res.json());
    }

    getFilteredCustomers(pageNumber: number, pageSize: number, searchQuery: string): Observable<IPage> {
        let url: string = `${this.customersUrl}?pageNumber=${pageNumber}&pageSize=${pageSize}&searchQuery=${searchQuery}`;
        return this.http.get(url).map((res: Response)=> res.json());
    }

    getCustomer(customerId: number): Observable<Customer> {
        let url: string = `${this.customersUrl}/${customerId}`;
        return this.http.get(url).map((res: Response)=> res.json());
    }

    deleteCustomer(customerId: number): Observable<number> {
        let url: string = `${this.customersUrl}/${customerId}`;
        return this.http.delete(url).map((res: Response)=> res.json());
    }

    saveCustomer(customer: Customer):  Observable<Customer> {
        let url: string = `${this.customersUrl}`;
        return this.http.put(url, JSON.stringify(customer)).map((res: Response)=> res.json());
    }
}
