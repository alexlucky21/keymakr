import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { Observable } from "rxjs";
import { CameraLocation } from "../model/camera-location.model";
import { Camera } from "../model/camera.model";
import { LogFilter } from "../model/log-filter.model";
import { LogDetailsFilter } from "../model/log-details-filter.model";
import { IPage } from "../model/page.model";

@Injectable()
export class LocationService {

    private locationsUrl: string = 'streams/locations';

    constructor(private http: Http) {
    }

    getLocations(): Observable<CameraLocation[]> {
        let url: string = `${this.locationsUrl}/all`;

        return this.http.get(url).map((res: Response) => res.json());
    }

    getCamerasByLocation(locationId: number): Observable<Camera[]> {
        let url: string = `${this.locationsUrl}/${locationId}/cameras`;

        return this.http.get(url).map((res: Response) => res.json());
    }

    getLocation(locationId: number): Observable<CameraLocation> {
        let url: string = `${this.locationsUrl}/${locationId}`;

        return this.http.get(url).map((res: Response) => res.json());
    }

    deleteLocation(locationId: number): Observable<number> {
        let url: string = `${this.locationsUrl}/${locationId}`;

        return this.http.delete(url).map((res: Response) => res.json());
    }

    saveLocation(location: CameraLocation): Observable<CameraLocation> {
        let url: string = `${this.locationsUrl}`;

        return this.http.post(url, JSON.stringify(location)).map((res: Response) => res.json());
    }

    getLocationLog(pageNumber: number, pageSize: number, logFilter: LogFilter): Observable<IPage> {
        let url: string = `${this.locationsUrl}/logs`;
        let parameters: string = "";


        parameters += (pageNumber ? `&pageNumber=${pageNumber}` : "")
            + (pageSize ? `&pageSize=${pageSize}` : "")
            + (logFilter.from ? `&from=${(new Date(logFilter.from)).getTime()}` : "")
            + (logFilter.to ? `&to=${(new Date(logFilter.to)).getTime()}` : "")
            + (logFilter.searchString && logFilter.searchString.trim() ? `&locationName=${logFilter.searchString}` : "")
            + (logFilter.customerId > 0 ? `&customerId=${logFilter.customerId}` : "")
            + (logFilter.cameraId > 0 ? `&cameraId=${logFilter.cameraId}` : "")
            + (logFilter.operatorId > 0 ? `&operatorId=${logFilter.operatorId}` : "");

        if (parameters) {
            url += "?" + parameters.substr(1);
        }

        return this.http.get(url).map((res: Response) => res.json());
    }


    getLogDetails(pageNumber: number, pageSize: number, logDetailsFilter: LogDetailsFilter): Observable<IPage> {
        let url: string = `${this.locationsUrl}/${logDetailsFilter.locationId}/log`;
        let parameters: string = "";

        parameters += (pageNumber ? `&pageNumber=${pageNumber}` : "")
            + (pageSize ? `&pageSize=${pageSize}` : "")
            + (logDetailsFilter.from ? `&from=${(new Date(logDetailsFilter.from)).getTime()}` : "")
            + (logDetailsFilter.to ? `&to=${(new Date(logDetailsFilter.to)).getTime()}` : "")
            + (logDetailsFilter.isMainTimeline ? `&isMain=${logDetailsFilter.isMainTimeline}` : "")
            + (logDetailsFilter.streamSearchString && logDetailsFilter.streamSearchString.trim() ? `&streamName=${logDetailsFilter.streamSearchString}` : "")
            + (logDetailsFilter.actionSearchString && logDetailsFilter.actionSearchString.trim() ? `&actionName=${logDetailsFilter.actionSearchString}` : "")
            + (logDetailsFilter.operatorSearchString && logDetailsFilter.operatorSearchString.trim() ? `&operatorName=${logDetailsFilter.operatorSearchString}` : "");

        if (parameters) {
            url += "?" + parameters.substr(1);
        }

        return this.http.get(url).map((res: Response) => res.json());
    }
}