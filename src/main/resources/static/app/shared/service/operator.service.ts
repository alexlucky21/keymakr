import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { Observable } from "rxjs";
import { IPage } from "../model/page.model";
import { Operator } from "../model/operator.model";
import { Camera } from "../model/camera.model";
import { OperatorAssignment } from "../model/operator-assignment.model";
import { CameraOperator } from "../model/camera-operator.model";
import { OperatorFilter } from "../model/operator-filter.model";
import { RequestFilter } from "../model/request-filter.model";
import { OperatorRequest } from "../model/operator-request.model";

@Injectable()
export class OperatorService {

    private operatorsUrl: string = 'operators';
    private operatorAssignmentsUrl: string = 'operatorAssignments';

    constructor(private http: Http) {
    }

    getOperators(): Observable<Operator[]> {
        let url: string = `${this.operatorsUrl}/all`;

        return this.http.get(url).map((res: Response) => res.json()).map((operators: Operator[]) => this.initOperators(operators));
    }

    getFilteredOperators(pageNumber: number, pageSize: number, operatorFilter: OperatorFilter): Observable<IPage> {
        //To pass array of states you should be used coma delimited string
        let url: string = `${this.operatorsUrl}`;
        let parameters: string = "";

        parameters += (pageNumber ? `&pageNumber=${pageNumber}` : "")
            + (pageSize ? `&pageSize=${pageSize}` : "")
            + (operatorFilter.state && operatorFilter.state.trim() ? `&states=${operatorFilter.state}` : "")
            + (operatorFilter.searchString ? `&searchQuery=${operatorFilter.searchString}` : "");

        if (parameters) {
            url += "?" + parameters.substr(1);
        }

        return this.http.get(url).map((res: Response) => res.json()).map((page: IPage) => {
            page.content = this.initOperators(page.content);
            return page;
        });
    }

    getOperator(operatorId: number): Observable<Operator> {
        let url: string = `${this.operatorsUrl}/${operatorId}`;

        return this.http.get(url).map((res: Response) => res.json()).map((operator: Operator) => {
            return this.initOperator(operator);
        });
    }

    getCameraOperatorsByOperatorId(operatorId: number): Observable<CameraOperator[]> {
        let url: string = `${this.operatorsUrl}/${operatorId}/cameras`;

        return this.http.get(url).map((res: Response) => res.json());
    }

    getCamerasOfOperator(operatorId: number): Observable<Camera[]> {
        return this.getCameraOperatorsByOperatorId(operatorId).map(
            (cameraOperators: CameraOperator[]) => cameraOperators.map(
                (cameraOperators: CameraOperator) => cameraOperators.camera)
        );
    }

    deleteOperator(operatorId: number): Observable<number> {
        let url: string = `${this.operatorsUrl}/${operatorId}`;

        return this.http.delete(url).map((res: Response) => res.json());
    }

    saveOperator(operator: Operator): Observable<Operator> {
        let url: string = `${this.operatorsUrl}`;

        return this.http.post(url, operator).map((res: Response) => {
            console.log(res);
            return res.json()
        });
    }


    private initOperators(operators: Operator[]) {
        return operators.map((operator: Operator) => this.initOperator(operator));
    }

    private initOperator(operator: Operator): Operator {
        operator.cameraOperators = [];
        return operator;
    }

    getSuggestedOperatorAssignments(locationId: number, cameraId: number, searchQuery: string): Observable<OperatorAssignment[]> {
        let url: string = `${this.operatorAssignmentsUrl}/suggested?locationId=${locationId}&cameraId=${cameraId}&searchQuery=${searchQuery}`;

        return this.http.get(url).map((res: Response) => res.json());
    }

    getOtherOperatorAssignments(locationId: number, cameraId: number, searchQuery: string): Observable<OperatorAssignment[]> {
        let url: string = `${this.operatorAssignmentsUrl}/other?locationId=${locationId}&cameraId=${cameraId}&searchQuery=${searchQuery}`;

        return this.http.get(url).map((res: Response) => res.json());
    }

    assignCameraToOperator(cameraOperator: CameraOperator): Observable<CameraOperator> {
        let url: string = `${this.operatorsUrl}/${cameraOperator.operator.id}/assign?cameraId=${cameraOperator.camera.id}` +
            `&isMain=${cameraOperator.isMain}`;

        return this.http.get(url).map((res: Response) => res.json());
    }

    unassignCameraFromOperator(cameraOperator: CameraOperator): Observable<CameraOperator> {
        let url: string = `${this.operatorsUrl}/${cameraOperator.operator.id}/unassign?cameraId=${cameraOperator.camera.id}`;

        return this.http.get(url).map((res: Response) => res.json());
    }

    getOperatorRequests(pageNumber: number, pageSize: number, requestFilter: RequestFilter): Observable<IPage> {
        let url: string = `${this.operatorsUrl}/requests`;
        let parameters: string = "";

        parameters += (pageNumber ? `&pageNumber=${pageNumber}` : "")
                    + (pageSize ? `&pageSize=${pageSize}` : "");

        if (parameters) {
            url += "?" + parameters.substr(1);
        }

        return this.http.get(url).map((res: Response) => res.json());
    }

    markRequestsAsViewed(justViewedRequests: number[]): void {
        let url: string = `${this.operatorsUrl}/requests/viewed`;

        if (justViewedRequests && justViewedRequests.length > 0) {
            this.http.post(url, justViewedRequests).map((res: Response) => res.json()).subscribe();
        }
    }
}
