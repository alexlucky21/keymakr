import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { Observable } from "rxjs";

import { IPage } from "../../shared/model/page.model";
import { Profile } from "../model/profile.model";

@Injectable()
export class ProfileViewService {

    private adminsUrl: string = 'profileView/admins';
    private operatorsUrl :string= 'profileView/operators';
    private profileUrl: string = 'profileView/profile';
    private serviceUrl: string = 'profile';

    constructor(private http: Http) {
    }

    getAdminList(pageNumber: number, pageSize: number, custAdminFilterFl: boolean, operAdminFilterFl: boolean, searchQuery: string): Observable<IPage> {
        let url: string = this.adminsUrl + '?pageNumber=' + pageNumber + '&pageSize=' + pageSize
            + '&custAdminFilterFl=' + custAdminFilterFl
            + '&operAdminFilterFl=' + operAdminFilterFl
            + '&searchQuery=' + searchQuery;

        return this.http.get(url).map((res: Response)=> res.json());
    }

    getOperatorList(pageNumber: number, pageSize: number, searchQuery: string): Observable<IPage> {
        let url: string = this.operatorsUrl + '?pageNumber=' + pageNumber + '&pageSize=' + pageSize
            + '&searchQuery=' + searchQuery;

        return this.http.get(url).map((res: Response)=> res.json());
    }

    getProfile(identityId: number): Observable<Profile> {
        return this.http.get(this.profileUrl + '?identityId=' + identityId).map((res: Response)=> res.json());
    }

    saveProfile(profile: Profile): Observable<Profile> {
        return this.http.post(this.serviceUrl + '/save', profile).map((res: Response)=> res.json());
    }
}
