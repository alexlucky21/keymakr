import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable()
export class RecaptchaService{
    constructor(
        private http: Http
    ) {  }

    private recapUrl = '/reCaptcha';

    checkCaptcha(token: string): Observable<string> {
        return this.http.post(this.recapUrl , token)
        .map(resp => resp.json() as string);
    }
}