import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { Observable } from "rxjs";

import { Role } from "../../shared/model/role.model";

@Injectable()
export class RoleService {

    private serviceUrl = 'roles';

    constructor(private http: Http) {
    }

    getAdminRoles(): Observable<Role[]> {
        return this.http.get(this.serviceUrl + '/admin').map((res: Response)=> res.json());
    }

    getAllRoles(): Observable<Role[]> {
        return this.http.get(this.serviceUrl + '/all').map((res: Response)=> res.json());
    }
}
