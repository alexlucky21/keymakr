import { Injectable } from "@angular/core";
import { Stream } from "../../model/stream/stream.model";
import { Observable } from "rxjs/Observable";

@Injectable()
export class StreamService {

    counter: number;

    serviceUrl: string = 'streams';
    streamList: Stream[];

    constructor() {
        this.streamList = [];
        this.counter = 1;
    }

    getStreams(): Observable<Stream[]> {
        return Observable.of(this.streamList);
    }

    addStream(name: string, url: string): void {
        let stream: Stream = new Stream();
        stream.id = this.counter;
        stream.name = 'stream' + this.counter;
        stream.url = url;

        this.streamList.push(stream);
        this.counter++;
    }

    removeStream(id: number): Observable<boolean> {
        this.streamList.splice(this.streamList.indexOf(this.streamList.filter(stream => (stream.id == id))[0]), 1);
        return Observable.of(true);
    }

}