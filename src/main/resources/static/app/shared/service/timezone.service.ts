import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { Observable } from "rxjs";

@Injectable()
export class TimezoneService{
    private timezoneUrl = 'timezone';

    constructor(private http: Http) {
    }

    getAllTimezones(): Observable<string[]> {
        return this.http.get(this.timezoneUrl + '/all').map((res: Response)=> res.json());
    }
}