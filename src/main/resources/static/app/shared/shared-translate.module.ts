import { TranslateModule, TranslateService, TranslateLoader, TranslateStaticLoader } from 'ng2-translate';
import { NgModule, ModuleWithProviders } from "@angular/core";
import { Http } from "@angular/http";
import { LanguageSwitcherComponent } from "./language-switcher/language-switcher.component";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

@NgModule({
    imports: [ CommonModule, TranslateModule, FormsModule ],
    declarations: [ LanguageSwitcherComponent],
    exports: [ TranslateModule, LanguageSwitcherComponent ]
})
export class SharedTranslateModule {

    static forRoot(): ModuleWithProviders {

        return {
            ngModule: SharedTranslateModule,
            providers: [ {
                provide: TranslateLoader,
                useFactory: (http: Http) => new TranslateStaticLoader(http, './app/shared/assets/i18n', '.json'),
                deps: [ Http ]
            },
                TranslateService ],
        };
    }
}