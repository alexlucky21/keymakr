package com.keymakr.repository;

import com.keymakr.KeymakrApplicationTests;
import com.keymakr.model.Identity;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class IdentityRepositoryTest extends KeymakrApplicationTests {

    @Autowired
    private IdentityRepository identityRepository;


    public IdentityRepositoryTest() {
    }

    @Test
    public void testFindByUsername() {
        Identity identity = identityRepository.findByUsername("operator");

        assertNotNull(identity);
        assertEquals(2, identity.getId().longValue());
        assertEquals("operator", identity.getUsername());
        assertEquals("123456", identity.getPassword());
        assertEquals(2, identity.getRole().getId().longValue());
    }

    @Override
    protected void setUp() throws Exception {

    }

    @Override
    protected void tearDown() throws Exception {

    }
}
