package com.keymakr.repository;

import com.keymakr.KeymakrApplicationTests;
import com.keymakr.model.ProfileView;
import com.keymakr.model.Role;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ProfileViewRepositoryTest extends KeymakrApplicationTests {

    @Autowired
    private ProfileViewRepository repository;

    @Test
    public void testFindAll() {
        Page<ProfileView> result = repository.findAll(new PageRequest(0, 2));

        assertNotNull(result);
        assertEquals(2, result.getSize());
        assertEquals(3, result.getTotalPages());
        assertEquals(5, result.getTotalElements());
    }

    @Test
    public void testFindByRoleId() {
        Page<ProfileView> result = repository.findByRoleId(new PageRequest(0, 2), Role.OPERATOR);

        assertNotNull(result);
        assertEquals(2, result.getSize());
        assertEquals(1, result.getContent().size());
        assertEquals(1, result.getTotalPages());
        assertEquals(1, result.getTotalElements());
    }

    @Test
    public void testFindByRoleIdIn() {
        List<Long> roleIds = Arrays.asList(Role.CUSTOMER_ADMIN, Role.OPERATOR_ADMIN, Role.MAIN_ADMIN);
        Page<ProfileView> result = repository.findByRoleIdIn(new PageRequest(0, 2), roleIds);

        assertNotNull(result);
        assertEquals(2, result.getSize());
        assertEquals(2, result.getTotalPages());
        assertEquals(3, result.getTotalElements());
    }

    @Override
    protected void setUp() throws Exception {

    }

    @Override
    protected void tearDown() throws Exception {

    }
}
