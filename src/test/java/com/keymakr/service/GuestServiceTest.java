package com.keymakr.service;

import com.keymakr.KeymakrApplicationTests;
import com.keymakr.model.Identity;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class GuestServiceTest extends KeymakrApplicationTests {

    @Autowired
    private GuestService guestService;

    @Test
    public void testFindIdentityByUsername() {
        Identity identity = guestService.findIdentityByUsername("operator");
        assertNotNull(identity);
        assertEquals("Operator", identity.getProfile().getFirstName());
        assertEquals("Single", identity.getProfile().getLastName());
        assertEquals("operator@keymakr.com", identity.getProfile().getEmailAddress());
        assertEquals("8 703 32 103 01", identity.getProfile().getPhone());
    }


    @Override
    protected void setUp() throws Exception {

    }

    @Override
    protected void tearDown() throws Exception {

    }
}
